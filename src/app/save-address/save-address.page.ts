import { CommonService } from './../common.service';
import { IonicHttpService } from './../ionic-http.service';
import { CallApisService } from './../call-apis.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-save-address',
  templateUrl: './save-address.page.html',
  styleUrls: ['./save-address.page.scss'],
})
export class SaveAddressPage implements OnInit {
  
  saveUser :any = {
    userId: '',
    pincode: '',
    fristName: "",
    lastName: "",
    address: "",
    mobileNo: "",
    state: "",
    city: "",
    addressUser: [{
      street1: ''
    }]
  };
  moNumber = ''; 
  userId: any;
  spinner: boolean = false;

  constructor(private apiService: CallApisService, private ionicHttp: IonicHttpService, private router: Router, private commonService: CommonService) { }
  
  ionViewWillEnter(){
    this.spinner = true;
    // this.commonService.present();
    this.apiService.typeValue.subscribe((res: any) => {
      this.moNumber = res; 
    }
    ,(err) => {
      console.log(err)
    });

    this.moNumber = localStorage.getItem('moNumber')

    this.apiService.getUserData(this.moNumber).subscribe((res: any) => {
      this.spinner = false;
      this.saveUser = res;
    });
    this.getUserData();
  }


  ngOnInit() {
    this.apiService.typeValue.subscribe((res: any) => {
      this.moNumber = res; 
    }
    ,(err) => {
      console.log(err)
    });
    
    this.moNumber = localStorage.getItem('moNumber')

    // this.moNumber = localStorage.getItem('moNumber')
    // this.apiService.getUserData(this.moNumber).subscribe((res: any) => {
    //   this.saveUser = res;
    // },(err) => {
    //   console.log(err)
    // });
  }

  getUserData(){
    this.ionicHttp.getUserDetails(this.moNumber).then((res: any) => {
      this.spinner = false;
      // this.commonService.dismiss();
     console.log(res);
    let data = JSON.parse(res.data)
    this.saveUser = data;
    })
    .catch(err => {
      console.log(err)
    });
 }

 gotoChangePassword(){
  this.router.navigate(['/change-password'])
}

gotoMyOrder(){
  this.router.navigate(['/my-order'])
}

gotoAddress(){
  this.router.navigate(['/save-address'])
}
}
