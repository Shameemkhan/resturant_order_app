import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SaveAddressPageRoutingModule } from './save-address-routing.module';

import { SaveAddressPage } from './save-address.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SaveAddressPageRoutingModule
  ],
  declarations: [SaveAddressPage]
})
export class SaveAddressPageModule {}
