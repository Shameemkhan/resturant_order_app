import { ActivatedRoute, ParamMap } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AngularDelegate } from '@ionic/angular';
import { CallApisService } from '../call-apis.service';

@Component({
  selector: 'app-catagories-details',
  templateUrl: './catagories-details.page.html',
  styleUrls: ['./catagories-details.page.scss'],
})
export class CatagoriesDetailsPage implements OnInit {

  catagories: any[] = [];
  constructor(private apiService: CallApisService, private route: ActivatedRoute) {
    route.paramMap.subscribe((params: ParamMap) => {
      if(params.get('id')){
        this.apiService
      }
    })
   }

  ngOnInit() {
    this.apiService.catagoryData.subscribe((res: any[]) => {
      this.catagories = res;
      console.log(res);
    })
  }

}
