import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CatagoriesDetailsPageRoutingModule } from './catagories-details-routing.module';

import { CatagoriesDetailsPage } from './catagories-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CatagoriesDetailsPageRoutingModule
  ],
  declarations: [CatagoriesDetailsPage]
})
export class CatagoriesDetailsPageModule {}
