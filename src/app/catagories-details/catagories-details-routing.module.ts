import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CatagoriesDetailsPage } from './catagories-details.page';

const routes: Routes = [
  {
    path: '',
    component: CatagoriesDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CatagoriesDetailsPageRoutingModule {}
