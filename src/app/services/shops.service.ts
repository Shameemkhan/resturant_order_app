import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
  
})
export class ShopsService {
data: any[];
  constructor(private Http: HttpClient) { 
    this.getapidata()
  }

   getapidata(){
    return this.Http.get('https://randomuser.me/api/').subscribe(( n: any) => {
    return this.data = n;
    });
     
   }

   getData(){
     console.log(this.data);
    return this.data ;
   }
}
