import { TestBed } from '@angular/core/testing';

import { SearchproductService } from './searchproduct.service';

describe('SearchproductService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchproductService = TestBed.get(SearchproductService);
    expect(service).toBeTruthy();
  });
});
