import { CommonService } from '../common.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';


// http://192.168.0.174:8080/web-eh4u/app/#/welcome
@Injectable({
  providedIn: 'root'
})
export class CartService {
  makeZero: number = 0;
  sendValue = new BehaviorSubject<number>(0);
  private cart = [];
  public cartItemCount = new BehaviorSubject<number>(0);
  public checkQuantity = new Subject<number>()
  constructor(private commonService: CommonService) {}

  getCart() {
    return this.cart;
  }
 
  getCartItemCount() {
    return this.cartItemCount;
  }
 

  addProductToAddOns(product) {
    let added = false;
    let data1
    let price;
    product.optionGroup.slice(-1).forEach((itemGroup: any) => {

     
      
      let value: boolean = false;
      this.cart.forEach((cartItem: any) => {
        cartItem.optionGroup.slice(-1).forEach((cartGroup: any) => {
          console.log(cartGroup)
          for(var i = 0; i<itemGroup.length; i++){
            for(var j = 0; j<cartGroup.length; j++){
              if (cartItem.id === product.id  && itemGroup[0].id === cartGroup[0].id && itemGroup[itemGroup.length - 1].id === cartGroup[cartGroup.length - 1].id &&  itemGroup[i].id === cartGroup[j].id && itemGroup.length === cartGroup.length) {
                value = true;
                this.sendValue.next(cartItem.quantity);
                added = true;
              }
            }
          }
         
          if(value){
            data1 = localStorage.getItem('qty');
            // alert('value if works')
            if(data1 > 1){
              let data: number = parseInt(data1)
              console.log(cartGroup)
              cartGroup.forEach((dataForQty: any) => {
                if(dataForQty.qty > 0){
                  dataForQty.qty += data;
                }
              })
              cartItem.quantity += data;
              let filteredProduct = this.commonService.get_itemList();
              filteredProduct.forEach((data: any) => {
                if(product.id === data.id){
                 this.getTotalQuantityOfPerticalItem(data)
                }
              })
              localStorage.setItem('qty', '1')
            }
            else {
              // alert('else works')
              console.log(cartGroup)
              cartGroup.forEach((data: any) => {
                if(data.qty > 0){
                  data.qty++;
                }
              })
              cartItem.quantity += 1;
              let filteredProduct = this.commonService.get_itemList();
              filteredProduct.forEach((data: any) => {
                if(product.id === data.id){
                 this.getTotalQuantityOfPerticalItem(data)
                }
              })
            } 
          }
          value = false;
        });
      });
    });
    // this.calGst(product, product.price);
    if(!added) {
      product.quantity += 1;
      this.cart.push(product);
    }
      this.cartItemCount.next(this.cartItemCount.value + 1);
   }

   calGst(product, taxPrice){
    let cgstTaxPer;
    let sgstTaxPer;
    let vatTaxPer;
    
    product.taxes.forEach((tax: any) => {
      if(tax.name.includes('CGST')){
        cgstTaxPer = tax.structure.value;
        let taxAmount = this.calculateGst(taxPrice, cgstTaxPer)
        // console.log(taxAmount);
        tax.amount = taxAmount;
      }
      else if(tax.name.includes('SGST')){
        sgstTaxPer = tax.structure.value;
      }
      else{
        vatTaxPer = tax.structure.value;
        let perPrice =  taxPrice / vatTaxPer;
        taxPrice = perPrice + taxPrice;
        // console.log(taxPrice)
      }
    })

    // console.log(product)
  }

  calculateGst(orginalAmount, gstPercentage){
    let amount
    amount = (orginalAmount * gstPercentage) / 100;
    return amount;
   }

   updateItemCount(quantity){
    this.cartItemCount.next(this.cartItemCount.value + quantity);
   }

  getTotalQuantityOfPerticalItem(product){
    console.log(product)
    let total = 0;
    const hasData: boolean = this.cart.some(element => {
      if(product.id === element.id){
        return true;
      }
      return false ;
    });

  if(hasData){
    if(this.cart.length > 0 && hasData){
      for(var i = 0; i<this.cart.length; i++){
        if(product.id === this.cart[i].id){
          total += Number(this.cart[i].quantity); 
          product.quantity = total;
        }
      }
    }
  } 
    else{
      product.quantity = 0
    }
  }

  deleteProductToAddOns(product){
    product.optionGroup.slice(-1).forEach((itemGroup: any) => {
      let value: boolean = false;
      for (let [index, cartItem] of this.cart.entries()) {
        cartItem.optionGroup.slice(-1).forEach((cartGroup: any) => {
          for(var i = 0; i<itemGroup.length; i++){
            for(var j = 0; j<cartGroup.length; j++){
              if (cartItem.id === product.id &&  itemGroup[itemGroup.length - 1] === cartGroup[cartGroup.length - 1] && itemGroup[0].id === cartGroup[0].id && itemGroup[i].id === cartGroup[j].id && itemGroup.length === cartGroup.length) {
                value = true;
              }
            }
          }
          if(value){
            cartGroup.forEach((data: any) => {
              if(data.qty > 0){
                data.qty--;
              }
            })
            cartItem.quantity -= 1;
            let filteredProduct = this.commonService.get_itemList();
            filteredProduct.forEach((data: any) => {
              if(product.id === data.id){
                this.getTotalQuantityOfPerticalItem(data)
              }
            });
            this.cartItemCount.next(this.cartItemCount.value - 1);
          }
          if (cartItem.quantity === 0) {
            product.quantity = 0;
            let filteredProduct = this.commonService.get_itemList();
            filteredProduct.forEach((data: any) => {
              if(product.id === data.id){
                this.getTotalQuantityOfPerticalItem(data)
              }
            });
           let data =  this.cart.splice(index, 1);
          }
          value = false;
        })
      }
    })

  }

  // addProduct(product) {
  //   let data;
  //   let optionData;
  //   product.optionGroup.slice(-1).forEach((cartGroup: any) => {
  //    data = cartGroup;
  //   });
  //   console.log(product)
  //   let added = false;
  //   let data1;
  //   data1 = localStorage.getItem('qty');

  //   for (let p of this.cart) {
  //     p.optionGroup.slice(-1).forEach((cartGroup: any) => {
  //       optionData = cartGroup;
  //      });
  //     if (p.id === product.id && product.optionGroup.length > 0) {
  //       // alert('if works')
  //       if(data1 > 1){
  //         // alert('if works')
  //         this.sendValue.next(p.quantity);
  //         product.quantity += 1;
  //         p.quantity += product.quantity;
  //         // p.quantity += 1;
  //         this.updateQuanityToOrginalItems(product);
  //         added = true;
  //         break;
  //       }
  //       else{
  //         // alert('else works')
  //         this.sendValue.next(p.quantity);
  //         p.quantity += 1;
  //         this.updateQuanityToOrginalItems(product);
  //         added = true;
  //         break;
  //       }
      
  //     }
  //     else if(p.id === product.id  && product.optionGroup.length === 0){
  //       //  alert('else works')
  //       this.sendValue.next(p.quantity);
  //       p.quantity += 1;
  //       this.updateQuanityToOrginalItems(product);
  //       added = true;
  //       break;
  //     }
  //     else{

  //     }

  //   }
  //   if (!added) {
  //     product.quantity += 1;
  //     this.cart.push(product);
  //     this.makeUnique();
  //   }
  //   this.cartItemCount.next(this.cartItemCount.value + 1);
  // }

  addProduct(product) {
    let data;
    let optionData;
    product.optionGroup.slice(-1).forEach((cartGroup: any) => {
     data = cartGroup;
    });
    // console.log(product)
    let added = false;
    let data1;
    data1 = localStorage.getItem('qty');

    for (let p of this.cart) {
      p.optionGroup.slice(-1).forEach((cartGroup: any) => {
        optionData = cartGroup;
       });
       if(p.id === product.id  && product.optionGroup.length === 0){
        //  alert('else works')
        this.sendValue.next(p.quantity);
        p.quantity += 1;
        this.updateQuanityToOrginalItems(product);
        added = true;
        break;
      }
      else if(p.id === product.id){
        if (p.id === product.id && data.length === 0 && optionData.length === 0) {
          // alert('if works')
          if(data1 > 1){
            // alert('if works')
            this.sendValue.next(p.quantity);
            product.quantity += 1;
            p.quantity += product.quantity;
            // p.quantity += 1;
            this.updateQuanityToOrginalItems(product);
            added = true;
            break;
          }
          else{
            // alert('else works')
            this.sendValue.next(p.quantity);
            p.quantity += 1;
            this.updateQuanityToOrginalItems(product);
            added = true;
            break;
          }
        
        }
      }
     
      
  

    }
    if (!added) {
      product.quantity += 1;
      this.cart.push(product);
      this.makeUnique();
    }
    this.cartItemCount.next(this.cartItemCount.value + 1);
  }
 
  decreaseProduct(product) {
    let data;
    let optionData;
    product.optionGroup.slice(-1).forEach((cartGroup: any) => {
     data = cartGroup;
    });
    for (let [index, p] of this.cart.entries()) {
      p.optionGroup.slice(-1).forEach((cartGroup: any) => {
        optionData = cartGroup;
       });
      if (p.id === product.id && product.optionGroup.length === 0) {
        p.quantity -= 1;
        this.updateQuanityToOrginalItems(product);
        if (p.quantity == 0) {
          this.cart.splice(index, 1);
        }
      }
      else if(p.id === product.id){
        if(p.id === product.id && data.length === 0 && optionData.length === 0){
          p.quantity -= 1;
          this.updateQuanityToOrginalItems(product);
          if (p.quantity == 0) {
            this.cart.splice(index, 1);
          }
        }
      }
    }
    this.cartItemCount.next(this.cartItemCount.value - 1);
  }


  updateQuanityToOrginalItems(product){
        let filteredProduct = this.commonService.get_itemList();
        filteredProduct.forEach((data: any) => {
          if(product.id === data.id){
            this.getTotalQuantityOfPerticalItem(data);
          }
        })
  }

  removeProductOfAddOns(product){
    product.optionGroup.slice(-1).forEach((itemGroup: any) => {
      let value: boolean = false;
      for (let [index, cartItem] of this.cart.entries()) {
        cartItem.optionGroup.slice(-1).forEach((cartGroup: any) => {
          for(var i = 0; i<itemGroup.length; i++){
            for(var j = 0; j<cartGroup.length; j++){
              if (cartItem.id === product.id && itemGroup[0].id === cartGroup[0].id && itemGroup[i].id === cartGroup[j].id && itemGroup.length === cartGroup.length) {
                this.cartItemCount.next(this.cartItemCount.value - product.quantity);
                this.cart.splice(index, 1);
                // product.quantity = this.makeZero
              }
            }
          }
        })
      }
    })
  }

  removeProduct(product) {
    // console.log(product)
    let data;
    let optionData;
    let value: boolean = false;
    product.optionGroup.slice(-1).forEach((cartGroup: any) => {
     data = cartGroup;
    });

    for (let [index, p] of this.cart.entries()) {
      p.optionGroup.slice(-1).forEach((cartGroup: any) => {
        optionData = cartGroup;
       });
       if (p.id === product.id && product.optionGroup.length === 0) {
       
         this.cartItemCount.next(this.cartItemCount.value - p.quantity);
         p.quantity = 0;
         if(p.quantity === 0){
          this.cart.splice(index, 1);
         this.updateQuanityToOrginalItems(product);
         }
      }
      else if(p.id === product.id ){
        if(p.id === product.id && data.length === 0 && optionData.length === 0){
         
          this.cartItemCount.next(this.cartItemCount.value - p.quantity);
          p.quantity = 0;
          if(p.quantity === 0){
            this.cart.splice(index, 1);
            this.updateQuanityToOrginalItems(product);
           }
        }
        else  if(p.id === product.id && data.length != 0 && optionData.length != 0){
          product.optionGroup.slice(-1).forEach((itemGroup: any) => {
            let value: boolean = false;
            for (let [index, cartItem] of this.cart.entries()) {
              cartItem.optionGroup.slice(-1).forEach((cartGroup: any) => {
                for(var i = 0; i<itemGroup.length; i++){
                  for(var j = 0; j<cartGroup.length; j++){
                    if (cartItem.id === product.id &&  itemGroup[itemGroup.length - 1] === cartGroup[cartGroup.length - 1] && itemGroup[0].id === cartGroup[0].id && itemGroup[i].id === cartGroup[j].id && itemGroup.length === cartGroup.length) {
                      optionData = cartItem.quantity;
                      value=true;
                    }
                  }
                }
               
                if (value) {
                cartItem.quantity = 0;
                }
                if (cartItem.quantity === 0) {
                  product.quantity = 0;
                  this.cartItemCount.next(this.cartItemCount.value - optionData);
                  let filteredProduct = this.commonService.get_itemList();
                  filteredProduct.forEach((data: any) => {
                    if(product.id === data.id){
                      this.getTotalQuantityOfPerticalItem(data)
                    }
                  });
                 let data =  this.cart.splice(index, 1);
                }
                value = false
              })
            }
          })
        }
      }
  }
}

 //only check condition of option group and its variation other then that all done
  // removeProduct(product) {
  //   console.log(product)
  //   let data;
  //   let optionData;
  //   let value: boolean = false;
  //   product.optionGroup.slice(-1).forEach((cartGroup: any) => {
  //    data = cartGroup;
  //   });

  //   for (let [index, p] of this.cart.entries()) {
  //     p.optionGroup.slice(-1).forEach((cartGroup: any) => {
  //       optionData = cartGroup;
  //      });
  //   if(product.id === p.id && product.optionGroup.length === 0){
  //     // alert('if works');
     
  //       if (p.id === product.id) {
  //         this.cartItemCount.next(this.cartItemCount.value - product.quantity);
  //         this.cart.splice(index, 1);
  //         this.getTotalQuantityOfPerticalItem(product)
  //         p.quantity = this.makeZero
  //       }
  //     }
  //     else if(p.id === product.id && data.length === 0 && optionData.length === 0){
  //       // alert('if e else works');
  //    if(data.length > 0){
  //     // alert(' array with add')
  //         product.optionGroup.slice(-1).forEach((itemGroup: any) => {
  //           let value: boolean = false;
  //           for (let [index, cartItem] of this.cart.entries()) {
  //             cartItem.optionGroup.slice(-1).forEach((cartGroup: any) => {
  //               // alert('wrksm')
  //               for(var i = 0; i<itemGroup.length; i++){
  //                 for(var j = 0; j<cartGroup.length; j++){
  //                   if (cartItem.id === product.id && itemGroup[0].name === cartGroup[0].name && itemGroup[i].name === cartGroup[j].name && itemGroup.length === cartGroup.length) {
  //                     optionData = cartItem.quantity;
  //                     // alert('if works ')
  //                     // this.cart.splice(index, 1);
  //                     // this.getTotalQuantityOfPerticalItem(product)
  //                     // cartItem.quantity = this.makeZero;
  //                     value=true;
  //                   }
                    
  //                 }
  //               }
               
  //               if (value) {
  //               cartItem.quantity = 0;
  //               //  console.log(data)
  //               }
  //               if (cartItem.quantity === 0) {
  //                 product.quantity = 0;
  //                 this.cartItemCount.next(this.cartItemCount.value - optionData);
  //                 let filteredProduct = this.commonService.get_itemList();
  //                 filteredProduct.forEach((data: any) => {
  //                   if(product.id === data.id){
  //                     this.getTotalQuantityOfPerticalItem(data)
  //                   }
  //                 });
  //                let data =  this.cart.splice(index, 1);
  //               }
  //               value = false
  //             })
            
  //           }
  //         })
  //       }
  //       else if(data.length === 0){
  //         alert('blank array without add')
  //         product.quantity = 0;
  //         if(product.optionGroup.length > 0){
  //           for (let [index, p] of this.cart.entries()) {
  //             if (p.id === product.id) {
  //               this.cartItemCount.next(this.cartItemCount.value - product.quantity);
  //               this.cart.splice(index, 1);
  //               this.getTotalQuantityOfPerticalItem(product)
  //               p.quantity = this.makeZero
              
  //             }
  //           }
  //         }
  //       }
  //     }
  //     else if(p.id === product.id && data.length > 0 && optionData.length > 0){
  //       alert('last else works')
  //       if(data.length > 0){
  //         alert(' array with add')
  //             product.optionGroup.slice(-1).forEach((itemGroup: any) => {
  //               let value: boolean = false;
  //               for (let [index, cartItem] of this.cart.entries()) {
  //                 cartItem.optionGroup.slice(-1).forEach((cartGroup: any) => {
  //                   // alert('wrksm')
  //                   for(var i = 0; i<itemGroup.length; i++){
  //                     for(var j = 0; j<cartGroup.length; j++){
  //                       if (cartItem.id === product.id && itemGroup[0].name === cartGroup[0].name && itemGroup[i].name === cartGroup[j].name && itemGroup.length === cartGroup.length) {
  //                         optionData = cartItem.quantity;
  //                         // alert('if works ')
  //                         // this.cart.splice(index, 1);
  //                         // this.getTotalQuantityOfPerticalItem(product)
  //                         // cartItem.quantity = this.makeZero;
  //                         value=true;
  //                       }
                        
  //                     }
  //                   }
                   
  //                   if (value) {
  //                   cartItem.quantity = 0;
  //                   //  console.log(data)
  //                   }
  //                   if (cartItem.quantity === 0) {
  //                     product.quantity = 0;
  //                     this.cartItemCount.next(this.cartItemCount.value - optionData);
  //                     let filteredProduct = this.commonService.get_itemList();
  //                     filteredProduct.forEach((data: any) => {
  //                       if(product.id === data.id){
  //                         this.getTotalQuantityOfPerticalItem(data)
  //                       }
  //                     });
  //                    let data =  this.cart.splice(index, 1);
  //                   }
  //                   value = false
  //                 })
                
  //               }
  //             })
  //           }
  //           else if(data.length === 0){
  //             alert('blank array without add')
  //             if(product.optionGroup.length > 0){
  //               for (let [index, p] of this.cart.entries()) {
  //                 if (p.id === product.id && data.length === 0) {
  //                   this.cartItemCount.next(this.cartItemCount.value - product.quantity);
  //                   this.cart.splice(index, 1);
  //                   this.getTotalQuantityOfPerticalItem(product)
  //                   p.quantity = this.makeZero
  //                 }
  //               }
  //             }
  //           }
  //     }
  // }
 
  //   // let filteredProduct = this.commonService.get_itemList()
  //   // filteredProduct.forEach((res: any) => {
  //   //     if(product.id === res.id){
  //   //       res.quantity = 0;
  //   //     }
  //   // })
  // }

  //remove all product from the cart after order and make the cart zero
  makeCartEmpty(){
    let filteredProduct = this.commonService.get_itemList();
    filteredProduct.forEach((res: any) => {
      for (let [index, p] of this.cart.entries()) {
        this.cart.splice(index, 1);
        p.quantity = this.makeZero;
        // res.quantity = 0;
      }
      this.cartItemCount.next(0);
      res.quantity = 0;
    });
  }

  makeUnique(){ 
    let cities = [...new Set(this.cart)]
    this.cart = []
    cities.forEach((data: any) => {
      this.cart.push(data);
    })
    
  }
}























//  otpUrl = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/otp/generateOtp";
  // signUpUrl = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/auth/authenticate";
  // registerUrl = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/auth/register";

  // getItems = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/all";
  // getCatagoryOne = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/category/";

  // userVarification = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/user/verify/user";
  // otpVarification = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/otp/mobile/authenticate";

  // fetchImagesUrl = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/files/cheesePizzaS.jpg"

  // // saveUserInformation = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/user/update';
  // saveUserInformation = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/user/update';
  // getAllUserInfor = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/user/all";


  // // http://164.52.194.61:8081/eRe4u-third-party-staging/user/save
  // saveOrders = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/save';
  // getOrders = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order';

  // getUserDeatilsByName = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/user/';
  
  // getOrdersById = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/getOne/";

  // getAllOrderOfUser = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/data/';

  // getOrderDelails = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/getOne/';
  // // getOrderDetails = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/data/1/'

  // fetchPerticularItemById = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/';

  // parentCategoryApi = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/category/'

  // deleteOrder = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/";

  // cancleOrderStatusApi = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/status/';

  // passwordChange = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/user/changePassword/"

  // fetchAddOns = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/'

  // forgotPasswordOtp = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/otp/forgatePassword'

  // forgotPassword = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/auth/register'

  // fetchState = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/stateWiseCity/all";

  // fetchCities = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/stateWiseCity/"

  // pincodeApi = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/pincode/code/'

  //  upload items api
  //  https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/12

  // otpUrl = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/otp/generateOtp"; // done
  // signUpUrl = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/auth/authenticate"; //done
  // registerUrl = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/auth/register"; //done

  // getItems = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/all"; //done
  // getCatagoryOne = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/category/"; //done

  // userVarification = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/user/verify/user"; //done
  // otpVarification = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/otp/mobile/authenticate"; //done

  // fetchImagesUrl = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/files/cheesePizzaS.jpg" // npt done

  // saveUserInformation = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/user/update'; //done
  // getAllUserInfor = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/user/all"; //done

  // saveOrders = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/save'; //done
  // getOrders = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order'; //done

  // getUserDeatilsByName = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/user/'; //done
  // // chrome://flags/
  // getOrdersById = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/getOne/"; //done

  // getAllOrderOfUser = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/data/'; //doone

  // getOrderDelails = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/getOne/'; //done
  // // getOrderDetails = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/data/1/'

  // fetchPerticularItemById = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/'; //done

  // parentCategoryApi = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/category/' //done

  // deleteOrder = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/"; //done

  // cancleOrderStatusApi = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/status/'; //done
  // // cancleOrderStatusApi = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/status/';

  // passwordChange = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/user/changePassword/" //done

  // fetchAddOns = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/' //done

  // forgotPasswordOtp = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/otp/forgatePassword' //done

  // forgotPassword = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/auth/register' //done

  // fetchState = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/stateWiseCity/all"; //done

  // fetchCities = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/stateWiseCity/" //done

  // pincodeApi = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/pincode/code/' //done