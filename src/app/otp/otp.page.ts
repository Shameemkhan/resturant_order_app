import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { CallApisService } from '../call-apis.service';
import { Subscription } from 'rxjs';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.page.html',
  styleUrls: ['./otp.page.scss'],
})
export class OtpPage implements OnInit{
  otpData: any[] = [];
  otp1: string;
  otp2: string;
  otp3: string;
  otp4: string;
  otpl: any[] = [];
  otpValue: string = '';
  moNumber: string = '';
  otp: string = '';
  password: any;
  customerId: any;
  showNotVerified: boolean = false;
  submitted: boolean = false;
  otpExpired: boolean = false;

  constructor(private apiService: CallApisService, private router: Router,
    public alertController: AlertController, public formBuilder: FormBuilder) { }
 
  form =  this.formBuilder.group({
    otp: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")])
  });

  ngOnInit() {
    this.apiService.typeValue.subscribe((res: any) => {
      this.moNumber = res;
    });

    this.apiService.pass.subscribe((res: any) => {
      this.password = res;
    });
  }

  f(controls) {
    return this.form.get(controls);
  }
 
  gotoSignUp(){
    this.router.navigate(['/signup'])
  }

  onSubmit(){
    this.submitted = true;

    if(this.form.invalid){
      return
    }

    // let userPass = this.password.toString();
    // console.log(this.password); 
    this.otp = this.form.get('otp').value;
    console.log(this.otp);
    // console.log(typeof(this.password));
    this.apiService.varifyOtp(this.moNumber, this.otp).subscribe((res: any) => {
      if(res === 2001){
        this.apiService.signUp(this.moNumber, this.password)
        .subscribe((res: any) => {
          this.apiService.userId.next(res.userId);
          this.customerId = res.userId;
          localStorage.setItem('userId', this.customerId);
          this.apiService.login(this.moNumber, this.password)
          .subscribe(result => {
              localStorage.setItem('moNumber', this.moNumber);
            this.router.navigate(['/address-details']);
          }
          ,(err)=> {
            console.log(err);
          })
        });
      
      }
      else if(res === 2002){
        this.showNotVerified = true;
      }
      else if(res === 2003){
        this.otpExpired = true;
      }
    })
  }


  // forgotPassword(){
   
  //   console.log(this.otp);
  //   this.apiService.varifyOtp(this.moNumber, this.otp).subscribe((res: any) => {
  //     if(res === 2001){
  //       this.apiService.signUp(this.moNumber, this.password)
  //       .subscribe((res: any) => {
  //         this.apiService.userId.next(res.userId);
  //         console.log(res);
  //         this.apiService.login(this.moNumber, this.password)
  //         .subscribe(result => {
  //           console.log(result)
  //           this.router.navigate(['/address-details']);
  //         }
  //         ,(err)=> {
  //           console.log(err);
  //         })
  //       });
      
  //     }
  //     else if(res === 2002){
  //       alert('otp is not verified')
  //     }
  //     else if(res === 2003){
  //       alert("your otp is expired please re-generate the otp")
  //     }
  //   })
    // this.otpData.push(this.otp1, this.otp2, this.otp3, this.otp4);
    // for(var i = 0; i < this.otpData.length; i++)
    // {
    //    this.otpl.push(this.otpData[i]);
    // }
    //   this.otpValue = this.otpl.toString();
    //   var verifyOtp = this.otpValue.split(',').join("");
    //   this.apiService.otp.next(verifyOtp);
    //   console.log(verifyOtp);
    //   console.log(this.moNumber);
    //   this.apiService.varifyOtp(this.moNumber, verifyOtp)
    //         .subscribe(res => {
    //           this.otpData = [];
    //           this.otpl = [];
    //           if(res === 2001){
    //             this.apiService.signUp(this.moNumber, this.password)
    //             .subscribe((res: any) => {
    //               this.apiService.userId.next(res.userId);
    //               console.log(res);
    //               this.apiService.login(this.moNumber, this.password)
    //               .subscribe(result => {
    //                 console.log(result)
    //                 this.router.navigate(['/address-details']);
    //               })
    //             });
              
    //           }
    //           else if(res === 2002){
    //             alert('otp is not verified')
    //           }
    //           else if(res === 2003){
    //             alert("your otp is expired please re-generate the otp")
    //           }
    //           console.log(res)
    //         },
    //         (err: any) => {
    //           console.log("error" +err);
    //         });
      //  }

       register(){
        this.apiService.signUp(this.moNumber, this.password)
        .subscribe((res: any) => {
          alert('register works')
        }
        ,err => {
          console.log(err);
        });
       }

       login(){
        this.apiService.login(this.moNumber, this.password)
        .subscribe(result => {
          alert('login works')
        },
        (err) => {})
       }

       resendPassword(){
        this.apiService.getOtp(this.moNumber).subscribe(async (res: any) => {
          console.log(res)
          if(res){
            const alert = await this.alertController.create({
              cssClass: 'my-custom',
              header: 'Alert',
              message: 'Otp successfully sent please check your mobile!',
              buttons: [{
                text: 'OK',
              }]
            });
        
            await alert.present();
          }
        },
        (err) => {
          console.log(err);
        });
       }

       makeValueFalse(){
         this.otpExpired = false
         this.showNotVerified = false;
       }
    }

 