import { TestBed } from '@angular/core/testing';

import { IonicHttpService } from './ionic-http.service';

describe('IonicHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IonicHttpService = TestBed.get(IonicHttpService);
    expect(service).toBeTruthy();
  });
});
