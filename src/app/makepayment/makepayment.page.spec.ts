import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MakepaymentPage } from './makepayment.page';

describe('MakepaymentPage', () => {
  let component: MakepaymentPage;
  let fixture: ComponentFixture<MakepaymentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MakepaymentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MakepaymentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
