import { DialogPage } from './../dialog/dialog.page';
import { Items } from './../models/items';
import { CallApisService } from './../call-apis.service';
import { Router } from '@angular/router';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CartService } from '../services/cart.service';
import { MatDialog } from '@angular/material/dialog';
import { CalculationService } from '../calculation.service';

@Component({
  selector: 'app-makepayment',
  templateUrl: './makepayment.page.html',
  styleUrls: ['./makepayment.page.scss'],
})
export class MakepaymentPage implements OnInit {
  cart: Items[] = [];
  qty=[];
  products = [];
  cartItemCount: BehaviorSubject<number>;
  totalAmt: any = 0;
  name: any = '';
  dis: any = '';
  curDay = '';
  curDate = '';
  value_selected: any = '0';
  tomDay = '';
  tomDate = '';
  dayAfterTomDay = '';
  dayAfterTomDate = '';
  paymentType: any[] = [];
  grandTotalAmount: number;


  // order: any = {
  //     currentState: 'Active',
  //     // details: "api done"
  //     items: [],
  //     storeId: '1'
  // }
  
  @ViewChild('cart', {static: false, read: ElementRef})fab: ElementRef;
  food: string = "Breakfast&Diary";
  foods:string = "brft1"; 
  sizeArray: any;
  modalCtrl: any;
  constructor(private cartService: CartService, 
    private router: Router, 
    private apiService: CallApisService,
    private dialog: MatDialog,
    public calculationService: CalculationService) { }

  ngOnInit() {

    const today =  new Date();
    const today2 =  new Date();
    let currentDay = today.toString();
    this.curDay = currentDay.substr(0, 3)
    this.curDate = currentDay.substr(3, 8)

    const tomorrow =  new Date(today.setDate(today.getDate() + 1));
    let tomStrDate = tomorrow.toString();
    this.tomDay = tomStrDate.substr(0, 3)
    this.tomDate = tomStrDate.substr(3, 8)

    const dayAfTom =  new Date(today2.setDate(today2.getDate() + 2));
    let dayStrTom = dayAfTom.toString();
    this.dayAfterTomDay = dayStrTom.substr(0, 3);
    this.dayAfterTomDate = dayStrTom.substr(3, 8);


    this.cart = this.cartService.getCart();


    this.apiService.customerId.subscribe((res: any) => {

    })

    this.apiService.getOrdersData()
    .subscribe(res => {
    });
  }

  getIonRadioValue(typeName){
    this.calculationService.setTypeName(typeName)
  }

  ionViewWillEnter(){
    this.apiService.grandTotal.subscribe((grandTotal: any) => {
      this.grandTotalAmount = grandTotal;
    })
    this.paymentType = this.calculationService.getPaymentList();
    for(let i=0; i<this.paymentType.length; i++){
      this.calculationService.setTypeName(this.paymentType[0].paymentType)
    }
  }


  // getTotal() {
  //   return this.cart.reduce((i, j) => i + j.price * j.amount, 0);
  // }

  getTotal() {
    // return this.cart.reduce((i, j) => i + j.price * j.discountPrice, 0);
    let total: any = 0;
    for(var i = 0; i<this.cart.length; i++){
      if(this.cart[i].price){
        total += (this.cart[i].price - this.cart[i].discountPrice) * this.cart[i].quantity;
    
        this.totalAmt = total;
      }
    }
    
    return total;
  }

 orderPlaced(){
    // this.apiService.generateOrder(this.order);
   let dialog = this.dialog.open(DialogPage)
  } 

  // payAmount(){
  //   this.router.navigate(['/orderplaced'])
  // }

  getCurrentDate(){
    const today =  new Date();
    let currentDay = today.toString();
    let curDateAndTime = currentDay.substr(0, 24)
    this.apiService.date.next(curDateAndTime);

  }

  gotoCoupon(){
    this.router.navigate(['/coupon'])
  }

  getTomorrowDate(){
    const today =  new Date();
    const tomorrow =  new Date(today.setDate(today.getDate() + 1));
    let tomorrowDateOfString = tomorrow.toString();
    
    let tomorrowDate = tomorrowDateOfString.substr(0, 24);
    this.apiService.date.next(tomorrowDate);
  }

  getDayAfterTommowDate(){
    const today =  new Date();
    const tomorrow =  new Date(today.setDate(today.getDate() + 2));
    let tomorrowDateOfString = tomorrow.toString();
    let tomorrowDate = tomorrowDateOfString.substr(0, 24);
    this.apiService.date.next(tomorrowDate);
  }

  
}