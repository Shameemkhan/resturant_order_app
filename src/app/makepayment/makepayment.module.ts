import { DialogPage } from './../dialog/dialog.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MakepaymentPageRoutingModule } from './makepayment-routing.module';

import { MakepaymentPage } from './makepayment.page';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MakepaymentPageRoutingModule,
    MatDialogModule,
    MatButtonModule
  ],
  declarations: [MakepaymentPage],
  // entryComponents: []
})
export class MakepaymentPageModule {}
