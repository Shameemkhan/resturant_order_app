import { ConnectivityServiceService } from './connectivity-service.service';
import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { BehaviorSubject } from 'rxjs';
declare var google;

const GOOGLE_MAPS_API_KEY = 'AIzaSyCaKbVhcX_22R_pRKDYuNA7vox-PtGaDkI';

export type Maps = typeof google.maps;
@Injectable({
  providedIn: 'root'
})
export class GoogleMapsService {
  mapElement: any;
  pleaseConnect: any;
  map: any;
  mapInitialised: boolean = false;
  mapLoaded: any;
  mapLoadedObserver: any;
  currentMarker: any;
  apiKey: string = "AIzaSyDa9IUG9jYDagAqeTX36j0mIUYO5a-jWhU";

  public lang = new BehaviorSubject<number>(0);
  public lati = new BehaviorSubject<number>(0);

  constructor(public connectivityService: ConnectivityServiceService, public geolocation: Geolocation) {}

  init(mapElement: any, pleaseConnect: any): Promise<any> {

    this.mapElement = mapElement;
    this.pleaseConnect = pleaseConnect;

    return this.loadGoogleMaps();

  }

  loadGoogleMaps(): Promise<any> {

    return new Promise((resolve) => {

      if(typeof google == "undefined" || typeof google.maps == "undefined"){

        console.log("Google maps JavaScript needs to be loaded.");
        this.disableMap();

        if(this.connectivityService.isOnline()){

          window['mapInit'] = () => {

            this.initMap().then(() => {
              resolve(true);
            });

            this.enableMap();
          }

          let script = document.createElement("script");
          script.id = "googleMaps";

          if(this.apiKey){
            script.src = 'https://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit&libraries=places';
          } else {
            script.src = 'https://maps.google.com/maps/api/js?callback=mapInit';       
          }
          document.body.appendChild(script);  
        } 
      } else {
        if(this.connectivityService.isOnline()){
          this.initMap();
          this.enableMap();
        }
        else {
          this.disableMap();
        }
        resolve(true);
      }
      this.addConnectivityListeners();
    });
  }

  initMap(): Promise<any> {

    this.mapInitialised = true;

    return new Promise((resolve) => {

      this.geolocation.getCurrentPosition().then((position) => {

        let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement, mapOptions);
        this.addMarker(this.map);
        resolve(true);

      });

    });

  }

   addMakerMax(lat: number, lng: number) {
    //  alert('add MAr')
    const marker = new google.maps.Marker({
      position: { lat, lng },
      map: this.map,
      title: 'taxi',
      icon: { url: "assets/five-star-images/mapIcon.png",
      scaledSize: new google.maps.Size(35, 35) },   
      })

    // i show the alert on mark click yeeeeees <3
    let self = this
      marker.addListener('click', function() {
        console.log("test");
        // self.presentAlert()
      });
    
  }

  addMarker(map:any){

let marker = new google.maps.Marker({
  map: map,
  animation: google.maps.Animation.DROP,
  position: map.getCenter()
});

let content = "<h4>Information!</h4>";

this.addInfoWindow(marker, content);



}

  addInfoWindow(marker, content){

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }

  disableMap(): void {

    if(this.pleaseConnect){
      this.pleaseConnect.style.display = "block";
    }

  }

  enableMap(): void {

    if(this.pleaseConnect){
      this.pleaseConnect.style.display = "none";
    }

  }

  addConnectivityListeners(): void {

    this.connectivityService.watchOnline().subscribe(() => {

      setTimeout(() => {

        if(typeof google == "undefined" || typeof google.maps == "undefined"){
          this.loadGoogleMaps();
        } 
        else {
          if(!this.mapInitialised){
            this.initMap();
          }

          this.enableMap();
        }

      }, 2000);

    });

    this.connectivityService.watchOffline().subscribe(() => {

      this.disableMap();

    });

  }

// public readonly api = this.load();

// private load(): Promise<Maps> {
//   const script = document.createElement('script');
//   script.type = 'text/javascript';
//   script.async = true;
//   script.defer = true;
//   // tslint:disable-next-line:no-bitwise
//   const callbackName = `GooglePlaces_cb_` + ((Math.random() * 1e9) >>> 0);
//   script.src = this.getScriptSrc(callbackName);

//   interface MyWindow { [name: string]: Function; };
//   const myWindow: MyWindow = window as any;

//   const promise = new Promise((resolve, reject) => {
//     myWindow[callbackName] = resolve;
//     script.onerror = reject;
//   });
//   document.body.appendChild(script);
//   return promise.then(() => google.maps);
// }

// private getScriptSrc(callback: string): string {
//   interface QueryParams { [key: string]: string; };
//   const query: QueryParams = {
//     v: '3',
//     callback,
//     key: GOOGLE_MAPS_API_KEY,
//     libraries: 'places',
//   };
//   const params = Object.keys(query).map(key => `${key}=${query[key]}`).join('&');
//   return `//maps.googleapis.com/maps/api/js?${params}&language=fr`;
// }
}
