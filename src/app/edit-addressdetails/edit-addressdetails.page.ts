import { IonicHttpService } from './../ionic-http.service';
import { saveUser } from './../models/saveUser.model';
import { Component, OnInit} from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CallApisService } from '../call-apis.service';

import { ElementRef, NgZone, ViewChild } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { GoogleMapsService } from '../google-maps.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';

declare var google;

@Component({
  selector: 'app-edit-addressdetails',
  templateUrl: './edit-addressdetails.page.html',
  styleUrls: ['./edit-addressdetails.page.scss'],
})
export class EditAddressdetailsPage implements OnInit{
  
  @ViewChild('map', {static: true}) element;
  @ViewChild('map',  {static: true}) mapElement: ElementRef;
  @ViewChild('pleaseConnect',  {static: true}) pleaseConnect: ElementRef;

  latitude: number;
  longitude: number;
  autocompleteService: any;
  placesService: any;
  query: string = '';
  places: any = [];
  searchDisabled: boolean;
  saveDisabled: boolean;
  location: any;  
  selectArea: any;
  pincodeCheck: boolean = false;
  pincodeCheckONSubmit: boolean = false;

  moNumber = '';
  userId = '';
  address1: any= '';
  submitted: boolean = false;
  stateId="1"
  
  states: any[] = [];
  cities: any = {}
  stateName: any;
  name: any

  saveUser :any = {
    userId: '',
    pincode: '',
    fristName: "",
    lastName: "",
    address: "",
    mobileNo: "",
    state: {
      id: '',
      stateName: ''
    },
    city: {
      id: '',
      cityName: ''
    },
    addressUser: [{
      street1: ''
    }]
  };

  form = new FormGroup({
    userId: new FormControl(),
    pincode: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]),
    fristName: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z \-\']+')]),
    lastName: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z \-\']+')]),
    street1: new FormControl('', Validators.required),
    addressUser: new FormArray([
     this.saveAddress()
    ]),
    customer: new FormArray([
     this.addCustomer()
    ]),
    mobileNo: new FormControl(''),
    state: new FormGroup({
      id: new FormControl(''),
      stateName: new FormControl('', Validators.required),
    }),
    city: new FormGroup({
      id: new FormControl(''),
      cityName: new FormControl('', Validators.required),
    })
  },
  {asyncValidators: []})

  constructor(private apiService: CallApisService, 
    private fb: FormBuilder, 
    private route: ActivatedRoute,
    private ionicHttp: IonicHttpService,
    private router: Router,
    private navCtrl: NavController,
     private zone: NgZone, 
     private maps: GoogleMapsService, private platform: Platform,private geolocation: Geolocation) {
      this.searchDisabled = true;
      this.saveDisabled = true;
      }

  ngOnInit() {
  }

  ionViewWillEnter(){
      this.userId =  this.route.snapshot.paramMap.get('userId');
    this.apiService.userId.next(this.userId);

    this.moNumber = localStorage.getItem('moNumber');

    this.apiService.getUserData(this.moNumber).subscribe((res: any) => {
      this.saveUser = res;  
      this.saveUser.addressUser.forEach((data: any) => {
        this.selectArea = data.street1;
      })
    });
    
  
     
     //uncoomnet this to show map
     let mapLoaded = this.maps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement).then(() => {
      this.autocompleteService = new google.maps.places.AutocompleteService();
      this.placesService = new google.maps.places.PlacesService(this.maps.map);
      this.searchDisabled = false;
    }); 

    
    this.apiService.getStates().subscribe((states: any) => {
      this.states = states;
    })

    this.ionicHttp.getUserDetails(this.moNumber).then((res: any) => {
      let data = JSON.parse(res.data)
      this.saveUser = data;
      this.saveUser.addressUser.forEach((data: any) => {
       this.selectArea = data.street1;
     });
      })
      .catch(err => {
        console.log(err)
      })

      setTimeout(() => {
        this.states.forEach((states: any) => {
          if(this.saveUser.state === states.id){
            alert(states.stateName);
          //  this.stateName =  states.stateName
          }
        })
      }, 500)  
  }

  
  f(controls) {
    return this.form.get(controls);
  }

  s(controls){
    return this.form.controls.state.get(controls);
  }

  c(controls){
    return this.form.controls.city.get(controls);
  }
 
  filterState(){
    let states = this.form.controls.state.get('stateName').value
    this.stateName = states
    this.saveUser.state = this.stateName
    this.cities = states;
    this.form.controls.state.value.id = this.saveUser.state.id
      this.form.controls.state.value.stateName = this.saveUser.state.stateName;
 }

 saveAddress(){
  return this.fb.group({
    street1: this.address1
  })
}

 filterCity(){
  let cityName = this.form.controls.city.get('cityName').value;
  this.saveUser.city = cityName;
  this.form.controls.city.value.id = this.saveUser.city.id
  this.form.controls.city.value.cityName = this.saveUser.city.cityName;
 }

 addCustomer(){
  return this.fb.group({
    email: '',
    address: {
      "city": 15,
      "is_guest_mode": "",
      "landmark": "",
      "latitude": "",
      "line_1": this.address1,
      "line_2": "Bhopal, Madhya Pradesh, India",
      "longitude": "",
      "pin": 41,
      "sub_locality": "",
      "tag": ""
    }
  })
}

 applyValidation(){
  this.pincodeCheckONSubmit = false;
  let value = this.form.get('pincode').value;
  let storeId = localStorage.getItem('storeId')
  this.apiService.getPincodeDEtails(this.form.get('pincode').value, storeId).subscribe((data: any) => {
    if(data === null){
      this.pincodeCheck = true;
  }
  else{
    this.pincodeCheck = false;
    }
  })
}

checkValue(){
  this.pincodeCheck = false;
  this.pincodeCheckONSubmit = false;
}

   onSubmit(){
      this.submitted = true;
      this.address1 = this.form.get('street1').value;
      this.form.value.addressUser[0].street1 = this.form.get('street1').value;
      this.form.value.userId = this.userId;
      this.form.controls.state.value.id = this.saveUser.state.id
      this.form.controls.state.value.stateName = this.saveUser.state.stateName;
      this.form.controls.city.value.id = this.saveUser.city.id
      this.form.controls.city.value.cityName = this.saveUser.city.cityName;

      this.form.value.customer[0].address.line_1 = this.address1;
      this.form.value.customer[0].address.line_2 = this.address1;
      this.form.value.customer[0].address.landmark = this.address1;
      this.form.value.customer[0].name = this.form.get('fristName').value + " " + this.form.get('lastName').value ;
      this.form.value.customer[0].phone = this.moNumber;
      this.form.value.customer[0].address.pin = this.form.get('pincode').value;
      this.form.value.mobileNo = this.moNumber;
      let storeId = localStorage.getItem('storeId')
     
      if(this.form.invalid){
         return;
      }
      
     
    
      this.apiService.getPincodeDEtails(this.form.value.pincode, storeId).subscribe((data: any) => {
        if(data != null){
            this.apiService.saveUser(this.form.value).subscribe((res: any) => {
              this.router.navigate(['/delivery-address'])
          }, err => {
            console.log(err);
          });
        }
        else{
          this.pincodeCheck = false
          this.pincodeCheckONSubmit = true;
        }
      })

      // this.address1 = this.form.get('street1').value
      // this.form.value.addressUser[0].street1 = this.address1
      // this.form.value.userId = this.userId
      // this.form.controls.state.value.id = this.saveUser.state.id
      // this.form.controls.state.value.stateName = this.saveUser.state.stateName;
      // this.form.controls.city.value.id = this.saveUser.city.id
      // this.form.controls.city.value.cityName = this.saveUser.city.cityName;
  
      // this.ionicHttp.saveUser(this.form.value).then((res: any) => {
      //   console.log(res)
      //   if(res){
      //     this.router.navigate(['/delivery-address']);
      //   }
      // })
      // .catch((err: any) => {
      //   console.log(err);
      // })
    }

  CreateUser(){
    return this.fb.group({
      street1: this.address1
    })
  }
  
  saveUserData(){
    this.address1 = this.form.get('street1').value
    this.form.value.addressUser[0].street1 = this.address1
    this.form.value.userId = this.userId

    this.ionicHttp.saveUser(this.form.value).then((res: any) => {
      if(res){
        this.router.navigate(['/delivery-address']);
      }
    })
    .catch((err: any) => {
      console.log(err);
    })
  }

  selectPlace(place){
    this.places = [];
  
    let location = {
        lat: null,
        lng: null,
        name: place.name
    };

    this.placesService.getDetails({placeId: place.place_id}, (details) => {

        this.zone.run(() => {

            location.name = details.name;
            location.lat = details.geometry.location.lat();
            location.lng = details.geometry.location.lng();
            this.saveDisabled = false;
            this.maps.lati.next(location.lat);
            this.maps.lang.next(location.lng);
            this.maps.map.setCenter({lat: location.lat, lng: location.lng}); 
            this.maps.addMakerMax(location.lat, location.lng);
            this.location = location;

        });

    });

    this.selectArea = place.description;
}

searchPlace(){

    this.saveDisabled = true;

    this.saveUser.addressUser.forEach((data: any) => {
      if(data.street1.length > 0 && !this.searchDisabled) {

        let config = {
            types: ['geocode'],
            input: data.street1
        }

        this.autocompleteService.getPlacePredictions(config, (predictions, status) => {

            if(status == google.maps.places.PlacesServiceStatus.OK && predictions){

                this.places = [];

                predictions.forEach((prediction) => {
                    this.places.push(prediction);
                });
            }

        });

    } else {
        this.places = [];
    }
    })
    

}

}
