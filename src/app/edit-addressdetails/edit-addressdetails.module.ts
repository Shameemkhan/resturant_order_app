import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditAddressdetailsPageRoutingModule } from './edit-addressdetails-routing.module';

import { EditAddressdetailsPage } from './edit-addressdetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    EditAddressdetailsPageRoutingModule
  ],
  declarations: [EditAddressdetailsPage]
})
export class EditAddressdetailsPageModule {}
