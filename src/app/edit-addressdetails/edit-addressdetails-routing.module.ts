import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditAddressdetailsPage } from './edit-addressdetails.page';

const routes: Routes = [
  {
    path: '',
    component: EditAddressdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditAddressdetailsPageRoutingModule {}
