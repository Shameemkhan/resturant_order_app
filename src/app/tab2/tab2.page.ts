import { EditAddOnsPage } from './../edit-add-ons/edit-add-ons.page';
import { AddOnsPage } from './../add-ons/add-ons.page';
import { SearchModalPagePage } from './../search-modal-page/search-modal-page.page';
import { CommonService } from './../common.service';
import { category } from './../models/category';
import { Items } from './../models/items';
import { Component, ViewChild, ElementRef, Pipe, Directive, OnDestroy } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';

import { BehaviorSubject, Observable, Observer, Subscription } from 'rxjs';
import { CartService } from '../services/cart.service';
import { CallApisService } from '../call-apis.service';

import { Router, ActivatedRoute } from '@angular/router';
import { Platform } from '@ionic/angular';
import { ImagePreviewPage } from '../image-preview/image-preview.page';
import { CalculationService } from '../calculation.service';

  // this.filteredProduct.push(p);
  // this.have = [...this.filteredProduct];
  // this.itemList.prototype.push.apply(this.filteredProduct);

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnDestroy{
  cart = [];
  qty=[];
  products = [];
  cartItemCount: any;
  spinner: boolean = true;
  firstSub: Subscription;
  secSub: Subscription;
  thirdSub: Subscription;
  fourthSub: Subscription;
  showPreview: boolean = false;
  
  @ViewChild('cart', {static: false, read: ElementRef})fab: ElementRef;
  food: string = "Breakfast&Diary";
  foods:string = "brft1"; 
  sizeArray: any;
  modalCtrl: any;
  images: any[] = [];
  itemSize = 80;
  imageName: any[] = [];
  // itemSubscription: Subscription;
  // catagorySubscription: Subscription;
  // imageSubscription: Subscription;
  urlCategory;
  categoryList: any[] = [];
  selectedAttr: any;
  itemList: Items[] = [];
  catagory: category[] = [];
  totalQty: number = 0;
  totalAmt: any = 0;
  cartCount = 0;
  saveItem: any[] = [];
  filteredProduct: any[] = [];
  searchText: string = '';
  productsData: any[] = []
  name: any = '';
  image: any;
  base64Image: any;
  imageName2: any[]= [];
  nameImage: any;
  imageUrl: any;
  popularItems: any[] = [];
  product: any = ''
  cartValue: any;
 itemValue: any;
 qty1: any;
 childCategory: any[] = [];
 disabled: boolean;
 enabled: boolean;
 cartData: any;
 storeId: number = 8;
 itemData: any;
 value: boolean = false;
 parentCategoryName = 'BREAKFAST';
 item: any;
 cat: any;
 parentCategory: any[] = [];
img: any[] = []
defaultImage: any = ''
checkParentCategory: any = {}
constructor(private route: ActivatedRoute,
  private router: Router,
   public modalController:ModalController,
    public alertCtrl: AlertController,
     private cartService: CartService,
      private apiService: CallApisService,
      public commonService: CommonService,
      private platform: Platform,
      private calculationService: CalculationService
      )
       {
        this.spinner = this.commonService.getSpinner();
        this.food = "Breakfast&Diary";this.foods = "brft1";
        this.item = 'ALL'



        let total: number = 0;
        this.calculationService.getObservable()
        .subscribe((chcBackPressed: any) => {
          if(chcBackPressed === 'backPressed'){
            this.setQtyToProduct();
          }
        });
        
    
        
      }
 
  ngOnInit() {
  //  this.firstSub =  this.commonService.itemName.subscribe((res: any) => {
  //     this.searchText = res;
  //  })
  //   console.log(this.imageName2)
  //   this.imageUrl = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/files/';
  //   this.loadItems()
  //   this.cart = this.cartService.getCart();


  //   this.cart.forEach((res: any) => {
  //     this.cartData = res;
  //   });

  //   console.log(this.filteredProduct)
  //   this.filteredProduct.forEach((res: any) => {
  //     this.itemData = res;
  //   })
  //   this.cartItemCount = this.cartService.getCartItemCount();
    
  //    console.log(this.filteredProduct)
  //    this.makeUnique()
  //    this.catagory = this.commonService.get_categoryList();
  //    this.catagory.forEach((category: any) => {
  //      if(category.description != "Category"){
  //        this.parentCategory.push(category)
  //      }
  //    })
  //   console.log(this.catagory)
    
  //   for(let i=0; i< this.parentCategory.length;i++){
  //     this.filteredProduct = [];
  //     this.commonService.get_itemList().forEach((element: any) => {
  //        element.categories.forEach((c: any) => {
  //          this.checkParentCategory = this.parentCategory[0]
  //         if(this.parentCategory[0].ref_id === c.parent_ref_id){
  //           this.filteredProduct.push(element);
  //           this.makeUnique()
  //         }
  //       });
  //     });
  //   }

  //   this.childCategory = []
  //   this.catagory.forEach((element: any) => {
  //     element.category.forEach((c: any) => {
  //       for(let i=0; i<this.parentCategory.length;i++){
  //         if(this.parentCategory[0].ref_id === c.parent_ref_id){
  //           this.childCategory.push(c);
  //           this.makeCategoryUnique()
  //         }
  //       }
      
  //     })
  //   })
  //     this.getItemImage();   
  //     this.defaultChildCategory();
  //     // this.makeUnique();

  } 


  ionViewWillEnter(){
    // this.spinner = this.commonService.getSpinner();
    // alert(this.spinner)
    this.loadItems();
      this.firstSub =  this.commonService.itemName.subscribe((res: any) => {
        // alert(res)
        this.searchText = res;
     })
     
      this.cart = this.cartService.getCart();
      
  
      this.cart.forEach((res: any) => {
        this.cartData = res;
      });

      this.filteredProduct.forEach((res: any) => {
        this.itemData = res;
      })
      this.cartItemCount = this.cartService.getCartItemCount();
       this.makeUnique()
       this.catagory = this.commonService.get_categoryList();
       this.catagory.forEach((category: any) => {
         if(category.description != "Category"){
           this.parentCategory.push(category)
           this.makeParentCateUnique();
         }
       })
   
      for(let i=0;i<this.parentCategory.length;i++){
        this.cat = this.parentCategory[0].name;
      }
     
      if(this.parentCategory.length != 0){
        for(let i=0; i< this.parentCategory.length;i++){
          this.filteredProduct = [];
          this.commonService.get_itemList().forEach((element: any) => {
            if(element.categories != null){
              element.categories.forEach((c: any) => {
                this.checkParentCategory = this.parentCategory[0]
              
               if(this.parentCategory[0].ref_id === c.parent_ref_id){
                 this.filteredProduct.push(element);
                 this.loadItems();
                 this.getItemImage(); 
                 this.makeUnique()
               }
               else if(this.parentCategory[0].ref_id != c.parent_ref_id){
                  this.spinner = false
                  // alert(this.spinner)
               }
             });
            }
           
          });
        }
      }
      else{
       
        this.getExistingData();
      }

      this.childCategory = []
      this.catagory.forEach((element: any) => {
        element.category.forEach((c: any) => {
          for(let i=0; i<this.parentCategory.length;i++){
            if(this.parentCategory[0].ref_id === c.parent_ref_id){
              this.childCategory.push(c);
              this.makeCategoryUnique()
            }
          }
        
        })
      })
        // this.getItemImage();   
        this.defaultChildCategory();
        // this.makeUnique();
  
     this.secSub = this.apiService.storeId.subscribe((res: any) => {
        // console.log(res)
      });
    //after ngOnInIt
 
    
    this.cart = this.cartService.getCart();
   
   this.thirdSub = this.apiService.getParentCategoryData()
    .subscribe((res: any) => {
 
    },
    (err: any) => {
     
    });

    this.setQtyToProduct();

    //  if(this.commonService.get_itemList().length === 0){
    //   //  this.router.navigate(['/tabs/tab1'])
    //   alert('if works')
    //   // this.getExistingData();
    //  }
    setTimeout(() => {
      if(this.spinner == true){
        this.spinner = this.commonService.getSpinner();
        this.getExistingData();
      }
    }, 800)
  }

  setQtyToProduct(){
    this.filteredProduct.forEach((data: any) => {
      this.cart.forEach((cartData: any) => {
        if(data.id === cartData.id){
          if(data.optionGroup.length === 0){
            data.quantity = cartData.quantity;
          }
          else{
            this.cartService.getTotalQuantityOfPerticalItem(data);
          }
        }
      });
    });
  }

  ionViewWillLeave(){
    this.searchText = ''
  }

  

  async getImagePreview(){
    const modal = await this.modalController.create({
      component: ImagePreviewPage,
      // cssClass: 'my-modal-css'
    });
    return await modal.present();
  }
  getName(name){
    // console.log(name)
    this.parentCategoryName = name;
  }

  loadItems(){
    // alert('lead items wokrs')
    // this.filteredProduct = this.commonService.get_itemList();
    if(this.commonService.get_itemList().length === 0){
      this.getExistingData();
    }
    else{
      this.spinner = false;
      // alert(this.spinner)
    }
   
  }
  
  // checkCartValue(){
  //     this.cart.forEach((data: any) => {
  //     this.cartValue = data.id;
  //     this.qty = data.discountPrice;
  //     // console.log(qty)
  //   });

  //   this.filteredProduct.forEach((data: any) => {
  //     this.itemValue = data.id;
  //   })

  //   if(this.itemValue === this.cartValue){
  //     alert("under If")
  //     this.filteredProduct.forEach((data: any) => {
  //       data.discountPrice = this.qty
  //       console.log(this.qty)
  //     })
  //   }

  // }
  
  getBase64ImageFromURL(url: string) {
    return Observable.create((observer: Observer<string>) => {
     
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.src = url;  img.src = url;
      if (!img.complete) {
        img.onload = () => {
          observer.next(this.getBase64Image(img));
          observer.complete();
        };
        img.onerror = (err) => {
          observer.error(err);
        };
      } else {
        observer.next(this.getBase64Image(img));
        observer.complete();
      }
    });
    
  }

  getBase64Image(img: HTMLImageElement) {
    var canvas = document.createElement("canvas");
    canvas.width = 180;
    canvas.height = 180;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, 180, 180);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }

  getExistingData(){
    console.log(this.spinner)
    this.spinner = true
    this.apiService.getAllItems().subscribe((itemRes: any[]) => {
      console.log(itemRes)   
      
      if(itemRes){
        this.spinner = false;
       let res = itemRes.filter((data: any) => {
         return data.storeId === this.storeId
      })

      
      res.forEach((res: any) => { 
        if(res.quantity === null){
          res.quantity = 0;
        }
        if(res.categories === null){

       }
       else{
        res.categories.forEach((data: any) => {
          if(data.name === "Popular"){
            this.popularItems.push(res);
          }
        });
        
       }
     });
       this.commonService.passItems(this.popularItems);
       this.filteredProduct = res;
      //  let imagesForItem: any[] = [];
      //  let images = {
      //    src: ''
      //  }
      //  this.filteredProduct.forEach(data => {
      //    if(data.itemImages.length != 0){
      //      for(let i=0;i<data.itemImages.length;i++){
      //        this.getBase64ImageFromURL(this.imageUrl + data.itemImages[i].img_name).subscribe(base64data => {
      //          data.base64Image = 'data:image/jpg;base64,' + base64data;
      //          images.src = 'data:image/jpg;base64,' + base64data;
      //          imagesForItem.push(images)
      //          data.itemsImages = imagesForItem
      //          let uniqueItemData = [...new Set(imagesForItem)]
      //          imagesForItem = []
      //          uniqueItemData.forEach((data: any) => {
      //            imagesForItem.push(data);
      //          })
      //            this.filteredProduct.push(data);
      //            this.makeUnique()
      //            console.log(this.filteredProduct)
      //          });
      //        }
      //      }
      //  });   
       this.commonService.set_itemList(this.filteredProduct);
       this.setQtyToProduct();
      }
      else{
       this.spinner = true;
       this.commonService.setSpinner(this.spinner);
      }
     
     });

        this.apiService.getCatagories()
       .subscribe((res: any) => {
         this.commonService.set_categoryList(res);
         res.forEach((data: any) => {
           this.apiService.storeId.next(data.storeId)
         });
     },
        (err: any) => {
         console.log(err);
      });

    }


  getItemImage(){
    let imagesForItem: any[] = [];
    let images = {
      src: ''
    }
    this.filteredProduct.forEach(data => {
      if(data.itemImages.length != 0){
        for(let i=0;i<data.itemImages.length;i++){
          this.fourthSub =  this.getBase64ImageFromURL(this.imageUrl + data.itemImages[i].img_name).subscribe(base64data => {
            data.base64Image = 'data:image/jpg;base64,' + base64data;
            images.src = 'data:image/jpg;base64,' + base64data;
            imagesForItem.push(images)
            data.itemsImages = imagesForItem
              this.filteredProduct.push(data);
              this.makeUnique()
            });
          }
        }
    });   
  }


 
  async incementQty(product: any){
    if(product.optionGroup.length > 0){
      if(product.quantity === 0){
        // this.router.navigate(['add-ons', product.id])
        const modal = await this.modalController.create({
          component: AddOnsPage,
          componentProps: {
            "prodId": product.id
          },
          cssClass: 'my-modal-css'
        });
        modal.onDidDismiss().then(() => {
          // alert('works ')
          this.cartService.getTotalQuantityOfPerticalItem(product)
        })
        return await modal.present();
      }
      else{
        // this.router.navigate(['/edit-add-ons']);
        const modal = await this.modalController.create({
          component: EditAddOnsPage,
          componentProps: {
            "prodId": product.id,
            mode: 'ios'
          },
          cssClass: 'my-Edit_AddOns_modal-css'
        });
        modal.onDidDismiss().then(() => {
          this.cartService.getTotalQuantityOfPerticalItem(product)
        })
        return await modal.present();
        // this.cartService.addProduct(product);
        // let cart = this.cartService.getCart();
        // this.filteredProduct.forEach((item: any) => {
        //   cart.forEach((cartItem: any) => {
        //     if(item.id === cartItem.id){
        //       item.quantity = cartItem.quantity;
        //     }
        //   });
        // });
      }
    }
    else{
      this.cartService.addProduct(product);
    }
  }



  async decrementQty(product: any){
  
   if(product.optionGroup.length > 0){
    if(product.quantity > 0){
      const modal = await this.modalController.create({
        component: EditAddOnsPage,
        componentProps: {
          "prodId": product.id
        },
        cssClass: 'my-Edit_AddOns_modal-css'
      });
      modal.onDidDismiss().then(() => {
        this.cartService.getTotalQuantityOfPerticalItem(product)
      })
      return await modal.present();
    }
    product.quantity -=1;
   }
   else{
    this.cartService.decreaseProduct(product)
   }
  
  //  alert(product.quantity)
  //  this.filteredProduct.forEach((item: any) => {
  //    this.cart.forEach((cartItem: any) => {
  //      if(item.id === cartItem.id){
  //         item.quantity  = cartItem.quantity;
  //      }
  //    });
  //    if(item.quantity === 1 && this.cart.length === 0){
  //      item.quantity = 0;
  //    }
  //  });  
  }

  defaultChildCategory(){
    this.catagory.forEach((ele: any) => {
      if(ele.id === 1){
        ele.category.forEach((c: any) => {
          if(ele.ref_id === c.parent_ref_id){
            this.childCategory.push(c);
            this.makeChildCategoryUnique();
          }
        });
      }
    });
  }

  filterChildCategory(attribute){
    this.childCategory = [];
    this.catagory.forEach((element: any) => {
      element.category.forEach((c: any) => {
        if(attribute.ref_id === c.parent_ref_id){
          this.childCategory.push(c);
          this.makeChildCategoryUnique();
        }
        else if(true){
          this.filteredProduct = [];
          this.commonService.get_itemList().forEach((element: any) => {
            if(element.categories != null){
              element.categories.forEach((c: any) => {
                if(attribute.ref_id === c.parent_ref_id){
                  this.filteredProduct.push(element);
                  // this.getItemImage();
                  this.makeUnique()
                }
              });
            }
          });
        }
      })
    })
    this.checkParentCategory = attribute;
  }

  showItems(attribute){
    this.spinner = false;
    if(attribute === 'ALL'){
        this.filteredProduct = [];
        this.commonService.get_itemList().forEach((element: any) => {
          if(element.categories != null){
            element.categories.forEach((c: any) => {
              if(this.checkParentCategory.ref_id === c.parent_ref_id){
                this.filteredProduct.push(element);
                // this.getItemImage();
                this.makeUnique()
              }
            });
          }
        });
      this.searchText = '';
     }
     else{
       this.filteredProduct = [];
       this.searchText = ''
       this.commonService.get_itemList().forEach((element: any) => {
         if(element.categories != null){
          element.categories.forEach((c: any) => {
            if(attribute.name === c.name){
              this.filteredProduct.push(element);
              // this.getItemImage();
              this.makeUnique();
            }
          });
         }
       });
    }
  }

  // decreaseCartItem(product) {
  //   this.cartService.decreaseProduct(product);
  // }

  // increaseCartItem(product) {
  //   this.cartService.addProduct(product);
   
  // }

  addToCart(product) {
    this.cartService.addProduct(product);
   this.animateCSS('tada');
  }

  itemDetailsPageRoute(productId){
    this.searchText = ''
    this.router.navigate(['/item-details/', productId])
  }

  openCart(){
    this.router.navigate(['/tabs/tab4'])
  }

  async openmodal() {
    const modal = await this.modalController.create({
      component: SearchModalPagePage,
      // cssClass: 'my-modal-css'
    });
    return await modal.present();
  }


// async openCart() {
//      this.animateCSS('bounceOutLeft', true);
  
//       let modal = await this.modalCtrl.create({
//         component: CartModalPage,
//         cssClass: 'cart-modal'
//       });
//       modal.onWillDismiss().then(() => {
//         this.fab.nativeElement.classList.remove('animated', 'bounceOutLeft')
//         this.animateCSS('bounceInLeft');
//       });
//       modal.present();
//     }

    getAddOns(product: any){  
      this.router.navigate(['add-ons', product.id])
    }
   
    animateCSS(animationName, keepAnimated = false) {
      const node = this.fab.nativeElement;
      node.classList.add('animated', animationName)
      
      //https://github.com/daneden/animate.css
      function handleAnimationEnd() {
        if (!keepAnimated) {
         node.classList.remove('animated', animationName);
       }
        node.removeEventListener('animationend', handleAnimationEnd)
      }
  node.addEventListener('animationend', handleAnimationEnd)
   }
  //  addons-Modal
async presentHalfModal(){
  // const modal = await this.modalContoller.create({
  //   component:ModalPage,
  //   cssClass:'half-modal'
  // });
  // return await modal.present();
}

makeParentCateUnique(){
  let uniqueItemData = [...new Set(this.parentCategory)]
  this.parentCategory = []
  uniqueItemData.forEach((data: any) => {
    this.parentCategory.push(data);
  })
}

makeUnique(){
  let uniqueItemData = [...new Set(this.filteredProduct)]
  this.filteredProduct = []
  uniqueItemData.forEach((data: any) => {
    this.filteredProduct.push(data);
  })
}

makeChildCategoryUnique(){
  let uniqueItemData = [...new Set(this.childCategory)]
  this.childCategory = []
  uniqueItemData.forEach((data: any) => {
    this.childCategory.push(data);
  })
}

makeCategoryUnique(){
  let uniqueItemData = [...new Set(this.childCategory)]
  this.childCategory = []
  uniqueItemData.forEach((data: any) => {
  
    // console.log(this.filteredProduct);
    this.childCategory.push(data)
    // console.log(data)
    // console.log(this.filteredProduct);
  })
}
  
ngOnDestroy(){
  this.firstSub.unsubscribe();
  this.secSub.unsubscribe();
  this.thirdSub.unsubscribe();
  this.thirdSub.unsubscribe();
  // alert('ng works')
  //   this.searchText = ''
  } 
}