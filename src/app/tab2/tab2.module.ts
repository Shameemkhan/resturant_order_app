import { AddOnsPage } from './../add-ons/add-ons.page';
import { SearchModalPagePageModule } from './../search-modal-page/search-modal-page.module';
import { SearchModalPagePage } from './../search-modal-page/search-modal-page.page';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicImageLoader } from 'ionic-image-loader';
import { FormsModule } from '@angular/forms';
import { Tab2Page } from './tab2.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';
// import { SearchCategoryPipe } from './search-category.pipe';
import { SearchCategoryPipe } from '../search-category.pipe';
import { SearchCategoryPipeModule } from '../search.category.pipe.module';
import {ScrollingModule} from '@angular/cdk/scrolling'; 




@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    IonicImageLoader,
    ScrollingModule,
   
    // SearchModalPagePageModule,
    ExploreContainerComponentModule,
    SearchCategoryPipeModule,
    RouterModule.forChild([{ path: '', component: Tab2Page }])
  ],
  declarations: [
    // AddOnsPage,
    Tab2Page,
    // SearchModalPagePage
  ],
    // entryComponents: [AddOnsPage],
    // entryComponents: [SearchModalPagePage],
    // exports: [SearchModalPagePage]

})
export class Tab2PageModule {}
