import { AuthService } from './../auth.service';
import { AuthData } from './../models/auth-data-model';
import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { CallApisService } from '../call-apis.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Platform } from '@ionic/angular';



@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  moNumber: string;
  inValidLogin: boolean = false;
  submitted: boolean = false;
  userMoNumber: any;
  token: any;

 constructor(private data:UserService,
   private router:Router,
   private navCtrl: NavController, 
   private apiService: CallApisService, 
   private http: AuthService, 
   private platform: Platform,
   public alertController: AlertController){
     
   }


   form = new FormGroup({
        moNumber: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10),  Validators.pattern("^[0-9]*$")])
   });

 ngOnInit() {
  
    
 }

 ionViewWillEnter(){
  this.token = localStorage.getItem('token');
    if(this.token){
       this.inValidLogin = true
       this.userMoNumber = sessionStorage.getItem('moNumber')
     // this.apiService.typeValue
     // .subscribe((res: any) => {
     //    this.userMoNumber = res;
     // });
    }
    else{
       this.inValidLogin = false;
    }

 }

 verifyUser(){
  let token = localStorage.getItem('token');
  if(token){
     this.apiService.typeValue.next(this.moNumber);
       this.router.navigate(['/delivery-address'])
    }
    else{
     this.apiService.typeValue.next(this.moNumber);
     this.http.varifyUser(this.moNumber)
     .then((res: any) => {
        if(res.status === 200)
        {
           this.router.navigate(['/password'])
        }
     })
     .catch((err: any) => {
        if(err.status === 400)
        {
           this.router.navigate(['/signup'])
        }
        else if(err.status === 403){
         this.router.navigate(['/password'])
        }
     });
    }
 }
 

//  numericOnly(event): boolean {
//     alert(event)
//     console.log(event)

//    let patter = /^([0-9])$/;
//     this.result = patter.test(event.key);
//    return this.result;
// }

gotoCheckOut(){
   this.router.navigate(['/check-out'])
}

 callOtpApi(){
  this.moNumber = this.form.get('moNumber').value
  // console.log(this.moNumber)
    let token = localStorage.getItem('token');
    if(token){
     this.apiService.typeValue.next(this.moNumber);
       this.router.navigate(['/delivery-address'])
    }
    else{
     this.apiService.typeValue.next(this.moNumber);
     this.apiService.varifyUser(this.moNumber)
     .subscribe((result : any) => {
        console.log(result);
       },
       (err: any) => {
          console.log(err);
        if(err.status === 200)
        {
         this.router.navigate(['/password'])
        }   
        else if(err.status === 400){
         this.router.navigate(['/signup'])
        }
        else if(err.status === 500){
           alert('Internal Server error')
        }
       });
    }   
 }

 f(controls) {
  return this.form.get(controls);
}


 async onSubmit(){
    this.submitted = true;

    if(this.form.invalid){
       return;
    }
    
    this.moNumber = this.form.get('moNumber').value
    // console.log(this.moNumber)
      let token = localStorage.getItem('token');
      if(token){
       this.apiService.typeValue.next(this.moNumber);
         this.router.navigate(['/delivery-address'])
      }
      else{
       this.apiService.typeValue.next(this.moNumber);
       this.apiService.varifyUser(this.moNumber)
       .subscribe((result : any) => {
          console.log(result);
         },
         (err: any) => {
         if(err.status === 200) {
            this.router.navigate(['/password']);
         }
          else {
            this.router.navigate(['/signup'])
          }  
         });
      }   
 }




  
}
