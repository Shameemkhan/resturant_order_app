import { AddOnsPage } from './../add-ons/add-ons.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditAddOnsPageRoutingModule } from './edit-add-ons-routing.module';

import { EditAddOnsPage } from './edit-add-ons.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditAddOnsPageRoutingModule
  ],
  // declarations: [EditAddOnsPage],
// entryComponents: [AddOnsPage]
})
export class EditAddOnsPageModule {}
