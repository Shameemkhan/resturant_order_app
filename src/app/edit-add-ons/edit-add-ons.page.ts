import { AttributeService } from './../attribute.service';
import { CommonService } from './../common.service';
import { Router } from '@angular/router';
import { CallApisService } from './../call-apis.service';
import { Items } from './../models/items';
import { CartService } from '../services/cart.service';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { ModalController, AlertController, LoadingController } from '@ionic/angular';
import { BehaviorSubject, Subscription } from 'rxjs';
import { SearchModalPagePage } from '../search-modal-page/search-modal-page.page';
import { Platform } from '@ionic/angular';
import { AddOnsPage } from '../add-ons/add-ons.page';


@Component({
  selector: 'app-edit-add-ons',
  templateUrl: './edit-add-ons.page.html',
  styleUrls: ['./edit-add-ons.page.scss'],
})
export class EditAddOnsPage implements OnInit, OnDestroy {
  @Input() prodId: any;
  
  cart: Items[] = [];
  itemList: Items[] = [];
  subTotal: any = "0";
  savePrice: any = '0';
  totalAmt: any = "0";
  addOnsCart: any[] = [];
  itemSubs: Subscription;
  productId: any;
  headerName: string;
  itemData: any = {}
  filteredProduct: any[] = [];
  addProductQuantity: any = {}
  quant: boolean = false;
  extras: boolean = false;
  cartItemCount: BehaviorSubject<number>;
  spinner: boolean = false;
  states: any[] = [];

  constructor(private cartService: CartService, 
    private modalCtrl: ModalController, 
    private alertCtrl: AlertController,
    private apiService: CallApisService,
    private router: Router,
    private commonService: CommonService,
    private attributeService: AttributeService,
    public modalController:ModalController,
    private platform: Platform,
    public loadingController: LoadingController) {
      
     
    }
  ngOnDestroy(): void {
  }

  ngOnInit() {
    this.filteredProduct = this.commonService.get_itemList();
    this.filteredProduct.forEach((data: any) => {
      this.itemData = data;
    })
  }

  
  async ionViewWillEnter(){
    this.spinner = true;
    this.itemSubs =  this.apiService.getAddOns(this.prodId)
    .subscribe((res: any) => {
      if(res){
        this.spinner = false;
      }
      this.headerName = res.name;
      for(var i = 0; i<this.cart.length; i++){
        if(res.id === this.cart[i].id){
          this.productId = res.id;
          this.addOnsCart.push(this.cart[i]);
        }
      }
      
      console.log(this.addOnsCart)
      this.filteredProduct.forEach((data: any) => {
        this.addOnsCart.forEach((res: any) => {
          if(res.id === data.id){
            this.addProductQuantity = data;
          }
        });
      });
    });

    let cart = this.cartService.getCart();
    this.cart = cart.reverse();
    this.cartItemCount = this.cartService.getCartItemCount();
    this.getTotal();
    
    this.apiService.getStates().subscribe((states: any) => {
      console.log(states);
      this.states = states;
    })
  }


  getTotalQuantityOfPerticalItem(){
    
    let total = 0;
    for(var i = 0; i<this.cart.length; i++){
    this.filteredProduct.forEach((res: any) => {
      // alert(this.cart[i].id === res.id)
        if(this.cart[i].id === res.id){
        

          total += Number(this.cart[i].quantity); 
          res.quantity = total;
         
          // this.cart[i].quantity = 0;
        }
      });
    }
  }

  dismissModal(){
    this.modalCtrl.dismiss();
  }

 

  async addNewItem(product){
    this.dismissModal();
    const modal = await this.modalController.create({
      component: AddOnsPage,
      componentProps: {
        "prodId": this.productId
      },
      cssClass: 'my-modal-css'
    });
    modal.onDidDismiss().then(() => {
   
      this.cartService.getTotalQuantityOfPerticalItem(product)
    })
    return await modal.present();
  }

  decreaseCartItem(product, item) {
    localStorage.setItem('qty', '1')
    let data;
    product.optionGroup.slice(-1).forEach((cartGroup: any) => {
     data = cartGroup;
    });
    if(product.optionGroup.length > 0 && data.length > 0){
      this.cartService.deleteProductToAddOns(product)
      this.removeItemsFromAddOnsCart(item);
    }
    else if(product.optionGroup.length > 0 && data.length === 0){
      this.cartService.decreaseProduct(product);
      this.removeItemsFromAddOnsCart(item)
    }
  }
 
  increaseCartItem(product) {
    localStorage.setItem('qty', '1')
    let data;
    let qty: any = 1;
    localStorage.setItem('qty', qty)
    product.optionGroup.slice(-1).forEach((cartGroup: any) => {
     data = cartGroup;
    });
    if(product.optionGroup.length > 0 && data.length > 0){
      this.cartService.addProductToAddOns(product);
    }
    else if(product.optionGroup.length > 0 && data.length === 0){
      this.cartService.addProduct(product);
    }
    
  }

  removeItemsFromAddOnsCart(item){
    for (let [index, cartItem] of this.addOnsCart.entries()) {
      if(cartItem.quantity === 0){
        this.addOnsCart.splice(index, 1);
        this.cartService.getTotalQuantityOfPerticalItem(item);
      }
    }
    if(this.addOnsCart.length === 0){
      this.modalCtrl.dismiss();
    }
  }

 
  removeCartItem(product) {
    this.cartService.removeProduct(product);
  }
 
  priceSave(){
    let savePrice: any = 0;
    for(var i = 0; i<this.cart.length; i++){
      if(this.cart[i].discountPrice){
        savePrice += this.cart[i].discountPrice * this.cart[i].quantity;
        this.savePrice = savePrice; 
      }
    }
    return savePrice;
  }

  gotoTab2(){
    this.router.navigate(['/tabs/tab2'])
  }

  subTotalPrice() {
  
    // return this.cart.reduce((i, j) => i + j.price * j.discountPrice, 0);
    let total: any = 0;
    for(var i = 0; i<this.cart.length; i++){
      if(this.cart[i].price){
        total += (this.cart[i].price - this.cart[i].discountPrice) * this.cart[i].quantity;
    
        this.subTotal = total;
      }
    }
    
    return total;
  
  }

  getTotal() {
    // return this.cart.reduce((i, j) => i + j.price * j.discountPrice, 0);
    let total: any = 0;
    for(var i = 0; i<this.cart.length; i++){
      if(this.cart[i].price){
        total += this.cart[i].price * this.cart[i].quantity;
        this.totalAmt = total;
      }
    }
    return total;
  }



  close() {
    this.modalCtrl.dismiss();
  }
 
  checkout()  {
    // alert("sub"+this.subTotal)
    // alert("tot"+this.totalAmt)
    
    
 
     let cartItem = this.cartService.getCartItemCount();
     let cartCount =  cartItem.getValue();
     if(cartCount <= 0){
       this.router.navigate(['/tabs/tab2']);
     }
     else{
      let token = localStorage.getItem('token');
      this.apiService.subTotalAmt.next(this.subTotal);
      this.apiService.totalAmt.next(this.totalAmt);
      this.apiService.savePrice.next(this.savePrice);
      if(token){
        this.router.navigate(['/delivery-address']);
      }
      else{
        this.router.navigate(['/tabs/tab3']);
      }
     }
    // Perfom PayPal or Stripe checkout process
    // let alert = await this.alertCtrl.create({
    //   header: 'Thanks for your Order!',
    //   message: 'We will deliver your food as soon as possible',
    //   buttons: ['OK']
    // });
    // alert.present().then(() => {
    //   this.modalCtrl.dismiss();
    // });
  }

  async openmodal() {
    const modal = await this.modalController.create({
      component: SearchModalPagePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  repeatLastItem(product){
    this.cartService.addProductToAddOns(product);
    this.modalCtrl.dismiss();
    this.router.navigate(['/check-out']);
  }
 
  makeUnique(){
    // this.spinner = true;
    let uniqueItemData = [...new Set(this.filteredProduct)]
    this.filteredProduct = []
    uniqueItemData.forEach((data: any) => {
    
      // console.log(this.filteredProduct);
      this.filteredProduct.push(data)
      // console.log(data)
      // console.log(this.filteredProduct);
    })
    // this.spinner = false;
  }
 


}
