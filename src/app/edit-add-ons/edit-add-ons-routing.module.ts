import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditAddOnsPage } from './edit-add-ons.page';

const routes: Routes = [
  {
    path: '',
    component: EditAddOnsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditAddOnsPageRoutingModule {}
