import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditAddOnsPage } from './edit-add-ons.page';

describe('EditAddOnsPage', () => {
  let component: EditAddOnsPage;
  let fixture: ComponentFixture<EditAddOnsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAddOnsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditAddOnsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
