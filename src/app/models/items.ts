export interface Items{
    amount: string;
    id: string;
    name: String;
    base64Image: string;
    categories: any[];
    available: string;
    price: any;
    uploadStatus: string;
    foodType: string;
    qty: any;
    lastQty : number;
    rate: any;
    totalRate: any;
    modifyRate : any;
    discountPrice: any;
    quantity: any;
}