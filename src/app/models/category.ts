export interface category{
    active: Boolean;
    description: string;
    id: string;
    name: string;
    storeId: string;
}