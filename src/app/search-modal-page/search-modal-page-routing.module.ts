import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchModalPagePage } from './search-modal-page.page';

const routes: Routes = [
  {
    path: '',
    component: SearchModalPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchModalPagePageRoutingModule {}
