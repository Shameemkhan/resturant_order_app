// import { Searchbar } from '@ionic/angular';
import { CommonService } from './../common.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import {  ViewChildren } from '@angular/core';


@Component({
  selector: 'app-search-modal-page',
  templateUrl: './search-modal-page.page.html',
  styleUrls: ['./search-modal-page.page.scss'],
})
export class SearchModalPagePage implements OnInit {
  // @ViewChildren(Fab) fabs: any;
  @ViewChildren('searchInput') searchBar: any;
  constructor(private modalCtrl: ModalController, private router: Router, private route: ActivatedRoute, private commonService: CommonService) { }
  searchText = '';
  shoeingItemList: Boolean = false;
  showingMessage: boolean = false;
  spinner: boolean = false;
  filteredProduct: any[] = []
  ngOnInit() {

    console.log(this.searchText);

  //  alert("works")
  
   
  }

  // readValue(){
  //  if(this.searchText === ''){
  //    this.spinner = true;
  //  }
  //  else{
  //    this.spinner = false;
  //  }
  // }

  ionViewDidEnter() {
    let elem = <HTMLInputElement>document.querySelector('.the-page-name-searchbar');
    if (elem) {
        elem.focus();
    }
}

  dismissModal(){
    this.modalCtrl.dismiss();
  }

  getData(){
    // console.log(this.searchText);
    this.modalCtrl.dismiss();
  }

  value(name){
       this.router.navigate(['/tabs/tab2'])
     setTimeout(() => {
      this.commonService.itemName.next(name)
    }, 100)
   
   
    // console.log(name)
   
  }
}
