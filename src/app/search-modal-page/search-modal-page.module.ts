import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchModalPagePageRoutingModule } from './search-modal-page-routing.module';

import { SearchModalPagePage } from './search-modal-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchModalPagePageRoutingModule
  ],
  // declarations: [SearchModalPagePage]
})
export class SearchModalPagePageModule {}
