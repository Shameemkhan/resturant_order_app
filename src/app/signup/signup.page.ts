import { AuthService } from './../auth.service';
import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { CallApisService } from '../call-apis.service';
import { Subscription } from 'rxjs';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit{
  moNumber: string = '';
  pass: string = '';

  submitted: boolean = false;

  private userUnsubscription: Subscription;
  constructor(public apiService: CallApisService, 
    private router: Router,
    private authService: AuthService,
    public formBuilder: FormBuilder) { }

    form =  this.formBuilder.group({
      password: new FormControl('', Validators.required),
      confirmPassword: new FormControl('', Validators.compose([
        Validators.required
      ]))
    }, {
      validators: this.password.bind(this)
    });

  ngOnInit() {
      this.apiService.typeValue.subscribe((res: any) => {
        this.moNumber = res;
      });
      // console.log(this.moNumber);
      // console.log(this.pass);


  }

  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: confirmPassword } = formGroup.get('confirmPassword');
    return password === confirmPassword ? null : { passwordNotMatch: true };
  }

  // register(){
  //   this.apiService.signUp(this.moNumber, this.password)
  //   .subscribe((res: any) => {
  //     console.log(res);
  //     console.log(this.password)
  //     this.apiService.pass.next(this.password)
  //     this.apiService.userId.next(res.userId);
  //   }
  //   ,err => {
  //     console.log(err);
  //   });

    // this.authService.signUp(this.moNumber, this.password)
    // .then((res: any) => {
    //   console.log(res);
    // })
    // .catch((err: any) => {
    //   console.log(err);
    // })
  // } 
  
  // generateOtp(){
  //   this.authService.getOtp(this.moNumber)
  //   .then((res: any) => {
  //     console.log(res);
  //     console.log(res.data);
  //   })
  //   .catch((err: any) => {
  //     console.log(err);
  //   });
  // }

  generateOtp(){
    this.apiService.getOtp(this.moNumber).subscribe((res: any) => {
      console.log(res);
      this.apiService.pass.next(this.pass);
      // console.log(this.pass)
    },
    (err) => {
      console.log(err);
    });
  }

  f(controls) {
    return this.form.get(controls);
  }

  gotoLoginPage(){
    this.router.navigate(['/tabs/tab3'])
  }
 
   onSubmit(){
      this.submitted = true;

      if(this.form.invalid){
         return;
      }

      let pass = this.form.get('password').value
      // console.log(pass)

      this.apiService.getOtp(this.moNumber).subscribe((res: any) => {
        console.log(res);
        this.apiService.pass.next(pass);
        // console.log(pass)
        this.router.navigate(['/otp']);
      },
      (err) => {
        console.log(err);
      });
    }

   
  

    

}