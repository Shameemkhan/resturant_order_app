import { AbstractControl, FormGroup } from "@angular/forms";
export function ConfirmPasswordValidator(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      let control = formGroup.controls[controlName];
      let matchingControl = formGroup.controls[matchingControlName]
      if (
        matchingControl.errors &&
        !matchingControl.errors.confirmPasswordValidator
      ) {
        return;
      }
      return control.value === matchingControl.value ? null : matchingControl.setErrors({ConfirmPasswordValidator: true})
    //   if (control.value === matchingControl.value) {
    //     matchingControl.setErrors(null);
      
    //   } else {
    //     matchingControl.setErrors({ confirmPasswordValidator: true });
    //   }
    };
  }

