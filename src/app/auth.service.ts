import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { Injectable } from '@angular/core';
import { AuthData } from './models/auth-data-model';
import {  HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private authStatusListner = new Subject<boolean>();

  constructor(private http: HTTP, 
    private httpClient: HttpClient, 
    private router: Router) { }

  token = localStorage.getItem('token');

  header = {
    Authorization: 'Bearer ' + this.token,
    'Content-Type': 'application/json; charset=utf-8' 
  }

  otpUrl = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/otp/generateOtp";
  loginUrl = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/auth/authenticate";
  registerUrl = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/auth/register";
  
  userVarification = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/user/verify/user";
  otpVarification = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/otp/mobile/authenticate";


  signUp(username: string, password: string){
    const user: AuthData = {
      "username": username,
      "password": password
    }
    this.http.setDataSerializer('json');
     return this.http.post(this.registerUrl, user, {});
  }

  login(username: string, password: string){
    const authData: AuthData = {
      username: username,
      password: password
    }
    return this.httpClient.post(this.loginUrl, authData)
    .pipe(map((response: any) => {
      let result = response;
      if(result && result.token){
        localStorage.setItem("token", result.token);
        this.authStatusListner.next(true);
        return true;
      }
      return false;
    }));
  }

  getOtp(typeValue: string){
    console.log(typeValue);
    this.http.setDataSerializer('json');
     return this.http.post(this.otpUrl, {}, {"typeValue": typeValue});
  }

  varifyUser(username: string){
    this.http.setDataSerializer('json');
    return this.http.post(this.userVarification, {"username": username}, {});
   }

  varifyOtp(otp: string){
    return this.http.put(this.otpVarification, {},  {"typeValue": otp});
  }

  logout(){
    localStorage.removeItem('token');
    this.router.navigate(['/tabs/tab1']);
    this.authStatusListner.next(false);
  }

}
