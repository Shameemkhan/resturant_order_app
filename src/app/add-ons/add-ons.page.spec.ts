import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddOnsPage } from './add-ons.page';

describe('AddOnsPage', () => {
  let component: AddOnsPage;
  let fixture: ComponentFixture<AddOnsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOnsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddOnsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
