import { MatRadioModule } from '@angular/material/radio';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddOnsPageRoutingModule } from './add-ons-routing.module';

// import { AddOnsPage } from './add-ons.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatRadioModule,
    AddOnsPageRoutingModule,

  ],
  // declarations: [AddOnsPage]
})
export class AddOnsPageModule {}
