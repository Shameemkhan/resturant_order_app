import { products } from './../data1.service';
import { CallApisService } from './../call-apis.service';
import { CommonService } from './../common.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';
import { AttributeService } from '../attribute.service';
import { Observable, Observer } from 'rxjs';
import { LoadingController, ModalController } from '@ionic/angular';
import { CalculationService } from '../calculation.service';

@Component({
  selector: 'app-add-ons',
  templateUrl: './add-ons.page.html',
  styleUrls: ['./add-ons.page.scss'],
})
export class AddOnsPage implements OnInit {

  @Input() prodId: any;
  addOns: any[] = [];
  variations: any[] = [];
  items: any;
  addOnsItems: any =  {}
  optionGroup: any[] = [];
  public qty: any = 1;
  p1= 60;
  p2= 150;
  // spinner: boolean = false;
  price: any = "";
  total: any = "";
  checked: boolean = false;
  checked2: boolean = false;
  maxQty: any;
  minQty: any;
  variationsItems: any[] = [];
  selectedAddOnsItems: any[] = []
  imageUrl: any;
  isChecked :boolean;
  // myModel = 1;
  optionsList: any[] = [];
  // radioValue: any;
  value_selected: any = '0';
  toShowAddOns: boolean = false;
  spinner: boolean = false;
  addVariOptionGroup: any[] = [];
  customOptionGroup: any[] = [];
  makeOptionGroup: any[] =[];
  optionsCondition: any = {}
  variationDefaultPrice: any = 0;

  constructor(private route: ActivatedRoute, 
    private router: Router,
    private commonService: CommonService,
    private apiService: CallApisService,
    private cartService: CartService,
    private attService: AttributeService,
    public loadingController: LoadingController,
    private modalCtrl: ModalController,
    private calService: CalculationService) { }

  ngOnInit() {
  }

 
   async ionViewWillEnter(){
 
    this.spinner = true;
    this.imageUrl = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/files/';
    // this.attService.dicountPrice.subscribe((res: any) => {
    //   console.log(res)
    //   if(res > 0){
    //     alert('if works')
    //     this.total - res;
    //   }
    // })
    
    // let id  =  this.route.snapshot.paramMap.get('id');
    this.items = this.commonService.get_itemList();
    console.log(this.prodId);
    this.apiService.getAddOns(this.prodId)
    .subscribe((res: any) => {
        if(res){
          this.spinner = false;
        }
      // this.spinner = false;
      if(res.itemImages.length != 0){
        for(let i=0;i<res.itemImages.length;i++){
         this.getBase64ImageFromURL(this.imageUrl + res.itemImages[i].img_name).subscribe(base64data => {
           res.base64Image = 'data:image/jpg;base64,' + base64data;
            // images.src = 'data:image/jpg;base64,' + base64data;
            // imagesForItem.push(images)
            // data.itemsImages = imagesForItem
            //   this.filteredProduct.push(data);
            //   this.makeUnique()
            });
          }
        }
          // if(res.img_name != null){
          //   let id = res.id.toString();
          //     this.getBase64ImageFromURL(this.imageUrl + res.img_name ).subscribe(base64data => {
          //     res.base64Image = 'data:image/jpg;base64,' + base64data;
          //     });
          //   }
      this.total = res.price;
      // let disocunt = res.discountPrice
      // this.price = res.price - disocunt;
      // this.total = res.price -  disocunt ;
      res.optionGroup.forEach((data: any) => {
        data.options.forEach((res: any) => {
          
          let addOns: boolean = false;
          let addOns2: boolean = false;
              addOns = res.ref_id.includes('OGI');
              addOns2 = data.refId.includes('OG')
              let varia: boolean = false;
              let varia2: boolean = false;
              varia = res.ref_id.includes('VO')
              varia2 = data.refId.includes('VOG')
          if(addOns === true && addOns2 === true){
            this.addOns.push(data);
            this.makeAddOnsUnique();
          }
          else if(varia === true && varia2 === true){
            this.variations.push(data);
            this.makeVariationsUnique();
          }
        });
      });
      console.log(this.variations)

      console.log(this.addOns)

      this.addOns.forEach((data: any) => {
        if(!data.refId.includes('VOG')){
          data.options.forEach((optionsData: any) => {
            if(optionsData.ref_id.includes('OGI'))
            optionsData.qty = 0;
            data.options.push(optionsData);
            let uniqueOptionsData = [...new Set(data.options)]
            data.options = []
            uniqueOptionsData.forEach((data1: any) => {
              data.options.push(data1);
            })
          });
         }
      })

      console.log(this.addOns)

      this.variations.forEach((data: any) => {
        for(let i=0;i<data.options.length;i++){
         this.total =  data.options[0].price;
        // this.variationDefaultPrice = data.options[0].price;
        }
      })
      // for(let i=0;i<this.variations.length;i++){
      //   console.log(this.variations[0].price);
      //   this.variations[0].options.for
      //   this.total = this.variations[0].price
      // }
      if(this.variationsItems.length === 0){
        this.variations.forEach((data: any) => {
           for(let i=0; i<=data.options.length; i++){
              this.variationsItems.push(data.options[0]);
              this.makeVariationsDataUnique()
            }
        })
       
      }
      this.addOnsItems = res;
    });


    this.apiService.getAddOns(this.prodId)
    .subscribe((res: any) => {
      console.log(res)
      res.optionGroup.forEach((data: any) => {
        this.customOptionGroup.push(data);
        this.customOptionGroup.forEach((data: any) => {
          data.options = [];
          if(data.refId.includes('VOG')){
            this.variationsItems.forEach((res: any) => {
              data.options.push(res);
            });
          }
        });
      });
    });

    // this.items.forEach((res: any) => {
    //   // console.log(res)
    //   res.optionGroup.forEach((data: any) => {
    //     data.options.forEach((options: any) => {
    //       if(options.id === id){
    //         this.addOnsItems.push(options);
           
    //       }
    //     })
    //   })
    // })
    // alert(this.variationDefaultPrice);
    
    // this.total = this.variationDefaultPrice;
    // alert(this.total)

  }

  mcAnswer(event){
}

onTermsRadio($event){
}

makeAddOnsUnique(){
  let cities = [...new Set(this.addOns)]
  this.addOns = []
  cities.forEach((data: any) => {
    this.addOns.push(data);
  })
  
}

makeVariationsUnique(){
  let cities = [...new Set(this.variations)]
  this.variations = []
  cities.forEach((data: any) => {
    this.variations.push(data);
  })
  
}

makeVariationsDataUnique(){
  let cities = [...new Set(this.variationsItems)]
  this.variationsItems = []
  cities.forEach((data: any) => {
    this.variationsItems.push(data);
  })
  
}


getIonRadioValue(data, opGrpName){
  console.log(opGrpName)
  console.log(data)
  // this.variationDefaultPrice = data.price;
  this.customOptionGroup.forEach((res: any) => {
    res.options = [];
    if(res.id === opGrpName.id){
      res.options = [];
      res.options.push(data)
    }
  });
 
  console.log(this.customOptionGroup)

  this.isChecked = false;
  setTimeout(() => {
    this.isChecked = undefined;
    this.total = data.price;
  }, 50)

  this.variationsItems = [];
  this.variationsItems.push(data);
  this.total = data.price;

  
  
// console.log(data.price)
// console.log(this.total)
}

radioValue(){
// alert(this.value_selected)
}

getBase64ImageFromURL(url: string) {
  return Observable.create((observer: Observer<string>) => {
   
    let img = new Image();
    img.crossOrigin = 'Anonymous';
    img.src = url;  img.src = url;
    if (!img.complete) {
      img.onload = () => {
        observer.next(this.getBase64Image(img));
        observer.complete();
      };
      img.onerror = (err) => {
        observer.error(err);
      };
    } else {
      observer.next(this.getBase64Image(img));
      observer.complete();
    }
  });
}

getBase64Image(img: HTMLImageElement) {
  var canvas = document.createElement("canvas");
  canvas.width = 180;
  canvas.height = 180;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0, 180, 180);
  var dataURL = canvas.toDataURL("image/png");
  // console.log(dataURL);
  return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}



changeModel(addOns){
  // console.log(addOns)

  this.total = addOns.price;
  this.price = addOns.price;
//  console.log( this.addOnsItems)
  this.addOnsItems.optionGroup.forEach((data: any) => {
    if(addOns.id === data.id){
      this.optionsList = [];
      data.options.forEach((res: any) => {
        // console.log(res)
      let name =  res.ref_id.substr(0, 1)
        if(data.refId === name){
          this.toShowAddOns = true;
          this.optionsList.push(res);

        }
      
      })
    }

  });
  
  
}


  checkButton(optionPrice){
    if(this.checked === true){
      // console.log('true')
    }
    else{
      // console.log('flase')
    }

    // if(value === true){
    //  this.total = this.total + this.p1 ;
    // }
    // else{
    //   // this.total = this.total -  this.p1 ;
    //   if(this.checked2 === true){
    //     this.qty = 1;
    //     this.total = this.price + this.p1;
    //   }
    //   else{
    //     this.qty = 1;
    //     this.total = this.price;
    //   }
    // }
  }

  onTermsChecked($event, opGrpName){
    console.log(opGrpName)
    console.log($event)
    console.log(this.variationDefaultPrice);
    // alert(this.variationDefaultPrice);
    this.optionsCondition = $event.detail
    let variData: any[] = [];
  let price: any = parseInt($event.detail.value.price)
    if ($event.detail.checked === true)
    { 
      if($event.detail.value.qty === 0){
        $event.detail.value.qty++;
      }
      
      this.variationsItems.push($event.detail.value)
      // variData.push($event.detail.value)
      this.customOptionGroup.forEach((res: any) => {
        if(opGrpName.id === res.id && $event.detail.checked === true){
          res.options.push($event.detail.value)
          // opGrpName.options.forEach((options: any) => {
          //   if(options.id === $event.detail.value.id){
          //     alert('if works');
          //     if(opGrpName.id === res.id){
          //       res.options
          //     }
          //     console.log(opGrpName.name)
          //   }
          // });
          // res.options.push($event.detail.value)
          
        }
      })
      this.total =   this.total + price;
      console.log(this.customOptionGroup)
    }
    else if($event.detail.checked === false){
      this.total = this.total - price
      $event.detail.value.qty = 0;
      // let totalOptionsPrice = $event.detail.value.price * $event.detail.value.qty;
      // this.total = this.total - totalOptionsPrice;
      // $event.detail.value.qty = 0;
      // alert($event.detail.value.qty)
      // let totalOptionsPrice = price * $event.detail.value.qty;
      // this.total = this.total + totalOptionsPrice;
        for (let [index, cartItem] of this.variationsItems.entries()) {
          if($event.detail.value.id === cartItem.id){
            this.variationsItems.splice(index, 1);
            this.customOptionGroup.forEach((res: any) => {
              if(opGrpName.id === res.id && $event.detail.checked === false){
                for (let [i, option] of res.options.entries()){
                  if($event.detail.value.id === option.id){
                    res.options.splice(i, 1);
                  }
                }
              }
            })
          }
        }
        console.log(this.customOptionGroup)
        // this.customOptionGroup.forEach((data1: any) => {
        //   data1.options.forEach((optionsData: any) => {
        //     if(optionsData.id === $event.detail.value.id){
        //       // data1.options = []
        //     }
        //   })
        // })
    }
  }

//   onTermsChecked($event)
// {
//   // this.isChecked = undefined
//   if($event.detail.checked === true){
    
//     this.variationsItems.push($event.detail.value)
    
//     // console.log(this.variationsItems);
//   }
//   if($event.detail.checked === false){
//     this.variationsItems = [];
//   }

//   // console.log($event.detail.value.price);
//   let price: any = parseInt($event.detail.value.price)
//     if ($event.detail.checked === true)
//     { 
//       // console.log( $event.detail.value)
//       // this.isChecked = true;
//       // $event.detail.checked = true;
//     this.total =   this.total + price;
    
//     }
//     else if($event.detail.checked === false){
//       this.total = this.total - price
//     }
//   }

increaseAddOns(optionsData){
  if(optionsData.qty === 1){
    optionsData.qty++;
    this.total = this.total - optionsData.price;
    let totalOptionsPrice = optionsData.price * optionsData.qty;
    this.total = this.total + totalOptionsPrice;
  }else if(optionsData.qty > 1){
    optionsData.qty++;
    this.total = this.total + optionsData.price;
  }
  else{
    alert('Please checked the addOns first');
  }
}

decreaseAddOns(optionsData){
  if(optionsData.qty === 1){
    // optionsData.qty--;
    // this.total = this.total - optionsData.price;
    // let totalOptionsPrice = optionsData.price * optionsData.qty;
    // this.total = this.total - totalOptionsPrice;
  }else if(optionsData.qty > 1){
    optionsData.qty--;
    this.total = this.total - optionsData.price;
  }
  else{
    alert('Please checked the item first');
  }
  
}
  
  increaseItem(){
   

      this.qty += 1

  }

  decreaseItem(){

    if(this.qty === 1){
      this.modalCtrl.dismiss();
    }
    else{
      this.qty -= 1;
      
      let stati = this.total;
      let total = stati - this.price; 
      this.total = total; 
    }
  }

  dismissModal(){
    this.modalCtrl.dismiss();
    // this.router.navigate(['/tabs/tab2'])
  }

  addItem(product){
    this.modalCtrl.dismiss();
    let filterList =  this.customOptionGroup.filter((data: any) => {
      return data.options.length != 0;
    })
    // for(let [i, optionGroup] of this.customOptionGroup.entries()){
    //   if(optionGroup.options.length === 0){
    //     this.customOptionGroup.splice(i, 1);
    //   }
    // }


    console.log(filterList)
    product.optionGroup = [];

    filterList.forEach((data: any) => {
      product.optionGroup.push(data)
    })
    // this.customOptionGroup.forEach((res: any) => {
    //   for (let [i, option] of res.options.entries()){
    //     if(){
    //       res.options.splice(i, 1);
    //     }
    //   }
    // })
    this.cartService.updateItemCount(this.qty - 1)
   
    localStorage.setItem('qty', this.qty);
    if(this.qty > 1){  
      product.quantity = this.qty - 1;
      
    }
    product.optionGroup.push(this.variationsItems);
    // this.calGst(product);
    if(this.variationsItems.length > 0){
    
      product.price = this.total;
      if(this.qty > 1){
       
        product.quantity = this.qty - 1;
        product.optionGroup.slice(-1).forEach((cartGroup: any) => {
          cartGroup.forEach((data: any) => {
            if(data.qty > 0){
              data.qty = this.qty;
            }
          })
        });
      }
     
      this.cartService.addProductToAddOns(product);
    }
    
    else{
      this.cartService.addProduct(product);
    }
  }

  // calGst(product, taxPrice){
  //   let price;
  //   let cgstTaxPer;
  //   let sgstTaxPer;
  //   let vatTaxPer;
    
  //   product.taxes.forEach((tax: any) => {
  //     if(tax.name.includes('CGST')){
  //       cgstTaxPer = tax.structure.value;
  //       let taxAmount = this.calService.calculateGst(price, cgstTaxPer)
  //       console.log(taxAmount);
  //       tax.amount = taxAmount;
  //     }
  //     else if(tax.name.includes('SGST')){
  //       sgstTaxPer = tax.structure.value;
  //     }
  //     else{
  //       vatTaxPer = tax.structure.value;
  //     }
  //   })

  //   console.log(product)
  // }

  repeatLastItem(product){
    this.cartService.addProductToAddOns(product);
    this.router.navigate(['/check-out']);
  }
}



