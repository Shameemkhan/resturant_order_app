import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddOnsPage } from './add-ons.page';

const routes: Routes = [
  {
    path: '',
    component: AddOnsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddOnsPageRoutingModule {}
