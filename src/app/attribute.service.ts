import { CallApisService } from './call-apis.service';
import { Injectable } from '@angular/core';
// import { Product } from './services/cart.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AttributeService {
  itemList: any[] = [];

  dicountPrice = new BehaviorSubject<string>("");
  totalPrice = new BehaviorSubject<string>("");
  constructor(private apiService: CallApisService) { }

  //  data: any[] = [
  //   { id: 0, image:'assets/p7.jpg', name:'BURGER', price: 130, amount: 0 },
  //   { id: 1,image:'assets/p6.jpg', name:'CHILLY PANEER', price:120, amount: 0 },
  //   { id: 2,image:'assets/p5.jpg', name:'VEG CHEESE PIZZE', price:150, amount: 0 },
  //   { id: 3,image:'assets/p4.jpg', name:'SAMOSA', price:100, amount: 0 },
  //   { id: 4,image:'assets/p3.jpg', name:'PEANUT BUTTER', price: 125, amount: 0 },
  //   { id: 5,image:'assets/p2.jpg', name:'GRILL CHICKEN', price:160, amount: 0 }
  // ];


  // getCart(){
  //   return this.data;
  // }

  getItems(){
    this.apiService.getAllItems().subscribe((res: any) => {
      let data: any = res;
      this.itemList = data; 
    }
    , err => {
      console.log(err);
    })
  }

  get_exist_getAllItems() {
    return this.itemList;
  }

  getAllAttribute() {
     return this.apiService.getCatagories();
  }

  
}
