import { Component, OnInit } from '@angular/core';
import { SearchproductService } from '../services/searchproduct.service';

@Component({
  selector: 'app-searchproduct',
  templateUrl: './searchproduct.page.html',
  styleUrls: ['./searchproduct.page.scss'],
})
export class SearchproductPage implements OnInit {

  mydata:any[];

  constructor(
    private data:SearchproductService) { }

  ngOnInit() {
 this.mydata=this.data.getproduct()

  }



  
}
