import { addressUser } from './../address-details/addressUser';
import { AttributeService } from './../attribute.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';
import { CallApisService } from '../call-apis.service';
import { Items } from '../models/items';
import { IonicHttpService } from '../ionic-http.service';
import { DatePipe } from '@angular/common'
import { GoogleMapsService } from '../google-maps.service';
import { AlertController } from '@ionic/angular';
import { CalculationService } from '../calculation.service';

//status
//ackno = placed, foodready = processing, dispach = completed or delivery


@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.page.html',
  styleUrls: ['./dialog.page.scss'],
})
export class DialogPage implements OnInit {
  
  date: any;
  paymentTypeName: any;
  latest_date: any;
  moNumber: any;
  message = "Item Added successfully!!"
  cart: Items[] = [];
  incrementId = 502023;
  totalAmount: any = '';
  order: any = {
    userId: "",
    currentState: 'Placed',
    channel: 'zomato',
    previousState: "Placed",
    provide: "Arbaaz",
    orderStates: [],
    message: "The order has been accepted by the restaurant",
    customerDetails: {
      // userId: '208',
      // addressId: '81'
    },
    extChannel: {
      
      "deliveryType": "cash",
      "name": null,
      "kind": null,
      "channalOrderId": null,
      "externalDiscountInfo": []
  },
  details: {
   
    "coupon": "drs",
    "created": null,
    deliveryDateTime: "",
    "discount": 10,
    "instructions": "",
    "totalChargesAmount": 100,
    "totalTaxesAmount": 0, // amount with taxprice and itemPrice
    "subtotal": null,
    "total": 500,
    "type": null,
    "state": "mp",
    "totalCharges": 0, //delivery charges.
    "totalExternalDiscount": 50,
    "totalTaxes": 40, 
    "itemTotalCharges": 1000,
    "itemTotalTaxes": 0, // totalTaxAmount
    "itemTaxes": 0,
    "merchant_ref_id": null,
    "taxes": [],
    "charges": []
},
    items: [
      
    ],
    storeId: '2',
    payment: [  {
      "amount": 0,
      "option": "cash"
  }],
    riderDetails: {
      deliveryPerson: {
        name: '',
        phone: '',
        address: '',
        vehicleNo: '',
        available: true,
      },
      status: '',
      refId: null
    },
    refId: null
}

orderData: any = {
  base64Image: '',
  img_name: '',
  img_url: '',
  quantity: '',
  price: '',
  total: '',
  discount: '',
  foodType: '',
  name: '',
  status: 'Acknowledged',
  message: "The order has been Ready by the restaurant",
 
}

optionsToAdd: any = {
  id: null,
  price: null,
  title: '',
  quantity: null
}

addressData: any = {
  address: {
    city: '',
    pin: '',
    landmark: '',
    latitude: 0.0,
    longitude: 10.0,
    line_1: "",
    line_2: ""
  },
  name: '',
  phone: ''
}

  constructor(private router: Router, 
    private apiService: CallApisService, 
    private cartService: CartService, 
    private ionicHttp: IonicHttpService,
    private datePipe: DatePipe,
    private attributeService: AttributeService,
    private maps: GoogleMapsService,
    public alertCtrl: AlertController,
    private calService: CalculationService) {
      this.paymentTypeName = this.calService.getTypeName();

      this.order.extChannel.deliveryType = this.paymentTypeName;
      this.order.payment.option = this.paymentTypeName;
      this.order.payment.forEach((paymentData: any) => {
        paymentData.option = this.paymentTypeName;
      })

      this.order.userId = localStorage.getItem('userId')
      this.moNumber = localStorage.getItem('moNumber')
      this.apiService.getUserData(this.moNumber)
      .subscribe((res: any) => {
        res.customer.forEach((data: any) => {
          data.address.id = null
          this.order.customerDetails = data;
        })
        
      })
     }

  ngOnInit() {
    let storeId = localStorage.getItem('storeId')
      this.order.storeId = storeId;
  this.apiService.subTotalAmt.subscribe((res: any) => {
    this.order.details.subtotal = res;
  })



  this.apiService.savePrice.subscribe((res: any) => {
    this.order.details.discount = res;
  })

    this.apiService.date.subscribe((date: any) => {
      this.date = date;
      this.latest_date =  this.datePipe.transform(this.date, 'yyyy-M-dd hh:mm');
      this.order.details.deliveryDateTime = this.latest_date;
    })
    
   


    this.apiService.typeValue.subscribe((res: any) => {
      this.moNumber = res;
    }); 

    // this.maps.lati.subscribe((lati: any) => {
    //   console.log(lati);
    // });

    // this.maps.lang.subscribe((lang: any) => {
    //   console.log(lang);
    // });

    this.order.userId = localStorage.getItem('userId')
    this.moNumber = localStorage.getItem('moNumber')
    console.log(this.moNumber)
    this.apiService.getUserData(this.moNumber)
    .subscribe((res: any) => {
      res.customer.forEach((data: any) => {
        data.address.id = null
        this.order.customerDetails = data;
      })
      
    })
    
    this.ionicHttp.getUserDetails(this.moNumber).then((res: any) => {
     let data = JSON.parse(res.data);
    data.customer.forEach((data1: any) => {
      data1.address.id = null
      this.order.customerDetails = data1;
    })
      
      // this.order.customerDetails = data;
    
     })
     .catch(err => {
       console.log(err)
     })

  //    this.apiService.getUserData(this.moNumber).subscribe((res: any) => {
  //      console.log(res)
  //    });
  //  }

    this.cart = this.cartService.getCart();
    let tax:any  = {
      // rate: '',
      // name: '',
      // value: ''
    }
    this.cart.forEach((res: any) => {
      let disPrce = res.discountPrice;
      this.orderData = {
        item: [],
        refId: null,
        total: 0,
        total_with_tax: 0,
        options_to_add: [
         
        ],
        taxes: [],
        charges: []
      };

     let  deliveryCharges = {
      "name": "delivery",
      "createDate": 1618903569000,
      "sortOrder": null,
      "active": null,
      "description": null,
      "ref_id": null,
      "deliveryCharge": null,
      "platforms": [
      "d"
      ],
      "fulfillment_modes": [
      "yes"
      ],
      "structure": {
      "applicable_on": "yes",
      "type": "cash",
      "value": 10.0
      }
    }

        let deliveryPrice = this.calService.getDeliveryCharges();
        // console.log(deliveryPrice)
        let price = parseInt(deliveryPrice);
        // console.log(price)
        deliveryCharges.deliveryCharge = price;
        this.order.details.totalCharges = price;
        this.orderData.name = res.name;
        this.orderData.foodType = res.foodType;
        this.orderData.price  = res.price;
        this.orderData.discount = res.discountPrice;
        this.orderData.total = res.price - disPrce;
        this.orderData.quantity = res.quantity;
        this.orderData.image_landscape_url = res.img_url;
        this.orderData.image_url = res.img_name;
        this.orderData.refId = res.refId;

        if(res.taxes.length != 0){
          res.taxes.forEach((res: any) => {
            tax = {}
            tax.rate = res.percentage;
            tax.name = res.name;
            tax.value = res.amount;
            
            this.order.details.itemTotalTaxes += tax.value;
            this.apiService.totalAmt.subscribe((res: any) => {
              this.totalAmount = res;
              this.orderData.total = this.totalAmount;
              this.orderData.total_with_tax = this.totalAmount + this.order.details.itemTotalTaxes;
              this.order.details.total = this.totalAmount + this.order.details.itemTotalTaxes;
            })
            this.order.details.totalTaxesAmount = this.order.details.itemTotalTaxes + this.totalAmount;
            // this.order.items.taxes.push(tax);
            this.orderData.taxes.push(tax);
          })
        }
        else{
          this.apiService.totalAmt.subscribe((res: any) => {
            this.totalAmount = res;
            this.order.details.total = this.totalAmount;
          });
        }
      

        res.optionGroup.slice(-1).forEach((data: any) => {
         
          data.forEach((res) => {
            this.optionsToAdd = {}
            this.optionsToAdd.id = res.id
            this.optionsToAdd.title = res.name
            this.optionsToAdd.price = res.price
            if(res.qty > 0){
              this.optionsToAdd.quantity = res.qty
            }
         
            this.orderData.options_to_add.push(this.optionsToAdd);
          })
        })
     

        res.optionGroup.splice(-1).pop();  
        res.charges.push(deliveryCharges);
        this.order.details.charges.push(deliveryCharges)
        this.orderData.charges.push(deliveryCharges);
        this.orderData.item.push(res);
        this.order.items.push(this.orderData);
         let total =  parseInt( this.order.details.total)
         let delieryPrice = parseInt(deliveryCharges.deliveryCharge);
        this.order.details.totalChargesAmount = total + delieryPrice;
        this.order.payment.amount = total + deliveryPrice;
        this.order.payment.forEach((paymentData: any) => {
          let totalAmount =  parseInt( this.order.details.total)
          let delieryAmount = parseInt(deliveryCharges.deliveryCharge);
          paymentData.amount = totalAmount + delieryAmount;
        })
       
       
        
    })
    // this.order.items.push(this.cart);
  
  
  }

  confirmOrder(){
  
    setTimeout(async () => {
      const prompt = await this.alertCtrl.create({  
        header: 'Thank You',  
        message: 'Enter a comment for your order!',  
        inputs: [  
          {  
            name: 'name',  
            type: 'text',  
            placeholder: 'Enter Comment'  
          }  
        ],  
        buttons: [  
          {  
            text: 'Save',  
            handler: data => {  
               this.order.details.instructions = data.name;
              if(this.cart.length === 0){
                alert('You have not selected any item, Please select some items!');
                this.router.navigate(['/tabs/tab2']);
              }
              else{
               this.apiService.generateOrder(this.order)
              .subscribe((res: any) => {
                if(res){
                  this.cartService.makeCartEmpty();
                  this.router.navigate(['/orderplaced'])
                }
              },
              (err: any) => {
                console.log(err);
              });
              }
            }  
          }  
        ]  
      });  
      await prompt.present();
    }, 1000)
  
  
  }

}

// 4356453454