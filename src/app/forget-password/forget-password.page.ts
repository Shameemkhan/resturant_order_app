import { Router } from '@angular/router';
import { CartService } from '../services/cart.service';
import { Component, OnInit } from '@angular/core';
import { CallApisService } from '../call-apis.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.page.html',
  styleUrls: ['./forget-password.page.scss'],
})
export class ForgetPasswordPage implements OnInit {
  cart: any[] = [];
  typeValue: string = "";
  submitted: boolean = false;
 

  constructor(private apiService: CallApisService, private cartService: CartService, private router: Router,  public alertController: AlertController) { }

  form = new FormGroup({
    typeValue: new FormControl('', [Validators.required, Validators.minLength(10), Validators.pattern("^[0-9]*$")])
  })

  
  ngOnInit() {
    this.cart = this.cartService.getCart();
    console.log(this.cart);
  }

  getOtp(){
    this.typeValue = this.form.get('typeValue').value
    console.log(this.typeValue);
    this.apiService.typeValue.next(this.typeValue);
    console.log(this.typeValue)
    this.apiService.getForgotPasswordOtp(this.typeValue)
    .subscribe((res: any) => {
      console.log(res);
      sessionStorage.setItem('userId', res.userId)
      this.router.navigate(['/otp-forgot-password']);
    },
    (err) => {
      console.log(err);
    });


    // this.apiService.getOtp(this.typeValue)
    // .subscribe((typeValue: any) => {
    //   console.log(typeValue);
    // },
    // (err: any) => {
    //   console.log(err);
    //    console.log(err.error.text);
    //    let data = err.error.text;
    //    let data2 = data.substring(57, 61);
    //    this.apiService.otp.next(data2);
    //    console.log(data2);
    // });
  }

  
  f(controls) {
    return this.form.get(controls);
  }

  gotoLogIn(){
    this.router.navigate(['/tabs/tab3']);
  }

 
   onSubmit(){
      this.submitted = true;
 
      if(this.form.invalid){
         return;
      }

      this.typeValue = this.form.get('typeValue').value
      console.log(this.typeValue);
      this.apiService.typeValue.next(this.typeValue);
      console.log(this.typeValue)
      this.apiService.getForgotPasswordOtp(this.typeValue)
      .subscribe((res: any) => {
        console.log(res);
        sessionStorage.setItem('userId', res.userId)
        this.router.navigate(['/otp-forgot-password']);
      },
      async (err) => {
        console.log(err);
        if(err.status === 400){
          const alert = await this.alertController.create({
            cssClass: 'my-custom',
            header: 'Alert',
            message: 'Mobile number is not registered with us, Please register the number!',
            buttons: [{
              text: 'OK',
              handler: () => {
                this.router.navigate(['/tabs/tab3']);
              }
            }]
          });
      
          await alert.present();
        }
      });
    }
}
