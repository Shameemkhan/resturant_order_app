import { HTTP } from '@ionic-native/http/ngx';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  constructor(private http: HTTP) { }

  getItems = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/all";
  getCatagoryOne = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/category/1";

  getAllItems(){
    this.http.setDataSerializer('json');  
    return this.http.get(this.getItems, {}, {});
  }

  getCatagories(){
    this.http.setDataSerializer('json');
    return this.http.get(this.getCatagoryOne, {}, {});
  }
}
