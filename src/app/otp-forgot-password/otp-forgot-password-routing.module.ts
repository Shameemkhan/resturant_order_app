import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OtpForgotPasswordPage } from './otp-forgot-password.page';

const routes: Routes = [
  {
    path: '',
    component: OtpForgotPasswordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OtpForgotPasswordPageRoutingModule {}
