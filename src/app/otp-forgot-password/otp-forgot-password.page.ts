import { Router } from '@angular/router';
import { CallApisService } from './../call-apis.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-otp-forgot-password',
  templateUrl: './otp-forgot-password.page.html',
  styleUrls: ['./otp-forgot-password.page.scss'],
})
export class OtpForgotPasswordPage implements OnInit {
  otpData: any[] = [];
  otp1: string;
  otp2: string;
  otp3: string;
  otp4: string;
  otpl: any[] = [];
  otpValue: string = '';
  moNumber: string = '';
  otp: string = '';
  password: any;
  submitted: boolean = false;

  constructor(private apiService: CallApisService,
    private router: Router,  public alertController: AlertController,) { }

  ngOnInit() {
    this.apiService.typeValue.subscribe((res: any) => {
      this.moNumber = res;
    });
  }

  
  form = new FormGroup({
    otp: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")])
  })

  f(controls) {
    return this.form.get(controls);
  }

  gotoForgetPasswor(){
    this.router.navigate(['/forget-password'])
  }

  onSubmit(){

    this.submitted = true;

    if(this.form.invalid){
       return;
    }

    this.otp = this.form.get('otp').value
    console.log(this.otp);
    this.apiService.varifyOtp(this.moNumber, this.otp).subscribe((res: any) => {
      if(res === 2001){
        this.router.navigate(['/otp-change-password']);
      }
      else if(res === 2002){
        alert('otp is not verified')
      }
      else if(res === 2003){
        alert("your otp is expired please re-generate the otp")
      }
    });
  }

  forgotPassword(){
   
       }

       resendPassword(){
        this.apiService.getForgotPasswordOtp(this.moNumber)
        .subscribe(async (res: any) => {
          console.log(res);
          if(res){
            const alert = await this.alertController.create({
              cssClass: 'my-custom',
              header: 'Alert',
              message: 'Otp successfully sent please check your mobile!',
              buttons: [{
                text: 'OK',
              }]
            });
        
            await alert.present();
          }
          sessionStorage.setItem('userId', res.userId)
          this.router.navigate(['/otp-forgot-password']);
        },
        (err) => {
          console.log(err);
        });
       }
}
