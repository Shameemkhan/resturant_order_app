import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OtpForgotPasswordPageRoutingModule } from './otp-forgot-password-routing.module';

import { OtpForgotPasswordPage } from './otp-forgot-password.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    OtpForgotPasswordPageRoutingModule
  ],
  declarations: [OtpForgotPasswordPage]
})
export class OtpForgotPasswordPageModule {}
