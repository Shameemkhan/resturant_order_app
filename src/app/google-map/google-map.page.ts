import { CommonService } from './../common.service';
import { Router } from '@angular/router';
import { CallApisService } from './../call-apis.service';
import { Items } from './../models/items';
import { CartService } from '../services/cart.service';
import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { SearchModalPagePage } from '../search-modal-page/search-modal-page.page';

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.page.html',
  styleUrls: ['./google-map.page.scss'],
})
export class GoogleMapPage implements OnInit {
  cart: Items[] = [];
  itemList: Items[] = [];
  totalAmt: any = 0;

  cartItemCount: BehaviorSubject<number>;
  constructor(private cartService: CartService, 
    private modalCtrl: ModalController, 
    private alertCtrl: AlertController,
    private apiService: CallApisService,
    private router: Router,
    private commonService: CommonService,
    public modalController:ModalController) {}
 
  ngOnInit() {
    this.cart = this.cartService.getCart();
    this.cartItemCount = this.cartService.getCartItemCount();
    console.log(this.cart);
  }
 
  decreaseCartItem(product) {
    this.cartService.decreaseProduct(product);
  }
 
  increaseCartItem(product) {
    this.cartService.addProduct(product);
  }
 
  removeCartItem(product) {
    this.cartService.removeProduct(product);
  }
 
  getTotal() {
    // return this.cart.reduce((i, j) => i + j.price * j.discountPrice, 0);
    let total: any = 0;
    for(var i = 0; i<this.cart.length; i++){
      if(this.cart[i].price){
        total += this.cart[i].price * this.cart[i].quantity;
        this.totalAmt = total;
      }
    }
    return total;
  }

  close() {
    this.modalCtrl.dismiss();
  }
 
  checkout()  {
    let cartItem = this.cartService.getCartItemCount();
     let cartCount =  cartItem.getValue();
     console.log(cartCount);
     
     if(cartCount <= 0){
       this.router.navigate(['/tabs/tab2']);
     }
     else{
      let token = localStorage.getItem('token');
      if(token){
        this.router.navigate(['/delivery-address']);
      }
      else{
        this.router.navigate(['/log-in']);
      }
     }
    // Perfom PayPal or Stripe checkout process
    // let alert = await this.alertCtrl.create({
    //   header: 'Thanks for your Order!',
    //   message: 'We will deliver your food as soon as possible',
    //   buttons: ['OK']
    // });
    // alert.present().then(() => {
    //   this.modalCtrl.dismiss();
    // });
  }

  async openmodal() {
    const modal = await this.modalController.create({
      component: SearchModalPagePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
 
 
  

  
}
