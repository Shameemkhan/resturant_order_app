import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
// import { GoogleMap, GoogleMapsEvent, GoogleMapsLatLng } from '@ionic';
import { GoogleMapPageRoutingModule } from './google-map-routing.module';
// import { AgmCoreModule} from '@agm/core';
import { GoogleMapPage } from './google-map.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoogleMapPageRoutingModule,
    // AgmCoreModule.forRoot({
    //   apiKey: 'AIzaSyA9DBvmQlAfYF_HqRDNhvBEC4cdWIZwDo0'
    // })
  ],
  declarations: [GoogleMapPage]
})
export class GoogleMapPageModule {}
