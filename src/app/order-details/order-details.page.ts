import { CommonService } from './../common.service';
import { CallApisService } from './../call-apis.service';
import {  CartService } from '../services/cart.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable, Observer } from 'rxjs';
import { IonicHttpService } from '../ionic-http.service';
import { SearchModalPagePage } from '../search-modal-page/search-modal-page.page';
import { AlertController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.page.html',
  styleUrls: ['./order-details.page.scss'],
})
export class OrderDetailsPage implements OnInit {
  private currentNumber = 0;
  storeId: any;
  orderProduct: any[] = [];
  momentjs: any = moment;
  imageUrl: any;
  orderItem: any = {}
  orderDate: any;
  orderId: any;
  moNumber: any;
  orderDetails: any = {}
  products: any;
  saveUser :any = {};
  showTotalQuantity: any;
  spinner: boolean = false;
  display: any;
  totalTaxAmount: number = 0;

  constructor(private cartService: CartService ,
    private apiService: CallApisService,  
    private route: ActivatedRoute,  
    private ionicHttp: IonicHttpService,
    private router: Router,
    public modalController:ModalController,
    public commonService: CommonService,
    public alertController: AlertController) {
      this.orderId =  this.route.snapshot.paramMap.get('id');
     }

  

  ngOnInit() {
    this.spinner = true;
    this.imageUrl = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/files/';
    // this.orderProduct.forEach((data: any) => {
    //   console.log(data)
    //   data.forEach((data: any) => {
    //     this.showTotalQuantity = data.quantity;
    //   })
      
    // })
    

    this.apiService.storeId.subscribe((res: any) => {
      this.storeId = res;
    });

   
    
   

    // this.apiService.typeValue.subscribe((res: any) => {
    //   this.moNumber = res;
    // });

    this.moNumber = localStorage.getItem('moNumber');
    
    this.ionicHttp.getUserDetails(this.moNumber).then((res: any) => {
     let data = JSON.parse(res.data)
     this.saveUser = data;
     })
     .catch(err => {
       console.log(err)
     })
     

     this.apiService.getOrderDetails(this.orderId)
    .subscribe((data: any) => {
      this.totalTaxAmount = data.details.itemTotalTaxes
      let date = new Date(data.currentDate);
      this.orderDate = date;
      this.spinner = false;
      // this.commonService.dismiss();
    // alert('api works')
      
      data.items.forEach((data: any) => {
        this.orderProduct.push(data)

    });
    
    this.orderDetails = data.details;
    this.orderItem = data;
    this.getItemImage();
  },
  (err: any) => {
    console.log(err);
  }); 

  

     this.apiService.getUserData(this.moNumber)
     .subscribe((data: any) => {
       this.saveUser = data;
      //  console.log(this.saveUser);
     })
}

ionViewWillEnter(){
  this.spinner = true;
  // this.commonService.present();
    // this.moNumber = sessionStorage.getItem('moNumber');


  // this.getItemImage();

  
  
  }


getBase64ImageFromURL(url: string) {
  return Observable.create((observer: Observer<string>) => {
    let img = new Image();
    img.crossOrigin = 'Anonymous';
    img.src = url;  img.src = url;
    if (!img.complete) {
      img.onload = () => {
        observer.next(this.getBase64Image(img));
        observer.complete();
      };
      img.onerror = (err) => {
        observer.error(err);
      };
    } else {
      observer.next(this.getBase64Image(img));
      observer.complete();
    }
  });
}

getBase64Image(img: HTMLImageElement) {
  var canvas = document.createElement("canvas");
  canvas.width = 180;
  canvas.height = 180;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0, 180, 180);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

getItemImage(){
    this.orderProduct.forEach((data: any) => {
      if(data.image_url != null){
        console.log(data)
        // alert('if works')
        this.getBase64ImageFromURL(this.imageUrl + data.image_url ).subscribe(base64data => {
          data.base64Image = 'data:image/jpg;base64,' + base64data;
          this.orderProduct.push(data)
          this.makeUnique()
        });
      }else{
        // alert('else works')
          this.orderProduct.push(data);
          this.makeUnique()
      }
    });
}

getTotalQuantity() {
    // return this.cart.reduce((i, j) => i + j.price * j.discountPrice, 0);
    let total: any = 0;
    for(var i = 0; i<this.orderProduct.length; i++){
      if(this.orderProduct[i].price){
        total +=  this.orderProduct[i].quantity;
        this.showTotalQuantity = total;
        this.makeUnique();
      }
    }
    return total;
  }

  async orderCancle(){
    const alert = await this.alertController.create({
      header: 'Alert',
      message: 'Please confirm to cancel the order',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Confirm',
          handler: () => {
            this.apiService.orderCancleStatus(this.orderId, this.storeId);
          }
        }
      ]
    });

    await alert.present();
   
  }

  async openmodal() {
    const modal = await this.modalController.create({
      component: SearchModalPagePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  
 
    makeUnique(){
    let cities = [...new Set(this.orderProduct)]
    this.orderProduct = []
    cities.forEach((data: any) => {
      this.orderProduct.push(data);
    })
    
  }

  gotoChangePassword(){
    this.router.navigate(['/change-password'])
  }
  
  gotoMyOrder(){
    this.router.navigate(['/my-order'])
  }
  
  gotoAddress(){
    this.router.navigate(['/save-address'])
  }
}
