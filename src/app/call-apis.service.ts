
import { Router } from '@angular/router';
import { AuthData } from './models/auth-data-model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CallApisService {

  baseUrl = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/';
  imageUrl = 'https://www.ere4u.in/thirdparty/uploads/p1.jpg';
  stId: any;
  constructor(private httpClient: HttpClient, 
    private router: Router) { 
      this.stId = localStorage.getItem('storeId');
  }

  typeValue = new BehaviorSubject<string>("");
  private authStatusListner = new Subject<boolean>();
  catagoryData = new BehaviorSubject<any>("");
  otp = new BehaviorSubject<string>("");
  userId = new BehaviorSubject<string>('');
  pass = new BehaviorSubject<string>('');
  customerId = new BehaviorSubject<string>('');
  storeId = new BehaviorSubject<string>('')
  forItemLoading = new BehaviorSubject<boolean>(false);
  subTotalAmt = new BehaviorSubject<string>('');
  totalAmt = new BehaviorSubject<string>('');
  savePrice = new BehaviorSubject<string>('');
  grandTotal = new BehaviorSubject<number>(0);

  date = new BehaviorSubject<any>('');

   order: any = {
     items: []
   }

   searchItems = new BehaviorSubject<any[]>([]);

  getAuthStatusListener(){
    return this.authStatusListner.asObservable();
  }

  createUser(username: string, password: string,){
    const authData: AuthData = {
      username: username,
      password: password
    };
    this.httpClient.post(this.baseUrl + "otp/generateOtp", authData, {
      headers: new HttpHeaders({
          'Content-Type': 'application/json',
          "X-Requested-With": "XMLHttpRequest"
      }),
      withCredentials: true
   })
    .subscribe(res => {
      console.log(res);
    }
    ,err => {
      console.log(err);
    });
  }

  getOtp(typeValue: string){
    console.log(typeValue);
     return this.httpClient.post(this.baseUrl + "otp/generateOtp", {"typeValue": typeValue});
  }

  login(username: string, password: string){
    const authData: AuthData = {
      username: username,
      password: password
    }
    console.log(password);
      return this.httpClient.post(this.baseUrl + "auth/authenticate", authData)
      .pipe(map((response: any) => {
        let result = response;
        if(result && result.token){
          localStorage.setItem("token", result.token);
          this.authStatusListner.next(true);
          return true;
        }
        return false;
      }));
  }

  signUp(username: string, password: string){
      const user: any = {
        "username": username,
        "password": password
      }
     return this.httpClient.post(this.baseUrl + "auth/register", user, {
      headers: new HttpHeaders({
          'Content-Type': 'application/json',
      }),
      withCredentials: true
   });
  }



  getAllItems(){
  return this.httpClient.get(this.baseUrl + "item/all", {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    }),
    withCredentials: true
    });
  }

  varifyUser(username: string){
   return this.httpClient.post(this.baseUrl + "user/verify/user", {"username": username})
    .pipe(map((res: any) => {
      let result = res;
      if(result.status === 200)
        return true;
      else
        return false;
    }));
  }

  varifyOtp(moNumber: any, otp: string){
    return this.httpClient.put(this.baseUrl + "otp/mobile/authenticate", {"typeValue": moNumber , "otp": otp});
  }

  logout(){
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    sessionStorage.removeItem('userId');
    localStorage.removeItem('moNumber');
    this.router.navigate(['/tabs/tab1']);
    this.authStatusListner.next(false);
  }

  fetchImages(){
    return this.httpClient.get(this.baseUrl + "item/files/cheesePizzaS.jpg", {responseType: 'blob'});
  }

  getCatagories(){
    return this.httpClient.get(this.baseUrl + "category/" + this.stId);
  }

  checkUserLogin(){
    const myData = localStorage.getItem('token');
    // console.log(myData);
    // console.log(myData);
    if(myData){
      this.router.navigate(['/tabs/tab4'])
    }
    else{
    this.router.navigate(['/log-in'])
    }
  }

  saveUser(formData: any){
    let token = localStorage.getItem('token');
   return  this.httpClient.post(this.baseUrl + "user/update", formData, {
      withCredentials: true,
      headers: new HttpHeaders({
        Authorization: "Bearer  " + token,
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": 'https://localhost:8100',
      })
   });

    // "X-Requested-With": "XMLHttpRequest",
    // "Access-Control-Allow-Headers": "X-Requested-With,content-type"

//   let header = new HttpHeaders().set(
//     "Authorization", "Bearer  " + localStorage.getItem("token")
//   ).set("content-type", "application/json");
// console.log(header);
//   this.http.post(this.saveUserInformation, userData, {headers: header}).subscribe(res => {
//        console.log(res);
//      },
//      (err) => {
//        console.log(err);
//      });
  }

  getAllUserInformation(){
    let token = localStorage.getItem('token');
    
    return this.httpClient.get(this.baseUrl + "user/all", {
      withCredentials: true,
      headers: new HttpHeaders({
        Authorization: "Bearer  " + token,
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*"
      })
    });
  }


  generateOrder(order: any){
    return this.httpClient.post(this.baseUrl + "order/save", order);
  }

  getOrdersData(){
   return this.httpClient.get(this.baseUrl + "order");
  }
  
  getUserData(moNumber){
    let token = localStorage.getItem('token');
    return this.httpClient.get(this.baseUrl + "user/" + moNumber ,{
      withCredentials: true,
      headers: new HttpHeaders({
        Authorization: "Bearer  " + token,
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*"
      })
    })
  }
 
  fetchOrderById(orderId: any){
   return this.httpClient.get(this.baseUrl + "order/getOne/" + orderId);
  }

  // getOederDetailsOfUser(customerId, date){
  //   return this.httpClient.get(this.getOrderDetails + customerId + "/" + date)
  // }

  getOrderDetailsOfUser(userId: any){
    return this.httpClient.get(this.baseUrl + "order/data/" + this.stId + '/' + userId)
  }

  getItemById(itemId: any){
    return this.httpClient.get(this.baseUrl + "item/" + itemId);
  }

  getParentCategoryData(){
    return this.httpClient.get(this.baseUrl + "category/" +  this.stId, {
      withCredentials: true,
      headers: new HttpHeaders({
        "content-type": "application/json"
      })
    });
  }

  getOrderDetails(orderId: any){
    return this.httpClient.get(this.baseUrl + "order/getOne/" + orderId);
  }

  cancleOrder(orderId: any){
    return this.httpClient.delete(this.baseUrl + "order/" + orderId);
    // .subscribe((res: any) => {
    //   console.log(res);
    // }
    // ,(err) => {
    //   console.log(err);
    // })
  }
  // http://164.52.194.61:8081/master_latest/app/index.html#/onlineOrder
  orderCancleStatus(orderId: any, refId: any){
      refId = localStorage.getItem('refId');
      let date = new Date();
      this.httpClient.put(this.baseUrl + "order/status/" + orderId + '/' + refId, {
        "date": date,
        "id":null,
        "message": 'Order is cancle by the user',
        "status": "Cancelled"
        })
      .subscribe((res: any) => {
        console.log(res)
        if(res){
          this.router.navigate(['/my-order']);
        }
      }
      ,(err) => {
        console.log(err);
      })
  }

  changePassword(oldPassword: any, userId: any, password: any){
    return this.httpClient.put(this.baseUrl + "user/changePassword/" + oldPassword, {
      "userId": userId,
      "password": password
    });
  }

  getAddOns(id){
    return this.httpClient.get(this.baseUrl + "item/" + id);
  }

  getForgotPasswordOtp(mobileNo: any){
    return this.httpClient.post(this.baseUrl + "otp/forgatePassword", {"mobileNo": mobileNo});
  }

  passwordForgot(userId: any, password: any){
    return this.httpClient.post(this.baseUrl + "auth/register", {"userId": userId, "password": password});
  }

  getStates(){
    return this.httpClient.get(this.baseUrl + "stateWiseCity/all");
  }

  getCities(stateName){
   return this.httpClient.get(this.baseUrl + "stateWiseCity/" + stateName)
  } 

  getPincodeDEtails(pincode, storeId){
    return this.httpClient.get(this.baseUrl + "pincode/code/" + pincode + '/' + storeId);
  }

  getAppconfigurationsProperties(){
    return this.httpClient.get(this.baseUrl + 'store' + "/" + "1");
  }
}


