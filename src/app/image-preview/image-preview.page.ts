import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-image-preview',
  templateUrl: './image-preview.page.html',
  styleUrls: ['./image-preview.page.scss'],
})
export class ImagePreviewPage implements OnInit {
  filteredProduct: any[] = [];
  itemImages: any[] = [];
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.filteredProduct =  this.commonService.get_itemList();
    console.log(this.filteredProduct)
  }

}
