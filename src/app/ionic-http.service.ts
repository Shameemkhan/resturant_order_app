import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';


@Injectable({
  providedIn: 'root'
})
export class IonicHttpService {

  getUserDeatilsByName = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/user/';
  saveUserInformation = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/user/update';
  token = localStorage.getItem('token');
  
 header = {
    Authorization: 'Bearer ' + this.token,
    'Content-Type': 'application/json; charset=utf-8' 
  }
  constructor(private http: HTTP) { }

  getUserDetails(moNumber){
   return this.http.get(this.getUserDeatilsByName + moNumber, {}, this.header);
  }

  saveUser(formData: any){
    this.http.setDataSerializer('json');
     return this.http.put(this.saveUserInformation, formData, {
        Authorization: 'Bearer ' + this.token
   });
  } 
}