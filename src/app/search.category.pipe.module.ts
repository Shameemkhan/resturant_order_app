import { NgModule } from '@angular/core';
import { SearchCategoryPipe } from './search-category.pipe';

@NgModule({
    imports: [],
    declarations: [SearchCategoryPipe],
    exports: [SearchCategoryPipe]
})
export class SearchCategoryPipeModule { }