import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HTTP } from '@ionic-native/http/ngx';
import { IonicModule } from '@ionic/angular';

import { DeliveryAddressPageRoutingModule } from './delivery-address-routing.module';

import { DeliveryAddressPage } from './delivery-address.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeliveryAddressPageRoutingModule
  ],
  providers: [HTTP],
  declarations: [DeliveryAddressPage]
})
export class DeliveryAddressPageModule {}
