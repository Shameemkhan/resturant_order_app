import { CommonService } from './../common.service';
import { Router } from '@angular/router';
import { IonicHttpService } from './../ionic-http.service';
import { UserAddressService } from './../user-address.service';
import { NavController } from '@ionic/angular';
import { CallApisService } from './../call-apis.service';
import { Component, OnInit } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-delivery-address',
  templateUrl: './delivery-address.page.html',
  styleUrls: ['./delivery-address.page.scss'],
})
export class DeliveryAddressPage implements OnInit {

  saveUser :any = {
    
  };
  moNumber = '';
  keys: any;
  userData :any[] = [];
  customerId: any;
  spinner: boolean = false;
  constructor(private apiService: CallApisService, 
    private nav: NavController,  
    private http: HTTP, 
    private ionicHttp: IonicHttpService,
    private router: Router,
    private commonService: CommonService,
    private cartService: CartService) { }


  ngOnInit() {
    this.moNumber = sessionStorage.getItem('moNumber');
    // this.getUserData();
  }

  ionViewWillEnter(){
    this.spinner = true;

    this.moNumber = localStorage.getItem('moNumber');


     this.apiService.getUserData(this.moNumber).subscribe((res: any) => {
       this.spinner = false;
       if(res.fristName === null){
        // this.commonService.dismiss();
        this.customerId = res.userId;
        localStorage.setItem('userId', this.customerId);
         this.router.navigate(['/address-details'])
      }
      else{
        // this.commonService.dismiss();
        this.customerId = res.userId;
        localStorage.setItem('pincode', res.pincode)
        localStorage.setItem('userId', this.customerId);
         res.customer.forEach((data: any) => {
           this.apiService.customerId.next(this.customerId);
 
         })
        this.saveUser = res;
      }
      
     });
   }

   checkCart(){
    let cart = this.cartService.getCart();
    if(cart.length === 0){
      this.router.navigate(['/tabs/tab2']);
    }
    else{
      this.router.navigate(['/coupon'])
    }
   }
  
  gotoCheckOut(){
    this.router.navigate(['/check-out'])
  }

    // this.getUserData()  
  }

  // gotoCheckOut(){
  //   this.router.navigate(['/check-out'])
  // }

  // getUserData(){
  //   console.log(this.moNumber);
  //    this.ionicHttp.getUserDetails(this.moNumber).then((res: any) => {
  //         this.spinner = false;
  //       let data = JSON.parse(res.data)
  //       console.log(data);
  //       this.spinner = false;
  //       // if(data.fristName === null){
  //       //   this.customerId = data.userId;
  //       //   this.apiService.customerId.next(this.customerId);
  //         localStorage.setItem('userId', this.customerId);
  //       //    this.router.navigate(['/address-details'])
  //       // }
  //       // else{
  //         this.customerId = data.userId;
  //         this.apiService.customerId.next(this.customerId);
  //         localStorage.setItem('userId', this.customerId);
  //         this.saveUser = data;
  //       // }
      
  //      //  console.log(this.saveUser);
  //     // console.log(res);
  //    })
  //    .catch(err => {
  //     //  alert('error works')
  //     //  alert(err);
  //      console.log(err)
  //    })
  // }


  // checkCart(){
  //   let cart = this.cartService.getCart();
  //   if(cart.length === 0){
  //     this.router.navigate(['/tabs/tab2']);
  //   }
  //   else{
  //     this.router.navigate(['/coupon'])
  //   }
  //  }

   
  // }