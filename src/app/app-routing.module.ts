import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },  
  {
    path: 'signup',
    loadChildren: () => import('./signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'log-in',
    loadChildren: () => import('./log-in/log-in.module').then( m => m.LogInPageModule)
  },
  {
    path: 'forget-password',
    loadChildren: () => import('./forget-password/forget-password.module').then( m => m.ForgetPasswordPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'savedaddresses',
    loadChildren: () => import('./savedaddresses/savedaddresses.module').then( m => m.SavedaddressesPageModule)
  },
  {
    path: 'savedaddresses/:userId',
    loadChildren: () => import('./savedaddresses/savedaddresses.module').then( m => m.SavedaddressesPageModule)
  },
  {
    path: 'order-details',
    loadChildren: () => import('./order-details/order-details.module').then( m => m.OrderDetailsPageModule)
  },
  {
    path: 'order-details/:id',
    loadChildren: () => import('./order-details/order-details.module').then( m => m.OrderDetailsPageModule)
  },
  {
    path: 'orderplaced',
    loadChildren: () => import('./orderplaced/orderplaced.module').then( m => m.OrderplacedPageModule)
  },
  
  {
    path: 'searchproduct',
    loadChildren: () => import('./searchproduct/searchproduct.module').then( m => m.SearchproductPageModule)
  },
  {
    path: 'cart-modal',
    loadChildren: () => import('./pages/cart-modal/cart-modal.module').then( m => m.CartModalPageModule)
  },
  {
    path: 'otp',
    loadChildren: () => import('./otp/otp.module').then( m => m.OtpPageModule)
  },
  {
    path: 'password',
    loadChildren: () => import('./password/password.module').then( m => m.PasswordPageModule)
  },
  {
    path: 'item-details',
    loadChildren: () => import('./item-details/item-details.module').then( m => m.ItemDetailsPageModule)
  },
  {
    path: 'item-details/:id',
    loadChildren: () => import('./item-details/item-details.module').then( m => m.ItemDetailsPageModule)
  },
  {
    path: 'delivery-address',
    loadChildren: () => import('./delivery-address/delivery-address.module').then( m => m.DeliveryAddressPageModule)
  },
  {
    path: 'coupon',
    loadChildren: () => import('./coupon/coupon.module').then( m => m.CouponPageModule)
  },
  {
    path: 'makepayment',
    loadChildren: () => import('./makepayment/makepayment.module').then( m => m.MakepaymentPageModule)
  },
  {
    path: 'change-password',
    loadChildren: () => import('./change-password/change-password.module').then( m => m.ChangePasswordPageModule)
  },
  {
    path: 'save-address',
    loadChildren: () => import('./save-address/save-address.module').then(m => m.SaveAddressPageModule)
  },
  {
    path: 'my-order',
    loadChildren: () => import('./my-order/my-order.module').then( m => m.MyOrderPageModule)
  },
   {
    path: 'address-details',
    loadChildren: () => import('./address-details/address-details.module').then( m => m.AddressDetailsPageModule)
  },
  {
    path: 'address-details/:userId',
    loadChildren: () => import('./address-details/address-details.module').then( m => m.AddressDetailsPageModule)
  },
  {
    path: 'edit-addressdetails/:userId',
    loadChildren: () => import('./edit-addressdetails/edit-addressdetails.module').then( m => m.EditAddressdetailsPageModule)
  },
  {
    path: 'dialog',
    loadChildren: () => import('./dialog/dialog.module').then( m => m.DialogPageModule)
  },
  {
    path: 'otp-change-password',
    loadChildren: () => import('./otp-change-password/otp-change-password.module').then( m => m.OtpChangePasswordPageModule)
  },
  {
    path: 'otp-forgot-password',
    loadChildren: () => import('./otp-forgot-password/otp-forgot-password.module').then( m => m.OtpForgotPasswordPageModule)
  },
  {
    path: 'add-ons',
    loadChildren: () => import('./add-ons/add-ons.module').then( m => m.AddOnsPageModule)
  },
  {
    path: 'add-ons/:id',
    loadChildren: () => import('./add-ons/add-ons.module').then( m => m.AddOnsPageModule)
  },
  {
    path: 'search-modal-page',
    loadChildren: () => import('./search-modal-page/search-modal-page.module').then( m => m.SearchModalPagePageModule)
  },
  {
    path: 'google-map',
    loadChildren: () => import('./google-map/google-map.module').then( m => m.GoogleMapPageModule)
  },
  {
    path: 'policy',
    loadChildren: () => import('./policy/policy.module').then( m => m.PolicyPageModule)
  },
  {
    path: 'check-out',
    loadChildren: () => import('./check-out/check-out.module').then( m => m.CheckOutPageModule)
  },
  {
    path: 'edit-add-ons',
    loadChildren: () => import('./edit-add-ons/edit-add-ons.module').then( m => m.EditAddOnsPageModule)
  },
  {
    path: 'agm-map',
    loadChildren: () => import('./agm-map/agm-map.module').then( m => m.AgmMapPageModule)
  },
  {
    path: 'image-preview',
    loadChildren: () => import('./image-preview/image-preview.module').then( m => m.ImagePreviewPageModule)
  },
  {
    path: 'loading',
    loadChildren: () => import('./loading/loading.module').then( m => m.LoadingPageModule)
  },









  // {
  //   path: 'catagories-details/:id',
  //   loadChildren: () => import('./catagories-details/catagories-details.module').then( m => m.CatagoriesDetailsPageModule)
  // },

  

  // {
  //   path: 'tab5',
  //   loadChildren: () => import('./tab5/tab5.module').then( m => m.Tab5PageModule)
  // },
  // {
  //   path: 'tab4',
  //   loadChildren: () => import('./tab4/tab4.module').then( m => m.Tab4PageModule)
  // }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
