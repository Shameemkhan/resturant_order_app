import { CommonService } from './../common.service';
import { Router } from '@angular/router';
import { CallApisService } from './../call-apis.service';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { BehaviorSubject, Observable, Observer } from 'rxjs';
import { Items } from '../models/items';
import { CartService } from '../services/cart.service';
import { SearchModalPagePage } from '../search-modal-page/search-modal-page.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-my-order',
  templateUrl: './my-order.page.html',
  styleUrls: ['./my-order.page.scss'],
})
export class MyOrderPage implements OnInit {
  orderProduct: any[] = [];
  orderItems: Items[] = [];
  cartItemCount: BehaviorSubject<number>;
  momentjs: any = moment;
  conv: any[] = [];
  conv2: any[] = [];
  v3: any;
  param2: any;
  date: any;
  customerId: any;
  customerDate: any;
  orderDate: any;
  imageUrl: any;
  orderData: any[] = []
  orderId: any[] = [];
  spinner: boolean = false;
  statuCheck: boolean = false;


  constructor(private cartService: CartService, 
    private apiService: CallApisService, 
    public modalController:ModalController,
    private router: Router,
    private commonService: CommonService) { 
  }

  
ionViewWillEnter(){
  // this.commonService.present();
  this.spinner = true;
  this.cartItemCount = this.cartService.getCartItemCount();
  this.getOrders();

  this.statuCheck
}

  ngOnInit() {
    this.imageUrl = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/files/';
    this.cartItemCount = this.cartService.getCartItemCount();
    // const curDate = new Date();
    // this.date = moment().format('Do MMMM YYYY');
    // this.customerDate = moment().format('L');
    // let myDate = this.date.toString();
    // var oldDate = moment(curDate).subtract(1, 'minutes').toDate();
    // let timeAndDate = oldDate.toString();
    // let trimTimeAndDate = timeAndDate.substr(15, 10)
    // this.conv.push(myDate, trimTimeAndDate);
    // for(var i = 0; i < this.conv.length; i++)
    // { 
    //     this.conv2.push(this.conv[i])
    // }
    // let v = this.conv2.toString();
    // let v2 = v.split(',').join("");

    // this.v3 = v2.trim();

    
    this.apiService.customerId.subscribe((res: any) => {

      this.customerId = res;
      // console.log(this.customerId)
    })
  
}



getBase64ImageFromURL(url: string) {
  return Observable.create((observer: Observer<string>) => {
    let img = new Image();
    img.crossOrigin = 'Anonymous';
    img.src = url;  img.src = url;
    if (!img.complete) {
      img.onload = () => {
        observer.next(this.getBase64Image(img));
        observer.complete();
      };
      img.onerror = (err) => {
        observer.error(err);
      };
    } else {
      observer.next(this.getBase64Image(img));
      observer.complete();
    }
  });
}

getBase64Image(img: HTMLImageElement) {
  var canvas = document.createElement("canvas");
  canvas.width = 180;
  canvas.height = 180;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0, 180, 180);
  var dataURL = canvas.toDataURL("image/png");
  // console.log(dataURL);
  return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

getOrders(){
  let imagesForItem: any[] = [];
  let images = {
    src: ''
  }
  let userId = localStorage.getItem('userId');
  this.apiService.getOrderDetailsOfUser(userId)
  .subscribe((res: any[]) => {
    this.spinner = false;
    // this.commonService.dismiss();
      this.orderData = res.reverse()
      console.log(this.orderData);
      // if(this.orderData.length === 0){
      //   alert('You dont have any order please make order first!');
      //   this.router.navigate(['/tabs/tab2']);
      // }
      // else{
        this.orderData.forEach((data: any) => {
          // this.orderDate = data.date;
        // console.log(data)
          // this.orderDate = moment().format('Do MMMM YYYY');
       
          data.items.forEach((itemsData: any) => {
            console.log(itemsData)
            itemsData.item.forEach((data: any) => {
              if(data.itemImages.length != 0){
                for(let i=0;i<data.itemImages.length;i++){
                   this.getBase64ImageFromURL(this.imageUrl + data.itemImages[i].img_name).subscribe(base64data => {
                    itemsData.base64Image = 'data:image/jpg;base64,' + base64data;
                    // images.src = 'data:image/jpg;base64,' + base64data;
                    // imagesForItem.push(images)
                    // data.itemsImages = imagesForItem
                    // this.orderItems.push(itemsData)
                    // let uniqueItemData = [...new Set(imagesForItem)]
                    //   imagesForItem = []
                    //   uniqueItemData.forEach((data: any) => {
                    //     imagesForItem.push(data);
                    //   })
                    });
                  }
                }
            })
          })
        })
      // }
  
  },
  (err) => {
    console.log(err);
  });
}

gotoTab3(){
  this.router.navigate(['/tabs/tab2'])
}

async openmodal() {
  const modal = await this.modalController.create({
    component: SearchModalPagePage,
    cssClass: 'my-custom-class'
  });
  return await modal.present();
}

gotoTab2(){
  this.router.navigate(['/tabs/tab2'])
}

gotoChangePassword(){
  this.router.navigate(['/change-password'])
}

gotoMyOrder(){
  this.router.navigate(['/my-order'])
}

gotoAddress(){
  this.router.navigate(['/save-address'])
}
  
  }