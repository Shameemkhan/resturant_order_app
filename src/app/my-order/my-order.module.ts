import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyOrderPageRoutingModule } from './my-order-routing.module';

import { MyOrderPage } from './my-order.page';
import { UniquePipe } from '../unique.pipe';
import { UniquePipeModule } from '../unique.pipe.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyOrderPageRoutingModule,
    UniquePipeModule
  ],
  declarations: [MyOrderPage, ]
})
export class MyOrderPageModule {}
