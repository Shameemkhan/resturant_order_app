import { Router, RouterEvent } from '@angular/router';
import { CallApisService } from './../call-apis.service';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SearchModalPagePage } from '../search-modal-page/search-modal-page.page';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {
  cartItemCount: any;
  token: any;
  selectedPath: any = '';
  
  thetab = {isYellow: true, tabname: 'Hello'}
  constructor(private apiService: CallApisService, private router: Router,  private cartService: CartService) {

  }
  
  ngOnInit(){
    this.cartItemCount = this.cartService.getCartItemCount();
    this.token = localStorage.getItem('token');

    this.router.events.subscribe((event: RouterEvent) => {
      if(event && event.url){
        this.selectedPath = event.url;
      }
    });
  }



  ionViewDidEnter() {
    let elem = <HTMLElement>document.querySelector(".tabbar");
    if (elem != null) {
      elem.style.display = 'none';
    }
  }


  ontabChange(event){
    if(event.tab.isActive){
      this.thetab.isYellow = !this.thetab.isYellow;
    }
    // this.thetab.isYellow = !this.thetab.isYellow;
  }

  getOrder(){
   let token = localStorage.getItem('token');
    console.log(token);
    if(token){
      this.router.navigate(['/my-order']);
    }
    else{
      this.router.navigate(['/tabs/tab3']);
    }
  }

  getToCart(){
    this.router.navigate(['/tabs/tab4'])
    
  }

  goToCheckOut(){
    this.router.navigate(['/google-map'])
  }
}
