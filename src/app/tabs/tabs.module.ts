import { SuperTabsModule } from '@ionic-super-tabs/angular';
import { SearchCategoryPipeModule } from './../search.category.pipe.module';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    SuperTabsModule,
    TabsPageRoutingModule,
    SearchCategoryPipeModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
