import { SearchCategoryPipeModule } from './../search.category.pipe.module';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    IonicImageLoader,
    FormsModule,
    ExploreContainerComponentModule,
    SearchCategoryPipeModule,
    RouterModule.forChild([{ path: '', component: Tab1Page }])
  ],
  declarations: [Tab1Page]
})
export class Tab1PageModule {z}
