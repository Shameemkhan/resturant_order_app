import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { AttributeService } from './../attribute.service';
import { CommonService } from './../common.service';
import { Items } from './../models/items';

import { Component, OnDestroy, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { CartService } from '../services/cart.service';
import { Route, Router } from '@angular/router';
import { ActionSheetController, AlertController, IonSlides, ModalController, Platform } from '@ionic/angular';
import { Data1Service } from '../data1.service';
import { CallApisService } from '../call-apis.service';
import { SearchModalPagePage } from '../search-modal-page/search-modal-page.page';
import { Observable, Observer, Subscription } from 'rxjs';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderOptions, NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit, OnDestroy  {

  filteredProduct: any[] = [];
  //  shoplist: any [];
   shopsDetail: any [];
   itemList: any[] = [];
   data2: any[] = [];
   items: any[] = [];
   imageUrl: any;
   popularItems: any[] = [];
   popularItemSubscription: Subscription;
   storeId: number = 8;

   address: string;
    // Location coordinates
  latitude: number;
  longitude: number;
  accuracy: number;

  geoencoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };

   slideOptions = {
     zoom: false,
    initialSlide: 1,
    speed: 1000,
  };
 
  itemLists: any[] = [];

  constructor(private commonService: CommonService, private Http: HttpClient,private data:Data1Service,
    private router:Router, private apiService: CallApisService,
    public modalContoller:ModalController,
    public alertCtrl: AlertController,
    public modalController:ModalController,
    private cartService: CartService,
    public localNotifications: LocalNotifications,
    public actionSheetController: ActionSheetController,
    private locationAccuracy: LocationAccuracy,
    private androidPermissions: AndroidPermissions) {
      if(this.commonService.get_itemList().length === 0){
        this.getExistingData();
      }
    }
  ngOnDestroy(): void {
      // this.popularItemSubscription.unsubscribe();
  }
 ngOnInit() {
  // this.imageUrl = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/files/';

  this.shopsDetail=this.data.getShopsDetails()
    this.popularItemSubscription =  this.commonService.sharedParam.subscribe((popularItems: any) => {
   this.popularItems = popularItems;
 })
  // console.log(this.popularItems)
// this.getItemImage();
  
 }

 ionViewWillEnter(){
 
  this.imageUrl = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/files/';
  this.filteredProduct = this.commonService.get_itemList();
  // this.getItemImage();

  //enable these two method for the permission of location
  // this.checkGPSPermission();
  // this.requestGPSPermission();

 
 }

 checkGPSPermission() {
  this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
    result => {
      if (result.hasPermission) {

        //If having permission show 'Turn On GPS' dialogue
        this.askToTurnOnGPS();
      } else {

        //If not having permission ask for permission
        this.requestGPSPermission();
      }
    },
    err => {
      // alert(err);
    }
  );
}

requestGPSPermission() {
  this.locationAccuracy.canRequest().then((canRequest: boolean) => {
    if (canRequest) {
    } else {
      //Show 'GPS Permission Request' dialogue
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
        .then(
          () => {
            // call method to turn on GPS
            this.askToTurnOnGPS();
          },
          error => {
            //Show alert if user click on 'No Thanks'
            // alert('requestPermission Error requesting location permissions ' + error)
          }
        );
    }
  });
}

askToTurnOnGPS() {
  this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
    () => {
      // When GPS Turned ON call method to get Accurate location coordinates
      // this.getLocationCoordinates()
    },
    // error => alert('Error requesting location permissions ' + JSON.stringify(error))
  );
}

 getBase64ImageFromURL(url: string) { 
  return Observable.create((observer: Observer<string>) => {
   
    let img = new Image();
    img.crossOrigin = 'Anonymous';
    img.src = url;  img.src = url;
    if (!img.complete) {
      img.onload = () => {
        observer.next(this.getBase64Image(img));
        observer.complete();
      };
      img.onerror = (err) => {
        observer.error(err);
      };
    } else {
      observer.next(this.getBase64Image(img));
      observer.complete();
    }
  });
  
}

getBase64Image(img: HTMLImageElement) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  // console.log(dataURL);
  return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

// getItemImage(){
  
//   this.filteredProduct.forEach(data => {
//     if(data.img_name != null){
//       this.getBase64ImageFromURL(this.imageUrl + data.img_name ).subscribe(base64data => {
//         data.base64Image = 'data:image/jpg;base64,' + base64data;
//         this.filteredProduct.push(data);
//         this.makeUnique();
//       });
//     }else{}
//   });   
// }


 async openmodal(){
  const modal = await this.modalController.create({
    component: SearchModalPagePage,
    cssClass: 'my-custom-class'
  });
  return await modal.present();
 }

 getExistingData(){
 this.apiService.getAllItems().subscribe((itemRes: any[]) => {   
    if(itemRes){
     let res = itemRes.filter((data: any) => {
       return data.storeId === this.storeId
    })

    
    res.forEach((res: any) => {
     //  if(res.optionGroup.length != 0){
     //    res.optionGroup.forEach((data: any) => {
     //      if(!data.refId.includes('VOG')){
     //       data.options.forEach((optionsData: any) => {
     //         if(optionsData.ref_id.includes('OGI'))
     //         optionsData.qty = 0;
     //         data.options.push(optionsData);
     //         let uniqueOptionsData = [...new Set(data.options)]
     //         data.options = []
     //         uniqueOptionsData.forEach((data1: any) => {
     //           data.options.push(data1);
     //         })
     //       });
     //      }
     //    });
     //  }

      if(res.quantity === null){
        res.quantity = 0;
      }
      if(res.categories === null){

     }
     else{
      res.categories.forEach((data: any) => {
        if(data.name === "Popular"){
          this.popularItems.push(res);
        }
      });
      
     }
   });
     this.commonService.passItems(this.popularItems);
     this.filteredProduct = res;  
     this.commonService.set_itemList(this.filteredProduct);
    }
    else{
     
    }
   
   });

  this.apiService.getCatagories()
     .subscribe((res: any) => {
       this.commonService.set_categoryList(res);
       res.forEach((data: any) => {
         this.apiService.storeId.next(data.storeId)
       });
   },
      (err: any) => {
       console.log(err);
    });
 }

 makeUnique(){
  let cities = [...new Set(this.filteredProduct)]
  this.filteredProduct = []
  cities.forEach((data: any) => {
    this.filteredProduct.push(data);
    // console.log(this.filteredProduct)
    this.commonService.set_itemList(this.filteredProduct);
  })
  
}

 goToProducts(id){
  this.router.navigate(['products',id])
}

getNotification(){
  this.localNotifications.schedule({
    id: 1,
    text: 'You have a 50% offers on the saturday',
    data: { secret: 'secret' }
  });
}

slidesDidLoad(slides: IonSlides) {
  slides.startAutoplay();
}

  async incementQty(product: Items){
  this.cartService.addProduct(product);
 
}

decrementQty(product: Items){
 this.cartService.decreaseProduct(product);

}

// makeUnique(){
//   let cities = [...new Set(this.filteredProduct)]
//   this.filteredProduct = []
//   cities.forEach((data: any) => {
//     this.filteredProduct.push(data);
//   })
  
// }


}
