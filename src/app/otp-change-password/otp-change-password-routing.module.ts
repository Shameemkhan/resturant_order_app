import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OtpChangePasswordPage } from './otp-change-password.page';

const routes: Routes = [
  {
    path: '',
    component: OtpChangePasswordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OtpChangePasswordPageRoutingModule {}
