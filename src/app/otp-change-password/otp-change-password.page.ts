import { CommonService } from './../common.service';
import { Router } from '@angular/router';
import { CallApisService } from './../call-apis.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { IonicHttpService } from '../ionic-http.service';
import { ModalController } from '@ionic/angular';
import { SearchModalPagePage } from '../search-modal-page/search-modal-page.page';

@Component({
  selector: 'app-otp-change-password',
  templateUrl: './otp-change-password.page.html',
  styleUrls: ['./otp-change-password.page.scss'],
})
export class OtpChangePasswordPage implements OnInit {
  // newPassword: any;
  // new_password: any;
  submitted: boolean = false;
  constructor(private ionicHttp: IonicHttpService, 
    private apiService: CallApisService,
    private router: Router,
    public modalController:ModalController,
    public commonService: CommonService) { }

  form = new FormGroup({
    // userId: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    confirmPassword: new FormControl('', Validators.compose([
      Validators.required
    ]))
  }, {
    validators: this.password.bind(this)
  })

  ngOnInit() {

  }

  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: confirmPassword } = formGroup.get('confirmPassword');
    return password === confirmPassword ? null : { passwordNotMatch: true };
  }

  f(controls) {
    return this.form.get(controls);
  }
 
   onSubmit(){
      this.submitted = true;

      if(this.form.invalid){
         return;
      }

    let userId = sessionStorage.getItem('userId');
    let password = this.form.get('password').value;
    console.log(password);
    console.log(userId)
    // this.form.value.userId = userId;
    // console.log(this.form.value)
    this.apiService.passwordForgot(userId, password)
      .subscribe((res: any) => {
        console.log(res);
        if(res){
          sessionStorage.removeItem('userId')
          this.router.navigate(['/tabs/tab1']);
        }
      }
      ,(err) => {
        console.log(err);
      });
    }

 

  // changePassword(){
  //   let userId = sessionStorage.getItem('userId');
  //   let password = this.form.value.password;
  //   console.log(password);
  //   console.log(userId)
  //   this.form.value.userId = userId;
  //   // console.log(this.form.value)
  //   this.apiService.passwordForgot(userId, password)
  //     .subscribe((res: any) => {
  //       console.log(res);
  //       if(res){
  //         this.router.navigate(['/tabs/tab1']);
  //       }
  //     }
  //     ,(err) => {
  //       console.log(err);
  //     });
  // }

  async openmodal() {
    const modal = await this.modalController.create({
      component: SearchModalPagePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
