import { ItemsService } from './items.service';
import { CommonService } from './common.service';
import { Component, OnInit, OnDestroy, ViewChild, enableProdMode } from '@angular/core';
import { Platform, AlertController, IonRouterOutlet, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { CallApisService } from './call-apis.service';
import { Observable, Observer, Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';
import { CartService } from '../app/services/cart.service';
import { Router } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Network } from '@ionic-native/network/ngx';
import { ToastController } from '@ionic/angular';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx'; 
import { BackgroundMode } from '@ionic-native/background-mode/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  @ViewChild(IonRouterOutlet, {static: false}) routerOutlet: IonRouterOutlet
  userIsAuthenticated: boolean = false;
  private authListenerSubs: Subscription;
  private itemSubscription: Subscription;
  private categorySubscription: Subscription;
  private netSubs: Subscription;
  imageUrl: any;
  popularItems: any[] = [];
  filteredProduct: any[] = [];
  storeId: number = 8;
  spinner: boolean = false;
  refId: number = 8;
  constructor(
    private platform: Platform,
    private backgroundMode: BackgroundMode,
    public localNotifications: LocalNotifications,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public apiService: CallApisService,
    private commonService: CommonService,
    private router: Router,
    private cartService:  CartService,
    private menu: MenuController,
    private socialSharing: SocialSharing,
    private network: Network,
    public toastController: ToastController
  ) {
    // alert('workign')
    this.initializeApp();
    this.backButtonEvent();
  
    this.imageUrl = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/files/';
    localStorage.setItem('storeId', '3');
    localStorage.setItem('refId', '3');
    // setInterval(function(){
    //   localNotifications.schedule({
    //     id: 1,
    //     text: 'You have a 50% offers on the saturday',
    //     // sound: isAndroid? 'file://sound.mp3': 'file://beep.caf',
    //     data: { secret: 'secret' }
    //   });
    // }, 500);    
  }

  ngOnInit(){
    this.apiService.getAppconfigurationsProperties()
    .subscribe((data: any) => {
      console.log(data);
      const appConfiguration = JSON.parse(data.appConfiguration);
      console.log(appConfiguration)
    },(err: any) => {
      console.log(err);
    })
    this.commonService.setSpinner(this.spinner);
    // let list = [];
    // this.commonService.set_itemList(list);
    this.network.onDisconnect().subscribe((res: any) => {
      setTimeout(async () => {
        const toast = await this.toastController.create({
          message: 'You are offline.',
          duration: 2000,
          color: 'danger',
          mode: 'ios'
        });
        toast.present();
      }, 2000)
    })

    this.network.onConnect().subscribe((res: any) => {
      // setTimeout(async () => {
      //   const toast = await this.toastController.create({
      //     message: 'You are online now.',
      //     duration: 2000
      //   });
      //   toast.present();
      // }, 2000)
    })

    let token = localStorage.getItem('token');
    if(token){
      this.userIsAuthenticated = true
    }
    else{
      this.userIsAuthenticated = false
    }
    this.authListenerSubs = this.apiService.getAuthStatusListener()
    .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
    })

    this.spinner = true;
    this.commonService.setSpinner(this.spinner);
     this.itemSubscription = this.apiService.getAllItems().subscribe((itemRes: any[]) => {   
      //  console.log(itemRes);
       if(itemRes){
         this.spinner = false;
         this.commonService.setSpinner(this.spinner);
        let res = itemRes.filter((data: any) => {
          return data.storeId === this.storeId
       })
       console.log(res);
       res.forEach((res: any) => {

         if(res.quantity === null){
           res.quantity = 0;
         }
         if(res.categories === null){

        }
        else{
         res.categories.forEach((data: any) => {
           if(data.name === "Popular"){
             this.popularItems.push(res);
           }
         });
         
        }
      });
        this.commonService.passItems(this.popularItems);
        this.filteredProduct = res;
        // console.log(this.filteredProduct)
        // let imagesForItem: any[] = [];
        // let images = {
        //   src: ''
        // }
        // this.filteredProduct.forEach(data => {
        //   if(data.itemImages.length != 0){
        //     for(let i=0;i<data.itemImages.length;i++){
        //       this.getBase64ImageFromURL(this.imageUrl + data.itemImages[i].img_name).subscribe(base64data => {
        //         data.base64Image = 'data:image/jpg;base64,' + base64data;
        //         images.src = 'data:image/jpg;base64,' + base64data;
        //         imagesForItem.push(images)
        //         data.itemsImages = imagesForItem
        //         let uniqueItemData = [...new Set(imagesForItem)]
        //         imagesForItem = []
        //         uniqueItemData.forEach((data: any) => {
        //           imagesForItem.push(data);
        //         })
        //           this.filteredProduct.push(data);
        //           this.makeUnique()
        //         });
        //       }
        //     // data.itemImages.forEach((images: any) => {
        //     //   this.fourthSub =  this.getBase64ImageFromURL(this.imageUrl + images.img_name).subscribe(base64data => {
        //     //     data.base64Image = 'data:image/jpg;base64,' + base64data;
        //     //       this.filteredProduct.push(data);
        //     //      this.makeUnique()
        //     //     });
        //     //  })
        //     }
        // });   
        this.commonService.set_itemList(this.filteredProduct);
       }
       else{
        this.spinner = true;
        this.commonService.setSpinner(this.spinner);
       }
      
      });

     this.categorySubscription = this.apiService.getCatagories()
        .subscribe((res: any) => {
          // console.log(res)
          this.commonService.set_categoryList(res);
          res.forEach((data: any) => {
            this.apiService.storeId.next(data.storeId)
          });
      },
         (err: any) => {
          console.log(err);
       });

      //  this.getItemImage();
  }

  

  // ngOnInit(){

  //   // this.makeBacgroundEnable();
    
  //   // setInterval(function(){
  //   //   this.localNotifications.schedule({
  //   //     id: 1,
  //   //     text: 'You have a 50% offers on the saturday',
  //   //     // sound: isAndroid? 'file://sound.mp3': 'file://beep.caf',
  //   //     data: { secret: 'secret' }
  //   //   });
  //   // }, 1000)
   

  //  this.netSubs =  this.network.onDisconnect().subscribe((res: any) => {
  //     setTimeout(async () => {
  //       const toast = await this.toastController.create({
  //         message: 'You are offline.',
  //         duration: 2000,
  //         color: 'danger',
  //         mode: 'ios'
  //       });
  //       toast.present();
  //     }, 2000)
  //   })

  //   this.network.onConnect().subscribe((res: any) => {
  //     // setTimeout(async () => {
  //     //   const toast = await this.toastController.create({
  //     //     message: 'You are online now.',
  //     //     duration: 2000
  //     //   });
  //     //   toast.present();
  //     // }, 2000)
  //   })

  //   let token = localStorage.getItem('token');
  //   if(token){
  //     this.userIsAuthenticated = true
  //   }
  //   else{
  //     this.userIsAuthenticated = false
  //   }
  //   this.authListenerSubs = this.apiService.getAuthStatusListener()
  //   .subscribe(isAuthenticated => {
  //       this.userIsAuthenticated = isAuthenticated;
  //   })

  //    this.itemSubscription = this.apiService.getAllItems().subscribe((res: any[]) => {   
  //      console.log(res);
  //      res.forEach((res: any) => {
  //       res.categories.forEach((data: any) => {
  //         if(data.name === "POPULAR"){
  //           this.popularItems.push(res);
  //         }
  //       });
  //     });
  //       this.commonService.passItems(this.popularItems);
  //       this.filteredProduct = res;
  //       this.filteredProduct.forEach(data => {
  //         if(data.img_name != null){
  //           this.getBase64ImageFromURL(this.imageUrl + data.img_name ).subscribe(base64data => {
  //             data.base64Image = 'data:image/jpg;base64,' + base64data;
  //             this.filteredProduct.push(data);
  //             this.makeUnique();
  //             // console.log(this.filteredProduct)
  //           });
  //         }
  //       });   
  //       // console.log(this.filteredProduct)
  //       // this.commonService.set_itemList(this.filteredProduct);
  //     });

  //    this.categorySubscription = this.apiService.getCatagories()
  //       .subscribe((res: any) => {
  //         this.commonService.set_categoryList(res);
  //         res.forEach((data: any) => {
  //           this.apiService.storeId.next(data.storeId)
  //         });
  //     },
  //        (err: any) => {
  //         console.log(err);
  //      });

  //     //  this.getItemImage();
  // }

  makeBacgroundEnable(){
    // alert('workifn ')
    this.backgroundMode.enable();
    this.backgroundMode.on('activated').subscribe(() => {
      this.backgroundMode.disableWebViewOptimizations();
      // alert('background mode enabled ')
    })
  }


  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    
    });
}

backButtonEvent(){
 this.platform.backButton.subscribeWithPriority(50, async() => {
       
        if(this.router.url === '/orderplaced'){
          // alert('order placed works ')
          this.router.navigate(['/tabs/tab2'])
        }
        else if(this.router.url === '/my-order'){
          // alert('order placed works ')
          this.router.navigate(['/tabs/tab2'])
        }
        else if(this.router.url === '/tabs/tab2'){
          // alert('tab2 works')
           this.router.navigate(['/tabs/tab1'])
        }
        else  if(this.router.url === '/tabs/tab5'){
          this.router.navigate(['/tabs/tab1'])
        }
        else if(this.router.url === '/tabs/tab3'){
          this.router.navigate(['/tabs/tab1'])
        }
        else   if(this.router.url === '/check-out'){
          this.router.navigate(['/tabs/tab1'])
        } 
        else if(this.router.url === '/tabs/tab1'){
          navigator["app"].exitApp();
        }
        else if(this.routerOutlet && this.routerOutlet.canGoBack()){
          this.routerOutlet.pop();
        }
        
      })
}

gotoOrderPage(){
  let token = localStorage.getItem('token');
  // console.log(token);
  if(token){
    this.router.navigate(['/my-order']);
    this.menu.close();
  }
  else{
    this.router.navigate(['/tabs/tab3']);
    this.menu.close();
  }
}

  getBase64ImageFromURL(url: string) {
    return Observable.create((observer: Observer<string>) => {
     
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.src = url;  img.src = url;
      if (!img.complete) {
        img.onload = () => {
          observer.next(this.getBase64Image(img));
          observer.complete();
        };
        img.onerror = (err) => {
          observer.error(err);
        };
      } else {
        observer.next(this.getBase64Image(img));
        observer.complete();
      }
    });
    
  }

  getBase64Image(img: HTMLImageElement) {
    var canvas = document.createElement("canvas");
    canvas.width = 180;
    canvas.height = 180;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, 180, 180);
    var dataURL = canvas.toDataURL("image/png");
    // console.log(dataURL);
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }

  getItemImage(){
  
  }

ngOnDestroy() {
  this.authListenerSubs.unsubscribe();
  this.itemSubscription.unsubscribe();
  this.itemSubscription.unsubscribe(); 
  this.netSubs.unsubscribe()
  }

  logout(){
    this.cartService.makeCartEmpty();
    this.menu.close();
   
  }

  gotoLogin(){
    this.menu.close();
    this.router.navigate(['/tabs/tab3'])
  }

  share(){
    this.menu.close();
    var appUrl = 'https://play.google.com/store/apps/details?id=com.tbi.fivestar'
    this.socialSharing.share('', '', '', appUrl)
    .then(() => {

    })

  }

  makeUnique(){
    let cities = [...new Set(this.filteredProduct)]
    this.filteredProduct = []
    cities.forEach((data: any) => {
      this.filteredProduct.push(data);
      // console.log(this.filteredProduct)
      this.commonService.set_itemList(this.filteredProduct);
    })
    
  }

  openEnd() {  
    this.menu.close();
   
    }
}

    //  this.platform.backButton.subscribeWithPriority(11, () => {
    //     if(this.router.url === '/tabs/tab2'){
    //       this.router.navigate(['/tabs/tab1'])
    //     }
    //   })