import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SavedaddressesPage } from './savedaddresses.page';

describe('SavedaddressesPage', () => {
  let component: SavedaddressesPage;
  let fixture: ComponentFixture<SavedaddressesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedaddressesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SavedaddressesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
