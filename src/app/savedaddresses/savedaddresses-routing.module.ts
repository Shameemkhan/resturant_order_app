import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SavedaddressesPage } from './savedaddresses.page';

const routes: Routes = [
  {
    path: '',
    component: SavedaddressesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SavedaddressesPageRoutingModule {}
