import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SavedaddressesPageRoutingModule } from './savedaddresses-routing.module';

import { SavedaddressesPage } from './savedaddresses.page';
import { SavedaddressesPipe } from '../savedaddresses.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    SavedaddressesPageRoutingModule
  ],
  declarations: [SavedaddressesPage, SavedaddressesPipe]
})
export class SavedaddressesPageModule {}
