import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
// import { AgmCoreModule } from '@agm/core';

import { AgmMapPageRoutingModule } from './agm-map-routing.module';

import { AgmMapPage } from './agm-map.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    // AgmCoreModule,
    AgmMapPageRoutingModule
  ],
  declarations: [AgmMapPage]
})
export class AgmMapPageModule {}
