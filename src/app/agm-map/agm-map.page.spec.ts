import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AgmMapPage } from './agm-map.page';

describe('AgmMapPage', () => {
  let component: AgmMapPage;
  let fixture: ComponentFixture<AgmMapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgmMapPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AgmMapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
