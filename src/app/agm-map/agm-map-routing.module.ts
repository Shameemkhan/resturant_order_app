import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgmMapPage } from './agm-map.page';

const routes: Routes = [
  {
    path: '',
    component: AgmMapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgmMapPageRoutingModule {}
