import { CallApisService } from './../call-apis.service';
import { ModalController } from '@ionic/angular';
import { SearchModalPagePage } from '../search-modal-page/search-modal-page.page';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {  CartService } from '../services/cart.service';
import { CalculationService } from '../calculation.service';

@Component({
  selector: 'app-coupon',
  templateUrl: './coupon.page.html',
  styleUrls: ['./coupon.page.scss'],
})
export class CouponPage implements OnInit {
  cart = [];
  qty=[];
  products = [];
  cartItemCount: BehaviorSubject<number>;
  totalAmt: any = '0';
  savePrice: any;
  subTotalAmt: any = '0'
  taxAmount: any = 0
  subTotalAmount: any = 0;
  subTotal1: any = "0";
  deliveryCharges: any = 0;
  
  taxes = {
    SGST: 0,
    CGST: 0,
    VAT: 0
  }

  @ViewChild('cart', {static: false, read: ElementRef})fab: ElementRef;
  food: string = "Breakfast&Diary";
  foods:string = "brft1"; 
  sizeArray: any;

  constructor(private cartService: CartService, 
    public modalController: ModalController,
    private calculationService: CalculationService,
    private apiService: CallApisService) { }

  ngOnInit() {
    this.cart = this.cartService.getCart();
    this.calculateTaxes();

   
    // this.calculationService.setVatAmount(this.taxes.VAT)
    // this.calculationService.setCgstAmount(this.taxes.CGST)
    // this.calculationService.setSgstAmount(this.taxes.SGST)
  }

  ionViewWillEnter(){
    let pincode = localStorage.getItem('pincode')
    let storeId = localStorage.getItem('storeId')

    this.apiService.getPincodeDEtails(pincode, storeId).subscribe((data: any) => {
      this.deliveryCharges =  data.deliveryCharges;
      this.calculationService.setDeliveryCharges(this.deliveryCharges)
      this.calculationService.setPaymentType(data.paymentType)
    })
  }

  calculateTaxes(){
    this.makeTaxesZero();
    // if(this.taxes.CGST > 0){
    //   this.taxes.CGST = this.totalCgstTax();
    //   this.taxes.CGST += this.taxes.CGST * product.quantity;
    // }
    // else{
    //   this.taxes.CGST = 0;
    // }

    // if(this.taxes.SGST > 0){
    //   this.taxes.SGST = this.totalSgstTax()
    //   this.taxes.SGST += this.taxes.SGST * product.quantity;
    // }
    // else{
    //   this.taxes.SGST = 0;
    // }

    // if(this.taxes.VAT > 0){ 
    //   let vatTotal = 0;
    //   this.taxes.VAT = this.totalVatTax();
    //   vatTotal += this.taxes.VAT * product.quantity;
    //   this.taxes.VAT = vatTotal;
    // }
    // else{
    //   this.taxes.VAT = 0;
    // }

    this.cart.forEach((cartItem: any) => {
      cartItem.taxes.forEach((taxes: any) => {
        if(taxes.name != null){
          if(taxes.name.includes('SGST')){
            let total =  (cartItem.price * taxes.structure.value) / 100;
            this.taxes.SGST  += total * cartItem.quantity;
            
            let getSingleTaxAMount = total * cartItem.quantity
            taxes.amount = getSingleTaxAMount
            }
            else if(taxes.name.includes('CGST')){
              let total =  (cartItem.price * taxes.structure.value) / 100;
              this.taxes.CGST  += total * cartItem.quantity;
    
              let getSingleTaxAMount = total * cartItem.quantity
              taxes.amount = getSingleTaxAMount;
            }
            else{
              let total =  (cartItem.price / taxes.structure.value) * cartItem.quantity;
              this.taxes.VAT += total;
            
              let getSingleTaxAMount =  (cartItem.price / taxes.structure.value) * cartItem.quantity; 
              taxes.amount = getSingleTaxAMount;
            }
        }
        
      });
    });
  }

  // calculateTaxes(){
  //   this.makeTaxesZero();
  //   this.cart.forEach((cartItem: any) => {
  //     cartItem.taxes.forEach((taxes: any) => {
  //       if(taxes.name.includes('SGST')){
  //        let sgstTotal = 0;
  //        sgstTotal += parseFloat(taxes.amount) * parseInt(cartItem.quantity);
  //        this.taxes.SGST += sgstTotal;
  //       }
  //       else if(taxes.name.includes('CGST')){
  //        let cgstTotal = 0;
  //        cgstTotal += parseFloat(taxes.amount) * parseInt(cartItem.quantity);
  //        this.taxes.CGST += cgstTotal;
  //       }
  //       else{
  //         let vatTotal = 0;
  //         vatTotal +=  parseFloat(taxes.amount) * parseInt(cartItem.quantity);
  //         this.taxes.VAT += vatTotal;
  //       }
  //     });
  //   });
  // }

  makeTaxesZero(){
    this.taxes.CGST = 0;
    this.taxes.SGST = 0;
    this.taxes.VAT = 0;
  }
  // getTotal() {
  //   return this.cart.reduce((i, j) => i + j.price * j.amount, 0);
  // }

  getTotal() {
    // return this.cart.reduce((i, j) => i + j.price * j.discountPrice, 0);
    let total: any = 0;
    for(var i = 0; i<this.cart.length; i++){
      if(this.cart[i].price){
        total += this.cart[i].price * this.cart[i].quantity;
        this.totalAmt = total;
      }
    }
    return total;
  }

  getTotal2(){
    this.subTotalAmount= 0;
    for(var i = 0; i<this.cart.length; i++){
      if(this.cart[i].price){
        this.subTotalAmount += (this.cart[i].price - this.cart[i].discountPrice) * this.cart[i].quantity;
    
        this.subTotal1 = this.subTotalAmount;
      }
    }
    
    return this.subTotalAmount;
  }

  priceSave(){
    let savePrice: any = 0;
    for(var i = 0; i<this.cart.length; i++){
      if(this.cart[i].discountPrice){
        savePrice += this.cart[i].discountPrice * this.cart[i].quantity;
        this.savePrice = savePrice;
      }
    }
    return savePrice;
  }

  subTotal() {
    // return this.cart.reduce((i, j) => i + j.price * j.discountPrice, 0);
    this.subTotalAmount = 0;
    for(var i = 0; i<this.cart.length; i++){
      if(this.cart[i].price){
        this.subTotalAmount += (this.cart[i].price - this.cart[i].discountPrice) * this.cart[i].quantity;
        this.subTotalAmt = this.subTotalAmount;
      }
    }
    return this.subTotalAmount;
  }

  totalTaxes(){
    this.taxAmount = 0;
    this.taxAmount = this.taxes.CGST + this.taxes.SGST + this.taxes.VAT
    // this.cart.forEach((cartData: any) => {
    //   for(let i=0;i<cartData.taxes.length;i++){
    //     if(cartData.taxes[i].length != 0){
    //       this.taxAmount += cartData.taxes[i].amount * cartData.quantity;
    //     }
    //   }
    // })
    return this.taxAmount;
  }

  // totalTaxes(){
  //   this.taxAmount = 0;
  //   console.log(this.cart);
  //   this.cart.forEach((cartData: any) => {
  //     for(let i=0;i<cartData.taxes.length;i++){
  //       if(cartData.taxes[i].length != 0){
  //         this.taxAmount += cartData.taxes[i].amount * cartData.quantity;
  //       }
  //     }
  //   })
  //   return this.taxAmount;
  // }

  grandTotal(){
    let grandTotal: number = 0;
    grandTotal = this.subTotalAmount + this.taxAmount + parseInt(this.deliveryCharges);
    this.apiService.grandTotal.next(grandTotal);
    return grandTotal;
  }

  async openmodal() {
    const modal = await this.modalController.create({
      component: SearchModalPagePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}