import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CalculationService {
  sgstPrice: number = 0;
  cgstPrice: number = 0;
  vatPrice: number = 0
  paymentList: any[] = [];
  paymentName: any;
  deliveryPrice: any = 0;
  constructor() { }

  private fireMethod = new Subject<any>();

    publishToOnBackPressed(data: any) {
        this.fireMethod.next(data);
    }

    getObservable(): Subject<any> {
        return this.fireMethod;
    } 
    
  setSgstAmount(sgst){
    this.sgstPrice = sgst;
  }

  getSgstPrice(){
    return this.sgstPrice;
  }

  setCgstAmount(cgst){
    this.cgstPrice = cgst;
  }

  getCgstPrice(){
    return this.cgstPrice;
  }

  setVatAmount(vat){
    this.vatPrice = vat;
  }

  getVatAmount(){
    return this.vatPrice;
  }

  setPaymentType(paymentList){
    this.paymentList = paymentList;
  }

  getPaymentList(){
    return this.paymentList;
  }

  setTypeName(paymentType){
    this.paymentName = paymentType;
  }

  getTypeName(){
    return this.paymentName;
  }

  setDeliveryCharges(chargesAmount){
    this.deliveryPrice = chargesAmount;
  }

  getDeliveryCharges(){
    return this.deliveryPrice;
  }
}
