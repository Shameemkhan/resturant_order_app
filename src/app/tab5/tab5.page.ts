import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SearchModalPagePage } from '../search-modal-page/search-modal-page.page';
import { CartService } from '../services/cart.service';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { CommonService } from '../common.service';



@Component({
  selector: 'app-tab5',
  templateUrl: './tab5.page.html',
  styleUrls: ['./tab5.page.scss'],
})
export class Tab5Page implements OnInit {

  cart: any[] = [];
  // latitude = 51.678418;
  // longitude = 7.809007;

  constructor( public modalController: ModalController, private cartService: CartService, private platform: Platform, private router: Router, public commonService: CommonService) {
      
   }

  ngOnInit() {
    this.cart = this.cartService.getCart();
    
    // console.log(this.cart)
  }

ionViewWillEnter(){
}
  async openmodal() {
    const modal = await this.modalController.create({
      component: SearchModalPagePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  emptyCart(){   
    this.cart =[];  
  
  }

  removeProduct(){
    this.cartService.makeCartEmpty();
  }
}
