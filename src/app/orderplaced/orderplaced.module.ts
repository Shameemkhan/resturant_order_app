import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderplacedPageRoutingModule } from './orderplaced-routing.module';

import { OrderplacedPage } from './orderplaced.page';
import { UniquePipeModule } from '../unique.pipe.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UniquePipeModule,
    IonicModule,
    OrderplacedPageRoutingModule
  ],
  declarations: [OrderplacedPage]
})
export class OrderplacedPageModule {}
