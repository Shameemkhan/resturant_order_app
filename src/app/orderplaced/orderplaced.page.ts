import { CommonService } from './../common.service';
import { Router } from '@angular/router';
import { CallApisService } from './../call-apis.service';
import { Items } from './../models/items';
import { CartService } from './../services/cart.service';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { BehaviorSubject, Observable, Observer } from 'rxjs';
import { ModalController } from '@ionic/angular';
import { SearchModalPagePage } from '../search-modal-page/search-modal-page.page';

@Component({
  selector: 'app-orderplaced',
  templateUrl: './orderplaced.page.html',
  styleUrls: ['./orderplaced.page.scss'],
})
export class OrderplacedPage implements OnInit {
  orderProduct: any[] = [];
  orderItems: Items[] = [];
  cartItemCount: BehaviorSubject<number>;
  momentjs: any = moment;
  conv: any[] = [];
  conv2: any[] = [];
  v3: any;
  param2: any;
  date: any;
  customerId: any;
  customerDate: any;
  orderDate: any;
  imageUrl: any;
  orderData: any[] = []
  orderId: any[] = [];
  spinner: boolean = false;


  constructor(private cartService: CartService, 
    private apiService: CallApisService,  public modalController:ModalController, private router: Router, private commonService: CommonService) { 
  }

  
ionViewWillEnter(){
  this.spinner = true;
  this.cartItemCount = this.cartService.getCartItemCount();
  this.getOrders();
}

  ngOnInit() {
    this.imageUrl = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/files/';
    this.cartItemCount = this.cartService.getCartItemCount();
    
    this.apiService.customerId.subscribe((res: any) => {

      this.customerId = res;
    })
  
}



getBase64ImageFromURL(url: string) {
  return Observable.create((observer: Observer<string>) => {
    let img = new Image();
    img.crossOrigin = 'Anonymous';
    img.src = url;  img.src = url;
    if (!img.complete) {
      img.onload = () => {
        observer.next(this.getBase64Image(img));
        observer.complete();
      };
      img.onerror = (err) => {
        observer.error(err);
      };
    } else {
      observer.next(this.getBase64Image(img));
      observer.complete();
    }
  });
}

getBase64Image(img: HTMLImageElement) {
  var canvas = document.createElement("canvas");
  canvas.width = 180;
  canvas.height = 180;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0, 180, 180);
  var dataURL = canvas.toDataURL("image/png");
  // console.log(dataURL);
  return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

getOrders(){
  let userId = localStorage.getItem('userId');
  this.apiService.getOrderDetailsOfUser(userId)
  .subscribe((res: any[]) => {
    console.log(res);
    this.spinner = false;
      this.orderData = res.reverse();
      // console.log(this.orderData);
      this.orderData.forEach((data: any) => {
        // this.orderDate = data.date;
        // this.orderDate = moment().format('Do MMMM YYYY');
        data.items.forEach((itemsData: any) => {
          // console.log(itemsData)
          itemsData.item.forEach((data: any) => {
            if(data.itemImages.length != 0){
              for(let i=0;i<data.itemImages.length;i++){
                 this.getBase64ImageFromURL(this.imageUrl + data.itemImages[i].img_name).subscribe(base64data => {
                  itemsData.base64Image = 'data:image/jpg;base64,' + base64data;
                  // images.src = 'data:image/jpg;base64,' + base64data;
                  // imagesForItem.push(images)
                  // data.itemsImages = imagesForItem
                  // this.orderItems.push(itemsData)
                  // let uniqueItemData = [...new Set(imagesForItem)]
                  //   imagesForItem = []
                  //   uniqueItemData.forEach((data: any) => {
                  //     imagesForItem.push(data);
                  //   })
                  });
                }
              }
          })
        })
        // data.items.forEach((data: any) => {
        //   if(data.image_url != null){
        //     this.getBase64ImageFromURL(this.imageUrl + data.image_url ).subscribe(base64data => {
        //       data.base64Image = 'data:image/jpg;base64,' + base64data;
        //       this.orderItems.push(data);
        //     });
        //   }else{}
        // })
      })
  },
  (err) => {
    console.log(err);
  });
}
gotoTab3(){
  this.router.navigate(['/tabs/tab2'])
}

async openmodal() {
  const modal = await this.modalController.create({
    component: SearchModalPagePage,
    cssClass: 'my-custom-class'
  });
  return await modal.present();
}

gotoChangePassword(){
  this.router.navigate(['/change-password'])
}

gotoMyOrder(){
  this.router.navigate(['/my-order'])
}

gotoAddress(){
  this.router.navigate(['/save-address'])
}
}
