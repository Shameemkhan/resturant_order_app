import { DialogPage } from './dialog/dialog.page';
import { EditAddOnsPageModule } from './edit-add-ons/edit-add-ons.module';
import { EditAddOnsPage } from './edit-add-ons/edit-add-ons.page';
import { AddOnsPageModule } from './add-ons/add-ons.module';
import { AddOnsPage } from './add-ons/add-ons.page';
import { IonicImageLoader } from 'ionic-image-loader';
import { SearchCategoryPipeModule } from './search.category.pipe.module';
import { SearchModalPagePageModule } from './search-modal-page/search-modal-page.module';
import { SuperTabsModule} from '@ionic-super-tabs/angular'
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { CartModalPageModule } from './pages/cart-modal/cart-modal.module';
import { HTTP } from '@ionic-native/http/ngx';
import { IonicModule, IonicRouteStrategy, Platform, ModalController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { ModalPage } from './modal/modal.page';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import { DatePipe, HashLocationStrategy, LocationStrategy } from '@angular/common'

import { SearchModalPagePage } from './search-modal-page/search-modal-page.page';

import { FormsModule } from '@angular/forms';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx'
// import { Network } from '@ionic-native/network/ngx';
import {MatRadioModule} from '@angular/material/radio';

import { GoogleMaps } from '@ionic-native/google-maps';
import { GoogleMapsService } from 'src/app/google-maps.service';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { Network } from '@ionic-native/network/ngx';
import { ConnectivityServiceService } from './connectivity-service.service';

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { ImagePreviewPage } from './image-preview/image-preview.page';
import { ImagePreviewPageModule } from './image-preview/image-preview.module';
import { UniquePipe } from './unique.pipe';
import { UniquePipeModule } from './unique.pipe.module';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';

// import { IonicImageViewerModule } from 'ionic-img-viewer';
// import { AgmCoreModule } from '@agm/core';
// import { Network } from '@ionic-native/network';

@NgModule({
  declarations: [
    AppComponent,

    SearchModalPagePage,
    AddOnsPage,
    DialogPage,
    EditAddOnsPage,
    // UniquePipe
    // SearchCategoryPipe
    ],
  
  imports: [BrowserModule, 
    // IonicImageViewerModule,
    IonicModule.forRoot(),
    // IonicModule.forRoot({
    //   animated: false
    // }),
    NgxIonicImageViewerModule,
    IonicImageLoader.forRoot(),
    SuperTabsModule.forRoot(),
     AppRoutingModule,
    //  IonicModule.forRoot({
    //   mode: 'ios',
    //  }),
     BrowserAnimationsModule,
     MatRadioModule,
      HttpClientModule ,
      CartModalPageModule,
      NgxIonicImageViewerModule,
      SearchCategoryPipeModule,
      UniquePipeModule,
    //   AgmCoreModule.forRoot({
    //     apiKey: "AIzaSyBEJASPEiprIZlZvgbpnxtPZRdEDRroSPY",
    //     libraries: ["places"]
    // }),
      MatDialogModule,
      MatButtonModule,
      FormsModule,
      EditAddOnsPageModule,
      SearchModalPagePageModule,
      ImagePreviewPageModule,
      AddOnsPageModule
    ],
    entryComponents: [SearchModalPagePage, AddOnsPage, EditAddOnsPage, DialogPage, ImagePreviewPage],
  providers: [
    WebView,
    DatePipe,
    StatusBar,
    HTTP,
    Network,
    BackgroundMode,
    SocialSharing,
    LocalNotifications,
    SplashScreen,
    GoogleMapsService,
    ConnectivityServiceService,
    AndroidPermissions,
    Geolocation,
    LocationAccuracy,
    GoogleMaps,
    Geolocation,
    NativeGeocoder,
    {provide : LocationStrategy , useClass: HashLocationStrategy},
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    // {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
  
})
export class AppModule {
constructor(public platform: Platform, public statusBar: StatusBar) {
    platform.ready().then(() => {
        statusBar.styleDefault();
        if (platform.is('android')) {
            statusBar.overlaysWebView(false);
            statusBar.backgroundColorByHexString('#000000');
        }
    });
}
}