import { IonicHttpService } from './../ionic-http.service';
import { addressUser } from './addressUser';
import { CallApisService } from './../call-apis.service';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Form } from '@pnp/sp/src/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {  ElementRef, NgZone, ViewChild } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { GoogleMapsService } from '../google-maps.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { pincodeValidators } from '../signup/pincode.validators';

declare var google;

@Component({
  selector: 'app-address-details',
  templateUrl: './address-details.page.html',
  styleUrls: ['./address-details.page.scss'],
})
export class AddressDetailsPage implements OnInit {
  @ViewChild('map', {static: true}) element;
  @ViewChild('map',  {static: true}) mapElement: ElementRef;
  @ViewChild('pleaseConnect',  {static: true}) pleaseConnect: ElementRef;

  latitude: number;
  longitude: number;
  autocompleteService: any;
  placesService: any;
  query: string = '';
  places: any = [];
  searchDisabled: boolean;
  saveDisabled: boolean;
  location: any;  
  selectArea: any;
  moNumber = '';
  userId = '';
  address1: any= '';
  submitted: boolean = false; 
  states: any[] = [];
  cities: any = {}
  stateName: any = {};
  pincode: any;
  pincodeCheck: boolean = false;
  pincodeCheckONSubmit: boolean = false;
  lengthOfTheCities: number = 0;

  form = new FormGroup({
    userId: new FormControl(),
    pincode: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]),
    fristName: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z \-\']+')]),
    lastName: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z \-\']+')]),
    street1: new FormControl('', Validators.required),
    addressUser: new FormArray([
     this.saveAddress()
    ]),
    customer: new FormArray([
     this.addCustomer()
    ]),
    mobileNo: new FormControl(''),
    state: new FormGroup({
      id: new FormControl(''),
      stateName: new FormControl('', Validators.required),
    }),
    city: new FormGroup({
      id: new FormControl(''),
      cityName: new FormControl('', Validators.required),
    })
  },
  {asyncValidators: []})
 
  constructor(private apiService: CallApisService, 
    private fb: FormBuilder, 
    private route: ActivatedRoute,
    private ionicHttp: IonicHttpService,
    private router: Router,
    private navCtrl: NavController,
    private zone: NgZone, 
    private maps: GoogleMapsService, 
    private platform: Platform,
    private geolocation: Geolocation) { 
      this.searchDisabled = true;
      this.saveDisabled = true;
    }
  
  
  ngOnInit() {  
    let token = localStorage.getItem('token');
    this.apiService.typeValue.subscribe((res: any) => {
      this.moNumber = res;
    });
  }

  ionViewWillEnter(){
    this.isEmpty(this.cities);
    this.userId = localStorage.getItem('userId');
// for map enable this
    let mapLoaded = this.maps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement).then(() => {
      this.autocompleteService = new google.maps.places.AutocompleteService();
      this.placesService = new google.maps.places.PlacesService(this.maps.map);
      this.searchDisabled = false;

      this.apiService.getStates().subscribe((states: any) => {
        this.states = states;
      })

  }); 

  this.apiService.getStates().subscribe((states: any) => {
    this.states = states;
  })
   
    // this.apiService.typeValue.subscribe((res: any) => {
    //   this.moNumber = res;
    // });

    this.moNumber = localStorage.getItem('moNumber');

    // this.apiService.userId.subscribe((res: any) => {
    //   this.userId = res;
    // })
    // console.log(this.userId);
    this.pincode = this.form.get('pincode').value;
  }

  f(controls) {
    return this.form.get(controls);
  }

  s(controls){
    return this.form.controls.state.get(controls);
  }

  c(controls){
    return this.form.controls.city.get(controls);
  }

  
  filterState(){
    
    let state_id = this.form.controls.state.get('stateName').value
    this.lengthOfTheCities = state_id.citys.length;
    this.stateName = state_id;
    this.cities = state_id;
   
 }

 filterCity(){
   
   let cityData = this.form.controls.city.get('cityName').value;
   this.form.controls.city.value.id = cityData.id
   this.form.controls.city.value.cityName = cityData.cityName;
 }

  customerData(){
    this.form.controls.customerDetails.value.address.pin = this.form.get('pincode').value;
      this.form.controls.customerDetails.value.address.line_1 = this.address1;
      this.form.controls.customerDetails.value.address.line_2 = this.address1;
      this.form.controls.customerDetails.value.address.city = this.form.controls.city.value.id;
  }

  applyValidation(){
    this.pincodeCheckONSubmit = false;
    let value = this.form.get('pincode').value;
    let storeId = localStorage.getItem('storeId')
    this.apiService.getPincodeDEtails(this.form.get('pincode').value, storeId).subscribe((data: any) => {
      if(data === null){
        this.pincodeCheck = true;
    }
    else{
      this.pincodeCheck = false;
    }
  })
  }

  checkValue(){
    this.pincodeCheck = false;
    this.pincodeCheckONSubmit = false;
  }

   onSubmit(){
      this.submitted = true;
      this.address1 = this.form.get('street1').value;
      this.form.value.addressUser[0].street1 = this.address1;
      this.form.value.customer[0].address.line_1 = this.address1;
      this.form.value.customer[0].address.line_2 = this.address1;
      this.form.value.customer[0].address.landmark = this.address1;
      this.form.value.customer[0].name = this.form.get('fristName').value + " " + this.form.get('lastName').value ;
      this.form.value.customer[0].phone = this.moNumber;
      this.form.value.customer[0].address.pin = this.form.get('pincode').value;
      this.form.value.userId = this.userId;
      this.form.value.mobileNo = this.moNumber;
      this.form.controls.state.value.id = this.stateName.id;
      this.form.controls.state.value.stateName = this.stateName.stateName;
      let storeId = localStorage.getItem('storeId');

      if(this.form.invalid){
        return;
     }

      this.apiService.getPincodeDEtails(this.form.value.pincode, storeId).subscribe((data: any) => {
        console.log(data);
        if(data != null){
            this.apiService.saveUser(this.form.value).subscribe((res: any) => {
              this.router.navigate(['/delivery-address'])
          }, err => {
            console.log(err);
          });
        }
        else{
          this.pincodeCheck = false
          this.pincodeCheckONSubmit = true;
        }
      });
  
      // this.address1 = this.form.get('street1').value
      // this.form.value.addressUser[0].street1 = this.address1;
      // this.form.value.customer[0].address.line_1 = this.address1;
      // this.form.value.customer[0].address.line_2 = this.address1;
      // this.form.value.customer[0].address.landmark = this.address1;
      // this.form.value.customer[0].name = this.form.get('fristName').value + " " + this.form.get('lastName').value ;
      // this.form.value.customer[0].phone = this.form.get('mobileNo').value;
      // this.form.value.customer[0].address.pin = this.form.get('pincode').value;
      // this.form.value.userId = this.userId
      // this.form.controls.state.value.id = this.stateName.id
      // this.form.controls.state.value.stateName = this.stateName.stateName
      // console.log(this.form.value)
  
      // this.ionicHttp.saveUser(this.form.value).then((res: any) => {
      //   console.log(res)
      //   if(res){
      //     this.router.navigate(['/delivery-address']);
      //   }
      // })
      // .catch((err: any) => {
      //   console.log(err);
      // });

    }

saveAddress(){
  return this.fb.group({
    street1: this.address1
  })
}

addCustomer(){
  return this.fb.group({
    email: '',
    address: {
      "city": 15,
      "is_guest_mode": "",
      "landmark": "",
      "latitude": "",
      "line_1": this.address1,
      "line_2": "Bhopal, Madhya Pradesh, India",
      "longitude": "",
      "pin": 41,
      "sub_locality": "",
      "tag": ""
    }
  })
}

isEmpty(obj) {
  for(var key in obj) {
      if(obj.hasOwnProperty(key))
          return false;
  }
  return true;
}

selectPlace(place){
  this.places = [];

  let location = {
      lat: null,
      lng: null,
      name: place.name
  };
  this.placesService.getDetails({placeId: place.place_id}, (details) => {

      this.zone.run(() => {

          location.name = details.name;
          location.lat = details.geometry.location.lat();
          location.lng = details.geometry.location.lng();
          this.saveDisabled = false;

          this.maps.map.setCenter({lat: location.lat, lng: location.lng}); 
          this.maps.addMakerMax(location.lat, location.lng);
          this.location = location;

      });

  });

  this.selectArea = place.description;
}

searchPlace(){

  this.saveDisabled = true;

  if(this.query.length > 0 && !this.searchDisabled) {

      let config = {
          types: ['geocode'],
          input: this.query
      }

      this.autocompleteService.getPlacePredictions(config, (predictions, status) => {

          if(status == google.maps.places.PlacesServiceStatus.OK && predictions){

              this.places = [];

              predictions.forEach((prediction) => {
                  this.places.push(prediction);
              });
          }

      });

  } else {
      this.places = [];
  }

  }
}