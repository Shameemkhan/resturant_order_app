import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { CallApisService } from './../call-apis.service';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SearchModalPagePage } from '../search-modal-page/search-modal-page.page';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {
  oldPassword: any;
  userId: any;
  newPassword: any;
  new_Password: any;
  submitted: boolean = false;
  constructor(private apiService: CallApisService, 
    public modalController:ModalController, 
    public formBuilder: FormBuilder,
    private router: Router) { }

  form =  this.formBuilder.group({
    oldPassword: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    confirmPassword: new FormControl('', Validators.compose([
      Validators.required
    ]))
  }, {
    validators: this.password.bind(this)
  });

  ngOnInit() {
   
    this.userId = localStorage.getItem('userId');
  }

  // changPassword(f: NgForm){
  //   this.apiService.changePassword(this.oldPassword, this.userId, this.newPassword);
  //   f.reset();
  //   alert('Password Changed Successfully!!');
  // }

  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: confirmPassword } = formGroup.get('confirmPassword');
    return password === confirmPassword ? null : { passwordNotMatch: true };
  }

  async openmodal() {
    const modal = await this.modalController.create({
      component: SearchModalPagePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  f(controls) {
    return this.form.get(controls);
  }
 

  onSubmit(){
    this.submitted = true;

    if(this.form.invalid){
      return
    }

    this.oldPassword = this.form.get('oldPassword').value;
    this.newPassword = this.form.get('password').value;

    console.log(typeof(this.oldPassword));

    let oldPass = this.oldPassword.toString();
    console.log(this.oldPassword);
    console.log(this.newPassword);
    console.log(this.userId);
    this.apiService.changePassword(oldPass, this.userId, this.newPassword)
      .subscribe((res: any) => {}
      ,(err: any) => {
        console.log(err);
        if(err.status === 202){
          alert('Password Changed Successfully!!');
          this.router.navigate(['/tabs/tab1'])
        }
      });
    this.form.reset();
  }

  gotoChangePassword(){
    this.router.navigate(['/change-password'])
  }

  gotoMyOrder(){
    this.router.navigate(['/my-order'])
  }

  gotoAddress(){
    this.router.navigate(['/save-address'])
  }
}
