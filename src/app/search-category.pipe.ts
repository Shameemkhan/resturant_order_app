import { Item } from '@pnp/sp';
import { Items } from './models/items';
import { CommonService } from './common.service';
import { category } from './models/category';
import { Pipe, PipeTransform } from '@angular/core';
import { CallApisService } from './call-apis.service';

@Pipe({
  name: 'searchCategory'
})
export class SearchCategoryPipe implements PipeTransform {
  constructor(private commonService: CommonService) {}
  attributeList: any[];
  transform(attribute: any[], searchText: string) {
    this.attributeList = this.commonService.get_itemList();
    // attribute = (searchText) ?
    // this.attributeList.filter((p: Items) => p.name.toLowerCase().includes(searchText.toLowerCase())):
    // this.attributeList;

    if (searchText == "") {
      return attribute;
    } else if (searchText != "" && searchText!=undefined) {
      return this.attributeList.filter(
        (item: Items) => item.name.toLowerCase().includes(searchText.toLowerCase())
            /*item.name.toLowerCase().includes(searchText.toLowerCase()) */
      );
    } else {
      return this.attributeList;
    }
  }
}
