import { AttributeService } from './../attribute.service';
import { CommonService } from './../common.service';
import { Router } from '@angular/router';
import { CallApisService } from './../call-apis.service';
import { Items } from './../models/items';
import { CartService } from '../services/cart.service';
import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { SearchModalPagePage } from '../search-modal-page/search-modal-page.page';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.page.html',
  styleUrls: ['./check-out.page.scss'],
})
export class CheckOutPage implements OnInit {
  cart: Items[] = [];
  itemList: Items[] = [];
  subTotal: any = "0";
  savePrice: any = '0';
  totalAmt: any = "0";
  filteredProduct: any[] = [];
  itemData: any = {}
  taxAmount: number = 0;
  subTotalAmount: number = 0;
  grandTotalAmt: any = 0;

  taxes = {
    SGST: 0,
    CGST: 0,
    VAT: 0
  }

  cartItemCount: BehaviorSubject<number>;
  constructor(private cartService: CartService, 
    private modalCtrl: ModalController, 
    private alertCtrl: AlertController,
    private apiService: CallApisService,
    private router: Router,
    public commonService: CommonService,
    private attributeService: AttributeService,
    public modalController:ModalController,
    private platform: Platform) {}
 
  ngOnInit() {
   
  }
 
  
  ionViewWillEnter(){
    let cart = this.cartService.getCart();
    this.cart = cart.reverse();
    this.cartItemCount = this.cartService.getCartItemCount();
    this.filteredProduct.forEach((data: any) => {
      this.itemData = data;
    });

    // this.calculateTaxes();
    this.taxes.CGST = this.totalCgstTax();
    this.taxes.SGST = this.totalSgstTax();
    this.taxes.VAT = this.totalVatTax();
  }

  calculateTaxes(product){
    this.makeTaxesZero();
    // if(this.taxes.CGST > 0){
    //   this.taxes.CGST = this.totalCgstTax();
    //   this.taxes.CGST += this.taxes.CGST * product.quantity;
    // }
    // else{
    //   this.taxes.CGST = 0;
    // }

    // if(this.taxes.SGST > 0){
    //   this.taxes.SGST = this.totalSgstTax()
    //   this.taxes.SGST += this.taxes.SGST * product.quantity;
    // }
    // else{
    //   this.taxes.SGST = 0;
    // }

    // if(this.taxes.VAT > 0){ 
    //   let vatTotal = 0;
    //   this.taxes.VAT = this.totalVatTax();
    //   vatTotal += this.taxes.VAT * product.quantity;
    //   this.taxes.VAT = vatTotal;
    // }
    // else{
    //   this.taxes.VAT = 0;
    // }

    this.cart.forEach((cartItem: any) => {
      cartItem.taxes.forEach((taxes: any) => {
        if(taxes.name != null){
          if(taxes.name.includes('SGST')){
            let total =  (cartItem.price * taxes.structure.value) / 100;
            this.taxes.SGST  += total * cartItem.quantity
            }
            else if(taxes.name.includes('CGST')){
              let total =  (cartItem.price * taxes.structure.value) / 100;
              this.taxes.CGST  += total * cartItem.quantity;
            }
            else{
              this.taxes.VAT +=  (cartItem.price / taxes.structure.value) * cartItem.quantity;
            }
        }
      
      });
    });
  }

  // removecalculateTaxes(product){
  //   if(this.taxes.CGST > 0){
  //     this.taxes.CGST = this.totalCgstTax();
  //     this.taxes.CGST += this.taxes.CGST * product.quantity;
  //   }
  //   else{
  //     this.taxes.CGST = 0;
  //   }

  //   if(this.taxes.SGST > 0){
  //     this.taxes.SGST = this.totalSgstTax()
  //     this.taxes.SGST += this.taxes.SGST * product.quantity;
  //   }
  //   else{
  //     this.taxes.SGST = 0;
  //   }

  //   if(this.taxes.VAT > 0){ 
  //     let vatTotal = 0;
  //     this.taxes.VAT = this.totalVatTax();
  //     vatTotal += this.taxes.VAT * product.quantity;
  //     this.taxes.VAT = vatTotal;
  //   }
  //   else{
  //     this.taxes.VAT = 0;
  //   }

  // }

  makeTaxesZero(){
    this.taxes.CGST = 0;
    this.taxes.SGST = 0;
    this.taxes.VAT = 0;
  }

  decreaseCartItem(product) {
    localStorage.setItem('qty', '1')
    let data;
    product.optionGroup.slice(-1).forEach((cartGroup: any) => {
     data = cartGroup;
    });
    if(product.optionGroup.length > 0 && data.length > 0){
      this.cartService.deleteProductToAddOns(product);
    }
    else if(product.optionGroup.length > 0 && data.length === 0){
      this.cartService.decreaseProduct(product);
    }
    else{
      this.cartService.decreaseProduct(product);
    }
    this.calculateTaxes(product);
  }
 
  increaseCartItem(product) {
    localStorage.setItem('qty', '1')
    let data;
    product.optionGroup.slice(-1).forEach((cartGroup: any) => {
      data = cartGroup 
    });
    if(product.optionGroup.length > 0 && data.length > 0){
      this.cartService.addProductToAddOns(product);
    }
    else if(product.optionGroup.length > 0 && data.length === 0){
      this.cartService.addProduct(product);
      this.makeUnique();
    }
    else{
      this.cartService.addProduct(product);
      this.makeUnique();
    }
    this.calculateTaxes(product);
  }
 
  removeCartItem(product) {
    this.cartService.removeProduct(product);
    this.calculateTaxes(product);
  }
 
  priceSave(){
    let savePrice: any = 0;
    for(var i = 0; i<this.cart.length; i++){
      if(this.cart[i].discountPrice){
        savePrice += this.cart[i].discountPrice * this.cart[i].quantity;
        this.savePrice = savePrice; 
      }
    }
    return savePrice;
  }

  gotoTab2(){
    this.router.navigate(['/tabs/tab2'])
  }

  subTotalPrice() {
  
    // return this.cart.reduce((i, j) => i + j.price * j.discountPrice, 0);
    this.subTotalAmount= 0;
    for(var i = 0; i<this.cart.length; i++){
      if(this.cart[i].price){
        this.subTotalAmount += (this.cart[i].price - this.cart[i].discountPrice) * this.cart[i].quantity;
    
        this.subTotal = this.subTotalAmount;
      }
    }
    
    return this.subTotalAmount;
  
  }

  // getTotal() {
  //   // return this.cart.reduce((i, j) => i + j.price * j.discountPrice, 0);
  //   let total: any = 0;
  //   for(var i = 0; i<this.cart.length; i++){
  //     if(this.cart[i].price){
  //       total += this.cart[i].price * this.cart[i].quantity;
  //       this.totalAmt = total;
  //     }
  //   }
  //   return total;
  // }



  close() {
    this.modalCtrl.dismiss();
  }
 
  checkout()  {
     let cartItem = this.cartService.getCartItemCount();
     let cartCount =  cartItem.getValue();
     if(cartCount <= 0){
       this.router.navigate(['/tabs/tab2']);
     }
     else{
      let token = localStorage.getItem('token');
      this.apiService.subTotalAmt.next(this.subTotal);
      this.apiService.totalAmt.next(this.subTotal);
      this.apiService.savePrice.next(this.savePrice);
      if(token){
        this.router.navigate(['/delivery-address']);
      }
      else{
        this.router.navigate(['/tabs/tab3']);
      }
     }
    // Perfom PayPal or Stripe checkout process
    // let alert = await this.alertCtrl.create({
    //   header: 'Thanks for your Order!',
    //   message: 'We will deliver your food as soon as possible',
    //   buttons: ['OK']
    // });
    // alert.present().then(() => {
    //   this.modalCtrl.dismiss();
    // });
  }

  async openmodal() {
    const modal = await this.modalController.create({
      component: SearchModalPagePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  

  makeUnique(){
    let cities = [...new Set(this.cart)]
    this.cart = []
    cities.forEach((data: any) => {
      this.cart.push(data);
    })
    
  }

  // totalVatTax(){
  //   let VatTaxAmount = 0;
  //   this.cart.forEach((cartData: any) => {
  //     for(let i=0;i<cartData.taxes.length;i++){
  //       if(cartData.taxes[i].length != 0 && cartData.taxes[i].name.includes('VAT')){
  //         VatTaxAmount += cartData.taxes[i].amount * cartData.quantity;
  //       }
  //     }
  //   })
  //   return VatTaxAmount;
  // }

  totalVatTax(){
    let VatTaxAmount = 0;
    this.cart.forEach((cartData: any) => {
      for(let i=0;i<cartData.taxes.length;i++){
        if(cartData.taxes[i].name != null){
          if(cartData.taxes[i].length != 0 && cartData.taxes[i].name.includes('VAT')){
            VatTaxAmount +=  (cartData.price / cartData.taxes[i].structure.value) * cartData.quantity;
          }
        }
      }
    })
    return VatTaxAmount;
  }

  totalCgstTax(){
    let cgstTaxAmount = 0;
    this.cart.forEach((cartData: any) => {
      for(let i=0;i<cartData.taxes.length;i++){
        if(cartData.taxes[i].name != null){
          if(cartData.taxes[i].length != 0 && cartData.taxes[i].name.includes('CGST')){
            let total =  (cartData.price * cartData.taxes[i].structure.value) / 100;
            cgstTaxAmount += total * cartData.quantity
          }
        }
       
      }
    });
    return cgstTaxAmount;
  }

  totalSgstTax(){
    let sgstTaxAmount = 0;
    this.cart.forEach((cartData: any) => {
      for(let i=0;i<cartData.taxes.length;i++){
        if(cartData.taxes[i].name != null){
          if(cartData.taxes[i].length != 0 && cartData.taxes[i].name.includes('SGST')){
            let total =  (cartData.price * cartData.taxes[i].structure.value) / 100;
            sgstTaxAmount += total * cartData.quantity
          }
        }
      }
    });
    return sgstTaxAmount;
  }

  totalTaxes(){
    this.taxAmount = 0;
    this.taxAmount = this.taxes.CGST + this.taxes.SGST + this.taxes.VAT
    // this.cart.forEach((cartData: any) => {
    //   for(let i=0;i<cartData.taxes.length;i++){
    //     if(cartData.taxes[i].length != 0){
    //       this.taxAmount += cartData.taxes[i].amount * cartData.quantity;
    //     }
    //   }
    // })
    return this.taxAmount;
  }

  grandTotal(){
    this.grandTotalAmt = 0;
    this.grandTotalAmt = this.subTotalAmount + this.taxAmount;
    return this.grandTotalAmt;
  }

}
