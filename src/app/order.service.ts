import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  
  getOrdersById = "https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/getOne/";

  getOrderDetails = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/order/data/1/83/2020-10-15'

  constructor(private http: HTTP) { }

  
  fetchOrderById(orderId: any){
    return this.http.get(this.getOrdersById + orderId, {}, {});
   }

   getOederDetailsOfUser(){
    return this.http.get(this.getOrderDetails, {}, {})
  }
  
}
