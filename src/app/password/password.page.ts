import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from './../auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CallApisService } from '../call-apis.service';

@Component({
  selector: 'app-password',
  templateUrl: './password.page.html',
  styleUrls: ['./password.page.scss'],
})
export class PasswordPage implements OnInit {
  password: string;
  moNumber: string;
  submitted: boolean = false;
  showError: boolean = false;
  
  constructor(private apiService: CallApisService, 
    private router: Router,
    private authService: AuthService) { }

    form = new FormGroup({
      password: new FormControl('', Validators.required)
    })


  ngOnInit() {
    this.apiService.typeValue.subscribe((res: any) => {
      // console.log(res);
      this.moNumber = res;
    });


    // this.password = this.form.get('password').value
  }

  
  f(controls) {
    return this.form.get(controls);
  }
 
   onSubmit(){
    this.showError = false;
    this.submitted = true;

    if(this.form.invalid){
       return;
    }

    this.password = this.form.get('password').value
    // console.log(this.password);
    // console.log(this.moNumber);
    // let userPass = this.password.toString();
    // console.log(userPass)
    this.apiService.login(this.moNumber, this.password)
    .subscribe(result => {
      // console.log(result)
      if(result)
      // sessionStorage.setItem('moNumber', this.moNumber);
      localStorage.setItem('moNumber', this.moNumber);
        this.router.navigate(['/delivery-address']);
    },
    (err) => {
      console.log(err)
      this.showError = true;
      // alert('Invalid UserId and Password!');  
      // this.router.navigate(['/tabs/tab3']);
    })
   }

   removeError(){
    this.showError = false;
   }

  
  login(){
    this.password = this.form.get('password').value
    // console.log(this.password)
    this.apiService.login(this.moNumber, this.password)
    .subscribe(result => {
      // console.log(result)
      if(result)
        this.router.navigate(['/delivery-address']);
    },
    (err) => {
      alert('Invalid UserId and Password!');  
      this.router.navigate(['/log-in']);
    })
  }

}
