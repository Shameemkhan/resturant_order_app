import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { CallApisService } from './call-apis.service';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  itemsList: any = [];
  categoriesList: any = [];
  popularItemList: any[] = [];
  isLoading = false;
  spinner: any;
  itemName = new Subject();
  private popularItems = new BehaviorSubject<any>([])
  sharedParam = this.popularItems.asObservable();
  public checkQuantity = new Subject<number>()
  constructor(private apiService: CallApisService, public loadingController: LoadingController, private router: Router) {
   }
 
  set_itemList(data){
    this.itemsList = data;
  }

  get_itemList(){
    return this.itemsList;
  }

  setSpinner(data){
    this.spinner = data;
  }

  getSpinner(){
    return this.spinner;
  }

  set_categoryList(data){
    this.categoriesList = data
  }

  get_categoryList(){
    return this.categoriesList;
  }

  passItems(params: any[]){
    this.popularItems.next(params)
  }

  async present() {
    this.isLoading = true;
    return await this.loadingController.create({
      message: 'Please wait...',
      // duration: 1000
      // duration: 5000,
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  gotoHome(){
    this.router.navigate(['/tabs/tab1']);
  }

}
