import { CommonService } from './../common.service';
import { DialogBoxComponent } from './../dialog-box/dialog-box.component';
import { MatDialog } from '@angular/material/dialog';
import { Items } from './../models/items';
import { CartService } from '../services/cart.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CallApisService } from './../call-apis.service';
import { Component, OnInit, ViewChild, } from '@angular/core';
import { IonSlides, ModalController, NavController } from '@ionic/angular';
import { Observable, Observer } from 'rxjs';
import { AddOnsPage } from '../add-ons/add-ons.page';
import { EditAddOnsPage } from '../edit-add-ons/edit-add-ons.page';
import { CalculationService } from '../calculation.service';
import { ViewerModalComponent } from 'ngx-ionic-image-viewer';

@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.page.html',
  styleUrls: ['./item-details.page.scss'],
})
export class ItemDetailsPage implements OnInit {
  @ViewChild('myNav', {static: true}) nav: NavController
  itemId: any
  cart = [];
  imageUrl: any;
  items :any[] = [];
  filteredProduct: any[] = [];
  cartId: any;
  qty: any;
  

  slideOptions = {
    initialSlide: 1,
    speed: 1000,
  };

  constructor(private apiService: CallApisService, 
    private route: ActivatedRoute,
    private cartService: CartService,
    private router: Router,
    private dialog: MatDialog,
    private commonService: CommonService,
    public modalController:ModalController,
    private calculationService: CalculationService,) { }

  ngOnInit() {
    this.imageUrl = 'https://www.ere4u.in/thirdparty/eRe4u-third-party-staging/item/files/';
    this.itemId =  this.route.snapshot.paramMap.get('id');
  }

  ionViewWillEnter(){
    this.getItemImage();

    this.filteredProduct = this.commonService.get_itemList();
   
    this.cartService.sendValue.subscribe((res: any) => {
      // console.log(res);
      this.qty = res;
    })
  }
  async openViewer(previewImage) {
    const modal = await this.modalController.create({
      component: ViewerModalComponent,
      componentProps: {
        src: "https://www.ere4u.in/thirdparty/uploads/" + previewImage
      },
      cssClass: 'ion-img-viewer',
      keyboardClose: true,
      showBackdrop: true,
      animated: false,
    });
 
    return await modal.present();
  }
  getBase64ImageFromURL(url: string) {
    return Observable.create((observer: Observer<string>) => {
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.src = url;  img.src = url;
      if (!img.complete) {
        img.onload = () => {
          observer.next(this.getBase64Image(img));
          observer.complete();
        };
        img.onerror = (err) => {
          observer.error(err);
        };
      } else {
        observer.next(this.getBase64Image(img));
        observer.complete();
      }
    });
  }

  getBase64Image(img: HTMLImageElement) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    // console.log(dataURL);
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }

  gotoTab2(){
    let data: string = 'backPressed';
    this.calculationService.publishToOnBackPressed(data);
    this.router.navigate(['/tabs/tab2']);
    // this.nav.navigateBack(['/tabs/tab2'])
  }

  getItemImage(){
    let imagesForItem: any[] = [];
    let images = {
      src: ''
    }

    this.apiService.getItemById(this.itemId)
    .subscribe((data: any) => {
      if(data.quantity === null){
        data.quantity = 0;
      }
      this.items.push(data);
      this.makeUnique();
    },
    (err: any) => {
      console.log(err);
    });  
  }

  makeUnique(){
    let uniqueItemData = [...new Set(this.items)]
    this.items = []
    uniqueItemData.forEach((data: any) => {
      this.items.push(data);
    })
  }

  async addToCart(product){
  
    if(product.optionGroup.length > 0){
      if(product.quantity === 0){
        // this.router.navigate(['add-ons', product.id])
        const modal = await this.modalController.create({
          component: AddOnsPage,
          componentProps: {
            "prodId": product.id
          },
          cssClass: 'my-modal-css'
        });
        modal.onDidDismiss().then(() => {
          // alert('works ')
          this.cartService.getTotalQuantityOfPerticalItem(product)
        })
        return await modal.present();
      }
      else{
        // this.router.navigate(['/edit-add-ons']);
        const modal = await this.modalController.create({
          component: EditAddOnsPage,
          componentProps: {
            "prodId": product.id,
            mode: 'ios'
          },
          cssClass: 'my-Edit_AddOns_modal-css'
        });
        modal.onDidDismiss().then(() => {
          this.cartService.getTotalQuantityOfPerticalItem(product)
        })
        return await modal.present();
        // this.cartService.addProduct(product);
        // let cart = this.cartService.getCart();
        // this.filteredProduct.forEach((item: any) => {
        //   cart.forEach((cartItem: any) => {
        //     if(item.id === cartItem.id){
        //       item.quantity = cartItem.quantity;
        //     }
        //   });
        // });
      }
    }
    else{
      this.cartService.addProduct(product);
    // let dialogRef = this.dialog.open(DialogBoxComponent, {
    //   width: '80%', 
    //   panelClass: 'myapp-no-padding-dialog'
    // });
    }
    
  }

  async goToBuy(product){
    // let items = this.commonService.get_itemList();
   
    if(product.optionGroup.length > 0){
      if(product.quantity === 0){
        // this.router.navigate(['add-ons', product.id])
        const modal = await this.modalController.create({
          component: AddOnsPage,
          componentProps: {
            "prodId": product.id
          },
          cssClass: 'my-modal-css'
        });
        modal.onDidDismiss().then(() => {
          // alert('works ')
          this.cartService.getTotalQuantityOfPerticalItem(product)
        })
        return await modal.present();
      }
      else{
        // this.router.navigate(['/edit-add-ons']);
        const modal = await this.modalController.create({
          component: EditAddOnsPage,
          componentProps: {
            "prodId": product.id,
            mode: 'ios'
          },
          cssClass: 'my-Edit_AddOns_modal-css'
        });
        modal.onDidDismiss().then(() => {
          this.cartService.getTotalQuantityOfPerticalItem(product)
        })
        return await modal.present();
        // this.cartService.addProduct(product);
        // let cart = this.cartService.getCart();
        // this.filteredProduct.forEach((item: any) => {
        //   cart.forEach((cartItem: any) => {
        //     if(item.id === cartItem.id){
        //       item.quantity = cartItem.quantity;
        //     }
        //   });
        // });
      }
    }
    else{
      this.cartService.addProduct(product);
      this.router.navigate(['/check-out'])
    }
    // this.cart = this.cartService.getCart();
    // this.cart.forEach((cartItem: any) => {
    //   items.forEach((itemData: any) => {
    //     if(cartItem.id === itemData.id && itemData.id === product.id){
    //       itemData.discountPrice = cartItem.discountPrice;
    //     }
    //   });
    // });
   
  }

  slidesDidLoad(slides: IonSlides) {
    slides.startAutoplay();
  } 
}