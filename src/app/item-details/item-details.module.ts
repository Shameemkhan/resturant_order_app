import { DialogBoxComponent } from './../dialog-box/dialog-box.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';

import { ItemDetailsPageRoutingModule } from './item-details-routing.module';

import { ItemDetailsPage } from './item-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ItemDetailsPageRoutingModule,
    MatDialogModule,
    MatButtonModule
  ],
  declarations: [ItemDetailsPage,  DialogBoxComponent],
  entryComponents: [DialogBoxComponent]
})
export class ItemDetailsPageModule {}
