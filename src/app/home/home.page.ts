import { Tab2Page } from './../tab2/tab2.page';
import { Tab1Page } from './../tab1/tab1.page';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  tab1Page: Tab1Page;
  tab2Page: Tab2Page
  constructor() { }

  ngOnInit() {
  }

}
