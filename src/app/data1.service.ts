import { Injectable } from '@angular/core';
export interface products {
  
  pro_id:string;
 name:string;
 image:string;
 price:number;
 quantity:number;
}
@Injectable({
  providedIn: 'root'
})
export class Data1Service {
 

  
  shops=[
    {id:'1',
    title:'Jain Kirana Store',
    img:'/assets/img/shop1.jpg',
    rat:4.5,
    del:'Delivery in 40 Mins',
    adress:'Mp Nagar Zone 1',
   
    },
    {id:'2',
    title:'Reliance Fresh',
    img:'assets/img/shop2.jpg',
    rat:4.5,
    del:'Delivery in 40 Mins',
    adress:'Mp Nagar Zone 1',
 
    },
    {id:'3',
    title:'Sheree Kirana Store',
    img:'assets/img/shop3.jpg',
    rat:4.5,
    del:'Delivery in 40 Mins',
    adress:'Mp Nagar Zone 1',

   
    },
    {id:'4',
    title:'Jain Kirana Store',
    img:'/assets/img/shop1.jpg',
    rat:4.5,
    del:'Delivery in 40 Mins',
    adress:'Mp Nagar Zone 1',
    },

    {id:'5',
    title:'New Kirana Store',
    img:'/assets/img/shop1.jpg',
    rat:4.5,
    del:'Delivery in 40 Mins',
    adress:'Mp Nagar Zone 1',
   
   
    },
    {id:'6',
    title:'apna Kirana Store',
    img:'/assets/img/shop1.jpg',
    rat:4.5,
    del:'Delivery in 40 Mins',
    adress:'Mp Nagar Zone 1',
   
   
    },
    {id:'7',
    title:'D-mart ',
    img:'/assets/img/shop1.jpg',
    rat:4.5,
    del:'Delivery in 40 Mins',
    adress:'Mp Nagar Zone 1',
   
   
    },
    {id:'8',
    title:'kolar D-mart',
    img:'/assets/img/shop1.jpg',
    rat:4.5,
    del:'Delivery in 40 Mins',
    adress:'Mp Nagar Zone 1',
   
   
    },
    
    ];

    // -------------------------------//

    data1:products[] = [
      { pro_id:'1',
        name:'oranges',
        image:'assets/img/background-bitter-breakfast-bright-161559.jpg',
        price:2,
        quantity:0
      },
      { pro_id:'2',
        name:'strawberry',
        image:'assets/img/strawberries-berries-fruit-freshness-46174.jpeg',
        price:10,
        quantity:0
      },
      { pro_id:'3',
        name:'graps',
        image:'assets/img/pexels-photo-708777.jpeg',
        price:2,
        quantity:0
      },
      { pro_id:'4',
        name:'berry',
        image:'assets/img/pexels-photo-175727.jpeg',
        price:18,
        quantity:0
      },
      { pro_id:'5',
        name:'Blueberry',
        image:'assets/img/blueb.jpg',
        price:2,
        quantity:0
      },
      { pro_id:'6',
        name:'apple',
        image:'assets/img/apple.jpg',
        price:25,
        quantity:0
      },

    ]


  constructor() { }
  getProductItem(Id){
      
    return this.shops.filter(p => {
      return p.id == Id
      
    });
   
  }
  getShopsDetails()
  {
    return this.shops;    
  }
 getProdDetails()
  {
    return this.data1;
  }




  /*filter*/
 
  
  
}
