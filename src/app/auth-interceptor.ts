import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthInterceptor implements HttpInterceptor{

    constructor(){

    }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        let authReq = req;
        const token = localStorage.getItem('token');
        if(token != null){
            authReq = req.clone({ headers: req.headers.set("Authorization", 'Bearer ' + token) });
        }
        console.log(authReq)
        return next.handle(authReq);
        
        // const token = localStorage.getItem('token');
        // console.log(token);
        // const authReq = req.clone({
        //     headers: req.headers.set("Authorization", "Bearer " + token)
        // })
        // console.log(authReq);
        // return next.handle(authReq);
    }
}
